from rest_framework import routers
from .viewsets import SheetViewSet

router = routers.SimpleRouter()

router.register(r'^api/sheets', SheetViewSet, 'sheets')
