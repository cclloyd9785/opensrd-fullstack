import React, { Component } from 'react';
import { reduxForm } from 'redux-form'
import { withStyles } from '@material-ui/core/styles'
import {connect} from "react-redux";
import compose from "recompose/compose";
import { loadSheet } from "../../../actions/sheet";
import { setView } from "../../../actions/view";
import { setSection} from "../../../actions/page";
import { userId, authErrors } from "../../../reducers/index";
import FormSheetEdit from '../../form/FormSheetEdit';


const styles = theme => ({

});

class LayoutSheetEdit extends Component {
    constructor(props) {
        super(props);

    }

    componentDidMount() {
        let regex = /\/sheets\/(\d+)/;
        let location = this.props.location.pathname;
        let sheetId = location.match(regex)[1];

        this.props.fetchSheet(sheetId);
        this.props.setSection('sheets');
        this.props.setView('edit');
    }
    onSubmit = (e) => {
        e.preventDefault();
        this.props.onSubmit(this.state.sheet)
    };



    render() {
        return (
            <div>
                <FormSheetEdit />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        errors: authErrors(state),
        user: state.auth.access,
        user_id: userId(state),
        page: {
            sheet: state.sheet
        },
        nav: {
            section: state.currentSection,
            view: state.currentView,
        },
        initialValues: state.sheet,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchSheet: (sheetId) => dispatch(loadSheet(sheetId)),
        setSection: (section) => dispatch(setSection(section)),
        setView: (view) => dispatch(setView(view)),
    }
};


export default compose(
    reduxForm({
        form: 'SheetEditForm',
    }),
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
    withStyles(styles, {
            withTheme: true
        },
    ))(LayoutSheetEdit);