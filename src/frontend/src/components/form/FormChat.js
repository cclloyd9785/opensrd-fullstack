import React, { Component } from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import {Field, formValueSelector, getFormValues, reduxForm, submit} from 'redux-form'
import { TextField, Paper, Typography, Divider, CardContent, Button, Hidden, Grid } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import { dark } from '../App'
import {connect} from "react-redux";
import compose from "recompose/compose";
import {userId, authErrors} from "../../reducers";
import SaveIcon from '@material-ui/icons/Save';
import CachedIcon from '@material-ui/icons/Cached';
import { renderCheckbox, renderSelectField, renderTextField } from "./reduxFormMaterialUi";
import {
    addCurrentInputToInputLog,
    addMessageToInputLog,
    createChatMessage,
    replaceCurrentInputInInputLog
} from "../../actions/chat";
import debounce from 'debounce';


const styles = theme => ({
    textInput: {
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    longPaper: {
        padding: theme.spacing.unit * 3,
        backgroundColor: dark[5],
    },
    gameTitle: {
        fontSize: 48,
        fontFamily: [
            'Balthazar',
        ],
    },
    gameContents: {
        fontSize: 22,
    },
    row: {
        'padding-top': theme.spacing.unit * 3,
        'padding-bottom': theme.spacing.unit * 3,
    },
    divider: {
        //'padding-top': theme.spacing.unit * 2
    },
    sectionTop: {
        'margin-top': theme.spacing.unit * 4,
    },
    sectionBottom: {
        'margin-bottom': theme.spacing.unit * 1,
    },
    sectionText: {
        'background-color': dark[5],
        'padding-left': theme.spacing.unit * 1,
    },
    pageContent: {},
    contentPage: {},
    sectionHeader: {
        'font-size': 24,
    },
    sectionPaper: {
        'margin-top': theme.spacing.unit * 3,
        'margin-bottom': theme.spacing.unit * 3,
    },
    gameCardContent: {
        'padding-left': 0,
        'padding-right': 0,
    },
    noPadding: {
        paddingLeft: 0,
        paddingRight: 0,
    },
    fullWidth: {
        width: '100%',
    },
    fullHeight: {
        height: '100%',
    },
    noGrow: {
        flexGrow: 0,
    }
});

class FormChat extends Component {
    constructor(props) {
        super(props);
        //this.debouncedOnChange = debounce(this.onChange, 1000);
        this.state = {
            inputLogPos: 0,
            inputStored: false,
        };

        //this.handleInputSend = this.handleInputSend.bind(this);
    }

    componentDidMount() {
        this.props.initialize(this.state)
    }

    captureKeystrokes = (e) => {
        // Send form on enter
        if(e.keyCode === 13 && e.shiftKey === false) {
            e.preventDefault();
            console.log('sending');
            this.props.sendSubmit(this.props.allValues);
            this.props.addCurrentInputToInputLog(this.props.allValues);
            this.props.change('Message', "")
        }

        // Cycle up in input log
        if(e.keyCode === 38 && e.shiftKey === false) {
            e.preventDefault();
            let pos = this.state.inputLogPos + 1;
            if (this.props.page.chat.inputLog.length === 1) {
                this.props.replaceCurrentInputInInputLog(this.props.allValues);
                return;
            }
            else {
                pos = Math.min(pos, this.props.page.chat.inputLog.length - 1);
                this.setState({
                    inputLogPos: pos,
                });
                if (pos === 1)
                    this.props.replaceCurrentInputInInputLog(this.props.allValues)
                this.props.change('Message', this.props.page.chat.inputLog[pos].Message)
            }

        }

        // Cycle down in input log
        if(e.keyCode === 40 && e.shiftKey === false) {
            e.preventDefault();
            let pos = this.state.inputLogPos - 1;
            if (this.props.page.chat.inputLog.length === 1) {
                this.props.replaceCurrentInputInInputLog(this.props.allValues);
            }
            else {
                pos = Math.max(pos, 0);
                this.setState({
                    inputLogPos: pos,
                });
                this.props.change('Message', this.props.page.chat.inputLog[pos].Message)
            }
        }
    };



    handleInputSend = () => {
        const message = {
            id: 0,
            Message: "",
        };
        this.props.addCurrentInputToInputLog(this.props.allValues);
        //this.simulateClick(document.getElementById("submit-button"));
        this.props.change('Message', "");

    };

    simulateClick(e) {
        e.click()
    }

    render() {
        const {classes} = this.props;

        // eslint-disable-next-line
        const { submit, handleSubmit, pristine, reset, submitting } = this.props;

        return (
            <form
                onSubmit={handleSubmit(this.props.sendSubmit)}
                className={classes.fullHeight}
                id="chat-form"
            >
                <Hidden only={['xs', 'sm', 'md', 'lg', 'xl']}>
                    <Field
                        name="session"
                        component={renderTextField}
                    />
                    <Field
                        name="Character"
                        component={renderTextField}
                    />
                    <Field
                        name="channel"
                        component={renderTextField}
                        value={this.props.chat.session.uri}
                    />
                </Hidden>

                <Grid
                    container
                    spacing={0}
                    className={classNames(classes.fullWidth, classes.fullHeight)}
                >
                    <Grid
                        item
                        xs={10}
                    >
                        <Field
                            fullWidth multiline
                            rows={2}
                            className={classes.fullHeight}
                            name="Message"
                            component={renderTextField}
                            InputProps={{
                                classes:{
                                    root: classes.fullHeight,
                                }
                            }}
                            onKeyDown={this.captureKeystrokes}
                        />
                    </Grid>
                    <Grid item xs={2}>
                        <Button
                            fullWidth
                            variant={'raised'}
                            color="primary"
                            className={classes.fullHeight}
                            onClick={handleSubmit(this.props.sendSubmit)}
                            id="submit-button"
                        >
                            Send
                        </Button>
                    </Grid>
                </Grid>


            </form>
        );
    }
}

FormChat.propTypes = {
    classes: PropTypes.object.isRequired,
};

FormChat.defaultProps = {
    chat: {
        session: {
            uri: '',
        }
    },
};

const mapStateToProps = (state) => {
    return {
        errors: authErrors(state),
        user: state.user,
        page: {
            game: state.game,
            chat: state.chat,
        },
        initialValues: {
            game: state.game.model.id,
            session: 1,
            channel: state.chat.session.uri,
        },
        allValues: getFormValues("FormChat")(state),
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        sendSubmit: (message) => {
            dispatch(createChatMessage(message))
                .then(response => {
                    //response.payload is ChatMessage model
                })
        },
        addCurrentInputToInputLog: (message) => dispatch(addCurrentInputToInputLog(message)),
        replaceCurrentInputInInputLog: (message) => dispatch(replaceCurrentInputInInputLog(message)),
    }
};

export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
    reduxForm({
        form: 'FormChat',
        enableReinitialize: true
    }),
    withStyles(styles, {
            withTheme: true
        },
    ))(FormChat);
