from rest_framework import viewsets, mixins
from django.contrib.auth.models import User
from rest_framework.response import Response
from rest_framework.decorators import action
import hashlib

from .serializers import UserSerializer, UserPublicSerializer


class UserViewSet(
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def retrieve(self, request, *args, **kwargs):
        return Response(status=403)

    @action(methods=['get'], detail=False)
    def info(self, request):
        user = None
        if request and hasattr(request, "user"):
            user = request.user

        if user.username == '':
            return Response(status=403)

        gravatar = hashlib.md5(user.email.encode("utf-8")).hexdigest()
        gravatar_url = "https://www.gravatar.com/avatar/%s" % gravatar
        user.gravatar = gravatar
        user.gravatar_url = gravatar_url

        serializer = self.get_serializer(user)
        return Response(serializer.data)

    @action(methods=['get'], detail=True)
    def public(self, request, pk=None):
        user = self.get_object()
        serializer = self.get_serializer(user)
        return Response(serializer.data)