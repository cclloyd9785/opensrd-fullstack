import React from 'react';
import { Router, Route, browserHistory, IndexRoute } from 'react-router';

// Layouts
import MainLayout from './components/main-layout';
import HomeLayout from './components/home-layout';
import SpellsLayout from './components/spells-layout';

// Pages
//import Home from './components/home';
//import UserList from './components/user-list';
//import UserProfile from './components/user-profile';
//import WidgetList from './components/widget-list';


//<Route path=":userId" component={UserProfile} />

export default (
    <Router history={browserHistory}>
        <Route component={MainLayout}>
            <Route path="/" component={LayoutHome} />

            <Route path="spells">
                <Route component={SpellsLayout}>
                    <IndexRoute component={UserList} />
                </Route>
            </Route>

        </Route>
    </Router>
);