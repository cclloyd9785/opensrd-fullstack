import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles'
import { connect } from "react-redux";
import compose from "recompose/compose";
import { setView } from "../../../actions/view";
import { setSection } from "../../../actions/page";
import { userId, authErrors } from "../../../reducers/index";
import FormGameNew from '../../form/FormGameNew';

const styles = theme => ({

});

class LayoutGameNew extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.setSection('games');
        this.props.setView('new');
    }

    render() {
        return (
            <div>
                <FormGameNew />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        errors: authErrors(state),
        user: state.auth.access,
        user_id: userId(state),
        page: {
            game: state.game
        },
        nav: {
            section: state.currentSection,
            view: state.currentView,
        },
        initialValues: state.game,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        setSection: (section) => dispatch(setSection(section)),
        setView: (view) => dispatch(setView(view)),
    }
};


export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
    withStyles(styles, {
            withTheme: true
        },
    ))(LayoutGameNew);