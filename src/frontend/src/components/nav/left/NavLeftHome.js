import React from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router'

import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';

import { dark } from '../../App'
import {authErrors, userId} from "../../../reducers";
import { connect } from 'react-redux'
import compose from 'recompose/compose';
import { Collapse, List, ListItem, ListItemText } from '@material-ui/core';

import {loadSection} from "../../../actions/page";
import {loadView} from "../../../actions/view";
import {logout} from "../../../actions/auth";


const styles = theme => ({
    hidden: {
        display: 'none',
    },
    active: {
        backgroundColor: dark[1],
    },
    navButton: {
        'text-decoration': 'none',
    },
    indentText: {
        paddingLeft: theme.spacing.unit * 5,
        backgroundColor: dark[2],
    }
});



class NavLeftHome extends React.Component {
    state = {
        logout: false,
    };

    constructor(props) {
        super(props);

        this.state = {
            logout: false,
        };

    }

    componentDidMount() {
        this.props.fetchCurrentSection();
        this.props.fetchCurrentView();
    }

    handleLogoutToggle = () => {
        this.setState({
            logout: !this.state.logout,
        });
    };

    logout = () => {
        localStorage.clear();
        this.props.logout();
        window.location.reload();
    };


    render() {
        let { classes } = this.props;


        let buttonLogin = undefined;
        if (!this.props.user_id) {
            buttonLogin =
                <Link className={classes.navButton} to="/login">
                    <ListItem
                        button
                        className={classNames({
                            [classes.active]: this.props.nav.section === 'home' && this.props.nav.view === 'login',
                        })}
                    >
                        <ListItemText>Login</ListItemText>
                    </ListItem>
                </Link>
        }

        let buttonRegister = undefined;
        if (!this.props.user_id) {
            buttonRegister =
                <Link className={classes.navButton} to="/register">
                    <ListItem
                        button
                        className={classNames({
                            [classes.active]: this.props.nav.section === 'home' && this.props.nav.view === 'register',
                        })}
                    >
                        <ListItemText>Register</ListItemText>
                    </ListItem>
                </Link>
        }

        let buttonLogout = undefined;
        let collapseLogout = undefined;
        if (this.props.user_id) {
            buttonLogout =
                <ListItem
                    button
                    onClick={this.handleLogoutToggle}
                    className={classNames({
                        [classes.active]: this.state.logout,
                    })}
                >
                    <ListItemText primary="Logout" />
                </ListItem>

            collapseLogout =
                <Collapse in={this.state.logout} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                        <ListItem button className={classes.indentText}>
                            <ListItemText  onClick={this.logout}>Confirm</ListItemText>
                        </ListItem>
                        <ListItem button className={classes.indentText}>
                            <ListItemText onClick={this.handleLogoutToggle}>Cancel</ListItemText>
                        </ListItem>
                    </List>
                </Collapse>
        }

        return (
            <div>
                {buttonLogin}
                {buttonRegister}
                {buttonLogout}
                {collapseLogout}

            </div>
        );
    }
}


const mapStateToProps = (state) => ({
    errors: authErrors(state),
    user: state.auth.access,
    user_id: userId(state),
    page: {
        spell: state.spell,
    },
    nav: {
        navLeft: state.navLeft,
        navRight: state.navRight,
        section: state.currentSection,
        view: state.currentView,
    }
});

const mapDispatchToProps = (dispatch) => {
    return {
        fetchCurrentSection: () => dispatch(loadSection()),
        fetchCurrentView: () => dispatch(loadView()),
        logout: () => dispatch(logout()),
    }
};

export default compose(
    withRouter,
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
    withStyles(styles, {
            withTheme: true
        }
    ))(NavLeftHome);

