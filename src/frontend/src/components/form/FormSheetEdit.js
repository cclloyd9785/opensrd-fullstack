import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, formValueSelector, getFormValues } from 'redux-form'
import { Grid, TextField, Paper, Typography, Divider, Card, CardHeader, CardContent, Button, IconButton, Checkbox, Tab, Tabs, AppBar, Table, TableBody, TableHead, TableRow, TableCell } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
//import { Grid, Row, Grid item } from 'react-flexbox-grid'
import { dark } from '../App'
import {connect} from "react-redux";
import compose from "recompose/compose";
import {userId, authErrors, accessToken, currentSheet} from "../../reducers";
import {updateSheet} from "../../actions/sheet";
import SaveIcon from '@material-ui/icons/Save';
import { renderCheckbox, renderSelectField, renderTextField, renderModTextField, renderTextFieldCentered } from "./reduxFormMaterialUi";

const debounce = require('debounce');
const ReactMarkdown = require('react-markdown');

const styles = theme => ({
    textInput: {
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    longPaper: {
        padding: theme.spacing.unit * 3,
        backgroundColor: dark[5],
    },
    sheetTitle: {
        fontSize: 48,
        fontFamily: [
            'Balthazar',
        ],
    },
    sheetContents: {
        fontSize: 22,
    },
    row: {
        'padding-top': theme.spacing.unit * 3,
        'padding-bottom': theme.spacing.unit * 3,
    },
    sectionHeader: {
        'font-size': 24,
    },
    sectionPaper: {
        'margin-top': theme.spacing.unit * 3,
        'margin-bottom': theme.spacing.unit * 3,
    },
    sheetCardContent: {
        'padding-left': 0,
        'padding-right': 0,
    },
    tableWrapper: {
    },
    tableStats: {
        width: 'calc(100%)',
        tableLayout: 'fixed',
    },
    abilityScoreInput: {
        width: 40,
    },
});

class FormSheetEdit extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: 1,
            values: null,
        };
    }

    handleChange = (event, value) => {
        this.setState({ value });
    };

    calcMod = (value) => {
        if (value == 0)
            return 0;
        return Math.floor((value - 10)/2);
    };

    render() {
        const {classes} = this.props;
        const { submit, handleSubmit, pristine, reset, submitting } = this.props;
        const { value } = this.state;


        return (
            <form
                onSubmit={handleSubmit(this.props.sendSubmit)}
            >
                <Button
                    variant="fab"
                    color="primary"
                    aria-label="Save"
                    disabled={pristine || submitting}
                    onClick={handleSubmit(this.props.sendSubmit)}
                >
                    <SaveIcon/>
                </Button>

                <Paper className={classes.sectionPaper}>
                        <Tabs value={this.state.value} onChange={this.handleChange}>
                            <Tab label="Character Info" />
                            <Tab label="Stats" />
                            <Tab label="Spells" />
                        </Tabs>
                </Paper>

                {this.state.value === 0 &&
                <Paper className={classes.sectionPaper}>
                    <CardContent>
                        <Typography className={classes.sectionHeader}>Character Info</Typography>
                        <Divider />
                    </CardContent>
                    <CardContent>
                        <Grid container spacing={16}>
                                <Grid item xs={12} lg={3}>
                                    <CardContent className={classes.sheetCardContent}>
                                        <Typography>Character Name</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Name"
                                            component={renderTextField}
                                            label="Character Name"
                                        />
                                    </CardContent>
                                </Grid>
                                <Grid item xs={12} lg={2}>
                                    <CardContent className={classes.sheetCardContent}>
                                        <Typography>Player</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Player"
                                            component={renderTextField}
                                            label="Player"
                                        />
                                    </CardContent>
                                </Grid>
                                <Grid item xs={12} lg={1}>
                                    <CardContent className={classes.sheetCardContent}>
                                        <Typography>Alignment</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Alignment"
                                            component={renderTextField}
                                            label="Alignment"
                                        />
                                    </CardContent>
                                </Grid>

                                <Grid item xs={12} lg={2}>
                                    <CardContent className={classes.sheetCardContent}>
                                        <Typography>Class</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Class"
                                            component={renderTextField}
                                            label="Class"
                                        />
                                    </CardContent>
                                </Grid>

                                <Grid item xs={12} lg={2}>
                                    <CardContent className={classes.sheetCardContent}>
                                        <Typography>Race</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Race"
                                            component={renderTextField}
                                            label="Race"
                                        />
                                    </CardContent>
                                </Grid>

                                <Grid item xs={12} lg={2}>
                                    <CardContent className={classes.sheetCardContent}>
                                        <Typography>Deity</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Deity"
                                            component={renderTextField}
                                            label="Deity"
                                        />
                                    </CardContent>
                                </Grid>


                                <Grid item xs={12} lg={3}>
                                    <CardContent className={classes.sheetCardContent}>
                                        <Typography>Level</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Level"
                                            component={renderTextField}
                                            label="Level"
                                        />
                                    </CardContent>
                                </Grid>

                                <Grid item xs={12} lg={1}>
                                    <CardContent className={classes.sheetCardContent}>
                                        <Typography>Gender</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Gender"
                                            component={renderTextField}
                                            label="Gender"
                                        />
                                    </CardContent>
                                </Grid>

                                <Grid item xs={12} lg={1}>
                                    <CardContent className={classes.sheetCardContent}>
                                        <Typography>Size</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Size"
                                            component={renderTextField}
                                            label="Size"
                                        />
                                    </CardContent>
                                </Grid>

                                <Grid item xs={12} lg={1}>
                                    <CardContent className={classes.sheetCardContent}>
                                        <Typography>Height</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Height"
                                            component={renderTextField}
                                            label="Height"
                                        />
                                    </CardContent>
                                </Grid>

                                <Grid item xs={12} lg={1}>
                                    <CardContent className={classes.sheetCardContent}>
                                        <Typography>Weight</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Weight"
                                            component={renderTextField}
                                            label="Weight"
                                        />
                                    </CardContent>
                                </Grid>

                                <Grid item xs={12} lg={1}>
                                    <CardContent className={classes.sheetCardContent}>
                                        <Typography>Age</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Age"
                                            component={renderTextField}
                                            label="Age"
                                        />
                                    </CardContent>
                                </Grid>

                                <Grid item xs={12} lg={2}>
                                    <CardContent className={classes.sheetCardContent}>
                                        <Typography>Eyes</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Eyes"
                                            component={renderTextField}
                                            label="Eyes"
                                        />
                                    </CardContent>
                                </Grid>

                                <Grid item xs={12} lg={2}>
                                    <CardContent className={classes.sheetCardContent}>
                                        <Typography>Hair</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Hair"
                                            component={renderTextField}
                                            label="Hair"
                                        />
                                    </CardContent>
                                </Grid>

                                <Grid item xs={12} lg={1}>
                                    <CardContent className={classes.sheetCardContent}>
                                        <Typography>Current XP</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="XPCurrent"
                                            component={renderTextField}
                                            label="XPCurrent"
                                        />
                                    </CardContent>
                                </Grid>

                                <Grid item xs={12} lg={1}>
                                    <CardContent className={classes.sheetCardContent}>
                                        <Typography>XP Next</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="XPNext"
                                            component={renderTextField}
                                            label="XPNext"
                                        />
                                    </CardContent>
                                </Grid>

                                <Grid item xs={12} lg={1}>
                                    <CardContent className={classes.sheetCardContent}>
                                        <Typography>XP Change</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="XPChange"
                                            component={renderTextField}
                                            label="XPChange"
                                        />
                                    </CardContent>
                                </Grid>

                                <Grid item xs={12} lg={1}>
                                    <CardContent className={classes.sheetCardContent}>
                                        <Typography>XP Speed</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="XPSpeed"
                                            component={renderTextField}
                                            label="XPSpeed"
                                        />
                                    </CardContent>
                                </Grid>

                        </Grid>
                    </CardContent>
                </Paper>}

                {this.state.value === 1 &&
                <Paper className={classes.sectionPaper}>
                    <CardContent>
                        <Typography className={classes.sectionHeader}>Stats</Typography>
                        <Divider />
                    </CardContent>
                    <CardContent>
                        <Grid container >
                            <Grid item xs={3} lg={3} className={classes.tableWrapper}>
                                <Table className={classes.tableStats}>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Ability</TableCell>
                                            <TableCell>Score</TableCell>
                                            <TableCell>Mod</TableCell>
                                            <TableCell>Temp</TableCell>
                                            <TableCell>Temp Mod</TableCell>
                                        </TableRow>
                                    </TableHead>

                                    <TableBody>
                                        <TableRow>
                                            <TableCell>
                                                STR
                                            </TableCell>
                                            <TableCell>
                                                <Field
                                                    className={classes.abilityScoreInput}
                                                    center
                                                    name="StrScore"
                                                    component={renderTextFieldCentered}
                                                    label="StrScore"
                                                    onChange={debounce(e => {
                                                        this.props.change('StrMod', this.calcMod(e.target.value))
                                                    }, 250)}
                                                />
                                            </TableCell>
                                            <TableCell>
                                                <Field
                                                    className={classes.abilityScoreInput}
                                                    name="StrMod"
                                                    component={renderModTextField}
                                                    label="StrMod"
                                                />
                                            </TableCell>
                                            <TableCell>
                                                <Field
                                                    className={classes.abilityScoreInput}
                                                    name="StrTemp"
                                                    component={renderTextFieldCentered}
                                                    label="StrTemp"
                                                    onChange={debounce(e => {
                                                        this.props.change('StrTempMod', this.calcMod(e.target.value))
                                                    }, 250)}
                                                />
                                            </TableCell>
                                            <TableCell>
                                                <Field
                                                    className={classes.abilityScoreInput}
                                                    name="StrTempMod"
                                                    component={renderModTextField}
                                                    label="StrTempMod"
                                                />
                                            </TableCell>
                                        </TableRow>

                                        <TableRow>
                                            <TableCell>
                                                DEX
                                            </TableCell>
                                            <TableCell>
                                                <Field
                                                    className={classes.abilityScoreInput}
                                                    center
                                                    name="DexScore"
                                                    component={renderTextFieldCentered}
                                                    label="DexScore"
                                                    onChange={debounce(e => {
                                                        this.props.change('DexMod', this.calcMod(e.target.value))
                                                    }, 250)}
                                                />
                                            </TableCell>
                                            <TableCell>
                                                <Field
                                                    className={classes.abilityScoreInput}
                                                    name="DexMod"
                                                    component={renderModTextField}
                                                    label="DexMod"
                                                />
                                            </TableCell>
                                            <TableCell>
                                                <Field
                                                    className={classes.abilityScoreInput}
                                                    name="DexTemp"
                                                    component={renderTextFieldCentered}
                                                    label="DexTemp"
                                                    onChange={debounce(e => {
                                                        this.props.change('DexTempMod', this.calcMod(e.target.value))
                                                    }, 250)}
                                                />
                                            </TableCell>
                                            <TableCell>
                                                <Field
                                                    className={classes.abilityScoreInput}
                                                    name="DexTempMod"
                                                    component={renderModTextField}
                                                    label="DexTempMod"
                                                />
                                            </TableCell>
                                        </TableRow>

                                        <TableRow>
                                            <TableCell>
                                                CON
                                            </TableCell>
                                            <TableCell>
                                                <Field
                                                    className={classes.abilityScoreInput}
                                                    center
                                                    name="ConScore"
                                                    component={renderTextFieldCentered}
                                                    label="ConScore"
                                                    onChange={debounce(e => {
                                                        this.props.change('ConMod', this.calcMod(e.target.value))
                                                    }, 250)}
                                                />
                                            </TableCell>
                                            <TableCell>
                                                <Field
                                                    className={classes.abilityScoreInput}
                                                    name="ConMod"
                                                    component={renderModTextField}
                                                    label="ConMod"
                                                />
                                            </TableCell>
                                            <TableCell>
                                                <Field
                                                    className={classes.abilityScoreInput}
                                                    name="ConTemp"
                                                    component={renderTextFieldCentered}
                                                    label="ConTemp"
                                                    onChange={debounce(e => {
                                                        this.props.change('ConTempMod', this.calcMod(e.target.value))
                                                    }, 250)}
                                                />
                                            </TableCell>
                                            <TableCell>
                                                <Field
                                                    className={classes.abilityScoreInput}
                                                    name="ConTempMod"
                                                    component={renderModTextField}
                                                    label="ConTempMod"
                                                />
                                            </TableCell>
                                        </TableRow>

                                        <TableRow>
                                            <TableCell>
                                                INT
                                            </TableCell>
                                            <TableCell>
                                                <Field
                                                    className={classes.abilityScoreInput}
                                                    center
                                                    name="IntScore"
                                                    component={renderTextFieldCentered}
                                                    label="IntScore"
                                                    onChange={debounce(e => {
                                                        this.props.change('IntMod', this.calcMod(e.target.value))
                                                    }, 250)}
                                                />
                                            </TableCell>
                                            <TableCell>
                                                <Field
                                                    className={classes.abilityScoreInput}
                                                    name="IntMod"
                                                    component={renderModTextField}
                                                    label="IntMod"
                                                />
                                            </TableCell>
                                            <TableCell>
                                                <Field
                                                    className={classes.abilityScoreInput}
                                                    name="IntTemp"
                                                    component={renderTextFieldCentered}
                                                    label="IntTemp"
                                                    onChange={debounce(e => {
                                                        this.props.change('IntTempMod', this.calcMod(e.target.value))
                                                    }, 250)}
                                                />
                                            </TableCell>
                                            <TableCell>
                                                <Field
                                                    className={classes.abilityScoreInput}
                                                    name="IntTempMod"
                                                    component={renderModTextField}
                                                    label="IntTempMod"
                                                />
                                            </TableCell>
                                        </TableRow>

                                        <TableRow>
                                            <TableCell>
                                                WIS
                                            </TableCell>
                                            <TableCell>
                                                <Field
                                                    className={classes.abilityScoreInput}
                                                    center
                                                    name="WisScore"
                                                    component={renderTextFieldCentered}
                                                    label="WisScore"
                                                    onChange={debounce(e => {
                                                        this.props.change('WisMod', this.calcMod(e.target.value))
                                                    }, 250)}
                                                />
                                            </TableCell>
                                            <TableCell>
                                                <Field
                                                    className={classes.abilityScoreInput}
                                                    name="WisMod"
                                                    component={renderModTextField}
                                                    label="WisMod"
                                                />
                                            </TableCell>
                                            <TableCell>
                                                <Field
                                                    className={classes.abilityScoreInput}
                                                    name="WisTemp"
                                                    component={renderTextFieldCentered}
                                                    label="WisTemp"
                                                    onChange={debounce(e => {
                                                        this.props.change('WisTempMod', this.calcMod(e.target.value))
                                                    }, 250)}
                                                />
                                            </TableCell>
                                            <TableCell>
                                                <Field
                                                    className={classes.abilityScoreInput}
                                                    name="WisTempMod"
                                                    component={renderModTextField}
                                                    label="WisTempMod"
                                                />
                                            </TableCell>
                                        </TableRow>

                                        <TableRow>
                                            <TableCell>
                                                CHA
                                            </TableCell>
                                            <TableCell>
                                                <Field
                                                    className={classes.abilityScoreInput}
                                                    center
                                                    name="ChaScore"
                                                    component={renderTextFieldCentered}
                                                    label="ChaScore"
                                                    onChange={debounce(e => {
                                                        this.props.change('ChaMod', this.calcMod(e.target.value))
                                                    }, 250)}
                                                />
                                            </TableCell>
                                            <TableCell>
                                                <Field
                                                    className={classes.abilityScoreInput}
                                                    name="ChaMod"
                                                    component={renderModTextField}
                                                    label="ChaMod"
                                                />
                                            </TableCell>
                                            <TableCell>
                                                <Field
                                                    className={classes.abilityScoreInput}
                                                    name="ChaTemp"
                                                    component={renderTextFieldCentered}
                                                    label="ChaTemp"
                                                    onChange={debounce(e => {
                                                        this.props.change('ChaTempMod', this.calcMod(e.target.value))
                                                    }, 250)}
                                                />
                                            </TableCell>
                                            <TableCell>
                                                <Field
                                                    className={classes.abilityScoreInput}
                                                    name="ChaTempMod"
                                                    component={renderModTextField}
                                                    label="ChaTempMod"
                                                />
                                            </TableCell>
                                        </TableRow>


                                    </TableBody>
                                </Table>
                            </Grid>
                        </Grid>
                    </CardContent>
                </Paper>}



            </form>
        );
    }
}

FormSheetEdit.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
    return {
        errors: authErrors(state),
        user: state.auth.access,
        user_id: userId(state),
        page: {
            sheet: state.sheet
        },
        nav: {
            section: state.currentSection,
            view: state.currentView,
        },
        initialValues: state.sheet,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        sendSubmit: (values) => dispatch(updateSheet(values)),
    }
};


export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
    reduxForm({
        form: 'FormSheetEdit',
        enableReinitialize: true
    }),
    withStyles(styles, {
            withTheme: true
        },
    ))(FormSheetEdit);


