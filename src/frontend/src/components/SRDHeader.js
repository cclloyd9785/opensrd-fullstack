import React, { Component } from 'react';

import { withStyles } from '@material-ui/core/styles'
import { dark } from './App'
import { Typography } from '@material-ui/core'
import Divider from '@material-ui/core/Divider';


const styles = theme => ({
    row: {
        'padding-top': theme.spacing.unit * 3,
        'padding-bottom': theme.spacing.unit * 3,
    },
    divider: {
    },
    sectionTop: {
        'margin-top': theme.spacing.unit * 4,
    },
    sectionBottom: {
        'margin-bottom': theme.spacing.unit * 1,
    },
    sectionText: {
        'background-color': dark[5],
        'padding-left': theme.spacing.unit * 1,
        fontSize: 28,
    },
});



class SRDHeader extends Component {

    constructor(props) {
        super(props);

        this.state = {
            value: '',
            spacing: '16',
        };
    }



    render() {
        const {classes, title} = this.props;
        //const { spacing } = this.state;

        return (
            <div>
                <Divider className={classes.sectionTop}/>
                <Typography className={classes.sectionText} variant="display1">{title}</Typography>
                <Divider className={classes.sectionBottom}/>
            </div>
        );
    }
} export default withStyles(styles, { withTheme: true })(SRDHeader);