import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router'
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import AppContent from './AppContent'
import { authErrors } from "../reducers";
import { connect } from 'react-redux'
import compose from 'recompose/compose';
import TopBar   from './nav/TopBar';
import NavLeft  from './nav/left/NavLeft';
import NavTools from './nav/NavRight';
import {isMobile} from "../actions/mobile";


const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    appFrame: {
        height: '100%',
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%',
    },
    hide: {
        display: 'none',
    },
    hidden: {
        display: 'none',
    },
});



class MaterialUiApp extends React.Component {
    constructor(props) {
        super(props);

    }

    componentDidMount() {
        const mobile = window.innerWidth <= 1024;
        this.props.setMobile(mobile)
    }

    render() {
        const { classes } = this.props;

        return (
            <div className={classes.root}>
                    <NavLeft />
                    <TopBar />
                    <AppContent />
                    <NavTools />
            </div>
        );
    }
}

MaterialUiApp.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
    errors: authErrors(state),
    user: state.auth.access,
    page: {
        type: 'spell',
        spell: state.spell,
    },
    device: {
        mobile: state.mobile,
    },
    nav: {
        navLeft: state.navLeft,
        navRight: state.navRight,
    }
});

const mapDispatchToProps = (dispatch) => {
    return {
        setMobile: (mobile) => dispatch(isMobile(mobile)),
    }
};


export default compose(
    withRouter,
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
    withStyles(styles, {
            withTheme: true
        }
    ))(MaterialUiApp);
