import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import auth, * as fromAuth from './auth'
import user, * as fromUser from './user'
import echo, * as fromEcho from './echo'

import spell, * as fromSpell from './spell'
import feat, * as fromFeat from './feat'
import artifact, * as fromArtifact from './artifact'
import sheet, * as fromSheet from './sheet'
import game, * as fromGame from './game'
import chat, * as fromChat from './chat'

import mobile, * as fromMobile from './mobile'
import navLeft, * as fromNavLeft from './navLeft'
import navRight, * as fromNavRight from './navRight'
import currentSection, * as fromCurrentPage from './page'
import currentView from './view'
import { reducer as form  } from 'redux-form';


export default combineReducers({
    auth: auth,
    echo: echo,
    user: user,
    spell: spell,
    feat: feat,
    artifact: artifact,
    sheet: sheet,
    game: game,
    chat: chat,
    mobile: mobile,
    navLeft: navLeft,
    navRight: navRight,
    currentSection: currentSection,
    currentView: currentView,
    form: form,
    router: routerReducer,
});


export const isAuthenticated =
    state => fromAuth.isAuthenticated(state.auth);
export const accessToken =
    state => fromAuth.accessToken(state.auth);
export const isAccessTokenExpired =
    state => fromAuth.isAccessTokenExpired(state.auth);
export const refreshToken =
    state => fromAuth.refreshToken(state.auth);
export const isRefreshTokenExpired =
    state => fromAuth.isRefreshTokenExpired(state.auth);
export const authErrors =
    state => fromAuth.errors(state.auth);

export function withAuth(headers={}) {
    return (state) => ({
        ...headers,
        'Authorization': `Bearer ${accessToken(state)}`
    })
}

export const userId =
    state => fromAuth.userId(state.auth);

export const currentUser =
    state => fromUser.loadUser(state.user);

export const currentSpell =
    state => fromSpell.loadSpell(state.spell);

export const currentFeat =
    state => fromFeat.loadFeat(state.feat);


/*
export const recentSpells =
    state => fromSpell.loadRecentSpells(state.spells);
*/
export const getCurrentPage =
    state => fromCurrentPage.getSection(state);

/*export const getCurrentView =
    state => fromCurrentPage.getView(state);
*/


