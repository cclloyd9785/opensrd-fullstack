from rest_framework import viewsets, mixins
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework.decorators import action
import json

from .models import ChatSession
from chatmessages.models import ChatMessage
from .serializers import ChatSessionSerializer
from chatmessages.serializers import ChatMessageSerializer


class ChatSessionViewSet(
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
    ):

    queryset = ChatSession.objects.all()
    serializer_class = ChatSessionSerializer

    @action(methods=['get'], detail=True)
    def messages(self, request, **id):
        chatsession = self.get_object()
        messages = ChatMessage.objects.filter(session=chatsession.id)

        if not chatsession.id:
            error = dict()
            error['error'] = True
            error['message'] = "Not found"
            return Response(error, status=404)

        serializer = ChatMessageSerializer(messages, many=True)
        return Response(serializer.data)

    @action(methods=['get'], detail=True)
    def join(self, request, **id):
        session = self.get_object()
        if not session.id:
            error = dict()
            error['error'] = True
            error['message'] = "Not found"
            return Response(error, status=404)

        users = json.loads(session.Users)
        for u in users:
            if u == request.user.id:
                serializer = ChatSessionSerializer(session, many=False)
                return Response(serializer.data)

        # If user is not allowed to join (check here), then return 403 error with error dict

        users.append(request.user.id)
        session.Users = json.dumps(users)

        session.save()

        serializer = ChatSessionSerializer(session, many=False)
        return Response(serializer.data)

    @action(methods=['get'], detail=True)
    def connect(self, request, **id):
        session = self.get_object()
        if not session.id:
            error = dict()
            error['error'] = True
            error['message'] = "Not found"
            return Response(error, status=404)

        users = json.loads(session.Users)
        for u in users:
            if u == request.user.id:
                serializer = ChatSessionSerializer(session, many=False)
                return Response(serializer.data)
