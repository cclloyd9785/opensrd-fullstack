import { RSAA } from 'redux-api-middleware';
import { withAuth } from '../reducers';

export const FETCH_SPELL = '@@spell/FETCH_SPELL';
export const FETCH_SPELL_SUCCESS = '@@spell/FETCH_SPELL_SUCCESS';
export const FETCH_SPELL_FAILURE = '@@spell/FETCH_SPELL_FAILURE';

export const UPDATE_SPELL = '@@spell/UPDATE_SPELL';
export const UPDATE_SPELL_SUCCESS = '@@spell/UPDATE_SPELL_SUCCESS';
export const UPDATE_SPELL_FAILURE = '@@spell/UPDATE_SPELL_FAILURE';

export const CREATE_SPELL = '@@spell/CREATE_SPELL';
export const CREATE_SPELL_SUCCESS = '@@spell/CREATE_SPELL_SUCCESS';
export const CREATE_SPELL_FAILURE = '@@spell/CREATE_SPELL_FAILURE';

export const FETCH_RECENT_SPELLS = '@@spell/FETCH__RECENT_SPELLS';
export const FETCH_RECENT_SPELLS_SUCCESS = '@@spell/FETCH_RECENT_SPELLS_SUCCESS';
export const FETCH_RECENT_SPELLS_FAILURE = '@@spell/FETCH_RECENT_SPELLS_FAILURE';

export const FETCH_MY_SPELLS = '@@spell/FETCH__MY_SPELLS';
export const FETCH_MY_SPELLS_SUCCESS = '@@spell/FETCH_MY_SPELLS_SUCCESS';
export const FETCH_MY_SPELLS_FAILURE = '@@spell/FETCH_MY_SPELLS_FAILURE';


export const updateSpell = (spell) => ({
    [RSAA]: {
        endpoint: '/api/spells/' + spell.id + '/',
        method: 'PUT',
        body: JSON.stringify(spell),
        //body: spell,
        headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            UPDATE_SPELL, UPDATE_SPELL_SUCCESS, UPDATE_SPELL_FAILURE
        ]
    }
});

export const createSpell = (spell) => ({
    [RSAA]: {
        endpoint: '/api/spells/',
        method: 'POST',
        body: JSON.stringify(spell),
        headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            CREATE_SPELL, CREATE_SPELL_SUCCESS, CREATE_SPELL_FAILURE
        ]
    }
});

export const loadSpell = (spellId) => ({
    [RSAA]: {
        endpoint: '/api/spells/' + spellId + '/',
        method: 'GET',
        //headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            FETCH_SPELL, FETCH_SPELL_SUCCESS, FETCH_SPELL_FAILURE
        ]
    }
});

export const loadRecentSpells = (page) => ({
    [RSAA]: {
        endpoint: '/api/spells/newest/',
        method: 'GET',
        //headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            FETCH_RECENT_SPELLS, FETCH_RECENT_SPELLS_SUCCESS, FETCH_RECENT_SPELLS_FAILURE
        ]
    }
});

export const loadMySpells = (page) => ({
    [RSAA]: {
        endpoint: '/api/spells/mine/',
        method: 'GET',
        headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            FETCH_MY_SPELLS, FETCH_MY_SPELLS_SUCCESS, FETCH_MY_SPELLS_FAILURE
        ]
    }
});
