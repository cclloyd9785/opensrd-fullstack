import * as view_action from '../actions/view'
import {isRefreshTokenExpired} from "./auth";


const initialState = 'home';

export default (state=initialState, action) => {
    switch(action.type) {
        case view_action.SET_VIEW:
            return action.payload;
        case view_action.GET_VIEW:
            return state;
        default:
            return state;
    }
}
export function getView(state) {
    if (state.currentView) {
        return state.currentView;
    }
}
