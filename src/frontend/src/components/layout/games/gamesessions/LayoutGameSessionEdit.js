import React, { Component } from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles'
import { dark } from '../../../App'
import {connect} from "react-redux";
import compose from "recompose/compose";
import { loadGame, loadGameSession } from "../../../../actions/game";
import { setView } from "../../../../actions/view";
import { setSection} from "../../../../actions/page";
import {authErrors, isAuthenticated, userId} from "../../../../reducers/index";
import {
    TextField,
    Grid, GridList, GridListTile,
    Paper,
    Typography,
    Divider,
    Card,
    CardHeader,
    CardContent,
    Button,
    IconButton,
    Checkbox,
    TableRow, TableCell,
    Hidden,
} from '@material-ui/core'
import {Link} from "react-router-dom";

import {appBarHeight} from '../../../App'
import ChatMessage from "./ChatMessage";
import {
    createChatSession,
    joinChatSession,
    loadChatSession,
    loadRecentChatMessages, pusherAddMessageToList,
    pusherSetChatMessageList, pusherSetCurrentRoom, pusherSetCurrentUser
} from "../../../../actions/chat";
import FormChat from "../../../form/FormChat";
import {login} from "../../../../actions/auth";
import {loadUser} from "../../../../actions/user";
import { ChatManager, TokenProvider } from '@pusher/chatkit'
import Chatkit from '@pusher/chatkit'
import Pusher from "pusher-js";
import FormFeatEdit from "../../../form/FormFeatEdit";
import FormGameSessionEdit from "../../../form/FormGameSessionEdit";

Pusher.logToConsole = true;


const ReactMarkdown = require('react-markdown');
const Color = require('color');

const chatInputWrapperHeight = 100;

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    sectionPaper: {
        marginBottom: theme.spacing.unit * 3,
    },
    chatContainer: {
        width: '100%',
        height: '100%',
    },
    full: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        top: 0,
        left: 0,
    },
    fullContent: {
        width: '100%',
        height: '100%',
        position: 'relative',
    },
    fullWidth: {
        width: '100%',
        maxWidth: '100%',
    },
    fullHeight: {
        height: '100%',
    },
    scrollable: {
        overflowX: 'hidden',
        overflowY: 'scroll'
    },
    chatMessages: {
        alignItems: 'flex-end',
    },
    chatInput: {
        backgroundColor: '#' + Color(dark[3]).lighten(0.00).rgbNumber().toString(16),
    },
});




class LayoutGameSessionEdit extends Component {
    constructor(props) {
        super(props);
    }


    componentDidMount() {
        const gameId = this.props.match.params.gameId;
        const sessionId = this.props.match.params.sessionId;

        this.props.setSection('games');
        this.props.setView('edit');


    }

    render() {
        const {classes} = this.props;

        const gameId = this.props.match.params.gameId;
        const sessionId = this.props.match.params.sessionId;

        return (
            <div>
                <FormGameSessionEdit />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        errors: authErrors(state),
        isAuthenticated: isAuthenticated(state),
        user: state.user,
        device: {
            mobile: state.mobile,
        },
        page: {
            game: state.game,
            //chat: state.chat,
        },
        nav: {
            section: state.currentSection,
            view: state.currentView,
        },
    }
};



const mapDispatchToProps = (dispatch) => {
    return {
        setSection: (section) => dispatch(setSection(section)),
        setView: (view) => dispatch(setView(view)),
    }
};


export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),
    withStyles(styles, {
            withTheme: true
        }
    ))(LayoutGameSessionEdit);