import React from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router'

import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';

import { dark } from '../../App'
import {authErrors, userId} from "../../../reducers";
import { connect } from 'react-redux'
import compose from 'recompose/compose';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import { ExpandLess, ExpandMore } from '@material-ui/icons';

import {loadSection} from "../../../actions/page";
import {loadView} from "../../../actions/view";


const styles = theme => ({
    hidden: {
        display: 'none',
    },

    navButton: {
        'text-decoration': 'none',
    },
    indentText: {
        paddingLeft: theme.spacing.unit * 5,
        backgroundColor: dark[2],
    },
    active: {
        backgroundColor: dark[1],
    },
});



class NavLeftFeats extends React.Component {
    state = {
        open: false,
    };

    constructor(props) {
        super(props);

        this.state = {
            open: false,
        };

    }

    componentDidMount() {
        let regex = /\/(feats)\//;
        let location = this.props.location.pathname;
        let matches = location.match(regex);
        let featsSection = false;
        if (matches)
            featsSection = true;

        this.setState({
            open: featsSection,
        });
        this.props.fetchCurrentSection();
        this.props.fetchCurrentView();
    }

    handleToggle = () => {
        this.setState({
            open: !this.state.open,
        });
    };


    render() {
        let { classes } = this.props;

        let buttonView = undefined;
        if (this.props.nav.section === 'feats') {
            if (this.props.nav.view === 'view' || 'edit') {
                if (this.props.page.feat.model.id) {
                    buttonView =
                        <Link className={classes.navButton} to={"/feats/" + this.props.page.feat.model.id}>
                            <ListItem
                                button
                                className={classNames(classes.indentText, {
                                    [classes.active]: this.props.nav.section === 'feats' && this.props.nav.view === 'view',
                                })}
                            >
                                <ListItemText
                                    className={classes.navButton}>{this.props.page.feat.model.Name}</ListItemText>
                            </ListItem>
                        </Link>
                }
            }
        }

        let buttonEdit = undefined;
        if (this.props.nav.section === 'feats') {
            if (this.props.nav.view === 'view' || 'edit') {
                if (this.props.page.feat.model.owner) {
                    if (this.props.page.feat.model.owner === this.props.user_id) {
                        buttonEdit =
                            <Link className={classes.navButton} to={"/feats/" + this.props.page.feat.model.id + "/edit"}>
                                <ListItem
                                    button
                                    className={classNames(classes.indentText, {
                                        [classes.active]: this.props.nav.section === 'feats' && this.props.nav.view === 'edit',
                                    })}
                                >
                                    <ListItemText className={classes.navButton}>Edit {this.props.page.feat.model.Name}</ListItemText>
                                </ListItem>
                            </Link>
                    }
                }
            }
        }

        let buttonMine = undefined;
        if (this.props.user_id) {
            buttonMine =
                <Link className={classes.navButton} to={"/feats/mine"}>
                    <ListItem
                        button
                        className={classNames(classes.indentText, {
                            [classes.active]: this.props.nav.section === 'feats' && this.props.nav.view === 'mine',
                        })}
                    >
                        <ListItemText className={classes.navButton}>My Feats</ListItemText>
                    </ListItem>
                </Link>
        }

        let buttonNew = undefined;
        buttonNew =
            <Link className={classes.navButton} to={"/feats/new"}>
                <ListItem
                    button
                    className={classNames(classes.indentText, {
                        [classes.active]: this.props.nav.section === 'feats' && this.props.nav.view === 'new',
                    })}
                >
                    <ListItemText className={classes.navButton}>Create new feat</ListItemText>
                </ListItem>
            </Link>;

        return (
            <div>
                <ListItem
                    button
                    onClick={this.handleToggle}
                    className={classNames({
                        [classes.active]: this.state.open,
                    })}
                >
                    <ListItemText primary="Feats" />
                    {this.state.open ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={this.state.open} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                        <Link className={classes.navButton} to="/feats/recent">
                            <ListItem
                                button
                                className={classNames(classes.indentText, {
                                    [classes.active]: this.props.nav.section === 'feats' && this.props.nav.view === 'all',
                                })}
                            >
                                <ListItemText>All Feats</ListItemText>
                            </ListItem>
                        </Link>
                        {buttonNew}
                        {buttonMine}
                        {buttonView}
                        {buttonEdit}
                    </List>
                </Collapse>
            </div>
        );
    }
}


const mapStateToProps = (state) => ({
    errors: authErrors(state),
    user: state.auth.access,
    user_id: userId(state),
    page: {
        feat: state.feat,
    },
    nav: {
        navLeft: state.navLeft,
        navRight: state.navRight,
        section: state.currentSection,
        view: state.currentView,
    }
});

const mapDispatchToProps = (dispatch) => {
    return {
        fetchCurrentSection: () => dispatch(loadSection()),
        fetchCurrentView: () => dispatch(loadView()),
    }
};

export default compose(
    withRouter,
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
    withStyles(styles, {
            withTheme: true
        }
    ))(NavLeftFeats);

