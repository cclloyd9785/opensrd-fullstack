import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router'

import FormRegister from '../form/FormRegister'
import {login} from  '../../actions/auth'
import {authErrors, isAuthenticated} from '../..//reducers'
import {withStyles} from "@material-ui/core/styles/index";
import compose from "recompose/compose";
import {setSection} from "../../actions/page";
import {setView} from "../../actions/view";

const styles = theme => ({

});

class LayoutRegister extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.setSection('home');
        this.props.setView('register');
    }

    render() {
        return (
            <div className="login-page">
                <div>Register an account here. Currently there is no feedback if it worked or not, but you can attempt
                    to log in immediately afterwards if it was successful.
                </div>
                <FormRegister {...this.props}/>
            </div>
        )
    }
};

const mapStateToProps = (state) => ({
    errors: authErrors(state),
    isAuthenticated: isAuthenticated(state)
});

const mapDispatchToProps = (dispatch) => ({
    setSection: (section) => dispatch(setSection(section)),
    setView: (view) => dispatch(setView(view)),
});

export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),
    withStyles(styles, {
            withTheme: true
        }
    ))(LayoutRegister);