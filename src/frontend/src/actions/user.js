import { RSAA } from 'redux-api-middleware';
import { withAuth } from '../reducers';
import axios from 'axios';

export const FETCH_USER = '@@user/FETCH_USER';
export const FETCH_USER_SUCCESS = '@@user/FETCH_USER_SUCCESS';
export const FETCH_USER_FAILURE = '@@user/FETCH_USER_FAILURE';

export const loadUser = () => ({
    [RSAA]: {
        endpoint: '/api/user/info',
        method: 'GET',
        headers: withAuth({}),
        types: [
            FETCH_USER, FETCH_USER_SUCCESS, FETCH_USER_FAILURE
        ]
    }
});
