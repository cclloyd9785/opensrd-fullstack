from rest_framework import viewsets, mixins
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework.decorators import action

from .serializers import CharacterSerializer
from .models import Character


class CharacterViewSet(
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
    ):

    queryset = Character.objects.all()
    serializer_class = CharacterSerializer

    @action(methods=['get'], detail=False)
    def newest(self, request):
        queryset = Character.objects.order_by('-id')[:20]
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(methods=['get'], detail=False)
    def oldest(self, request):
        queryset = Character.objects.order_by('id')[:20]
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(methods=['get', 'post'], detail=False)
    def mine(self, request):
        if request.method == 'POST':
            queryset = Character.objects.filter(owner=request.POST.get('id'))
            page = self.paginate_queryset(queryset)
            if page is not None:
                serializer = self.get_serializer(page, many=True)
                return self.get_paginated_response(serializer.data)
        queryset = Character.objects.filter(owner=1)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
