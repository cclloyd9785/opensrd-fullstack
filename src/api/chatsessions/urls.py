from rest_framework import routers
from .viewsets import ChatSessionViewSet

router = routers.SimpleRouter()

router.register(r'^api/chatsessions', ChatSessionViewSet, 'chatsessions')
