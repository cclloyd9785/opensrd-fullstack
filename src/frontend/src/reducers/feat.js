import * as feat_action from '../actions/feat'

const initialState = {
    model: {},
    list: [],
};

export default (state=initialState, action) => {
    switch(action.type) {
        case feat_action.FETCH_FEAT_SUCCESS:
            return {
                model: action.payload,
                list: [],
            };
        case feat_action.UPDATE_FEAT_SUCCESS:
            return {
                model: action.payload,
                list: [],
            };
        case feat_action.CREATE_FEAT_SUCCESS:
            return {
                model: action.payload,
                list: [],
            };
        case feat_action.FETCH_RECENT_FEATS_SUCCESS:
            return {
                model: {},
                list: action.payload,
            };
        case feat_action.FETCH_MY_FEATS_SUCCESS:
            return {
                model: {},
                list: action.payload,
            };
        default:
            return state;
    }
}

export function loadFeat(state) {
    if (state.feat) {
        return state.feat
    }
}
