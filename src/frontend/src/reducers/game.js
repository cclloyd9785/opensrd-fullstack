import * as game_action from '../actions/game'
import {CLEAR_GAME_SESSIONS_SUCCESS} from "../actions/game";

const initialState = {
    model: {},
    list: [],
    sessions: [],
    currentSession: {
        id: 0,
        Summary: '',
        Description: '',
    },
};

export default (state=initialState, action) => {
    switch(action.type) {
        case game_action.FETCH_GAME_SUCCESS:
            return {
                ...state,
                model: action.payload,
                list: [],
                sessions: [],
            };
        case game_action.CLEAR_GAME_SESSIONS_SUCCESS:
            return {
                ...state,
                sessions: [],
            };
        case game_action.UPDATE_GAME_SUCCESS:
            return {
                ...state,
                model: action.payload,
                list: [],
                sessions: [],
            };
        case game_action.CREATE_GAME_SUCCESS:
            return {
                ...state,
                model: action.payload,
                list: [],
                sessions: [],
            };
        case game_action.FETCH_RECENT_GAMES_SUCCESS:
            return {
                ...state,
                list: action.payload,
                sessions: [],
            };
        case game_action.FETCH_MY_GAMES_SUCCESS:
            return {
                ...state,
                list: action.payload,
            };
        case game_action.FETCH_GAME_SESSIONS_SUCCESS:
            return {
                ...state,
                sessions: action.payload,
            };
        case game_action.CREATE_GAME_SESSION_SUCCESS:
            return {
                ...state,
                sessions: [
                    ...state.sessions,
                    action.payload,
                ],
                currentSession: action.payload,
            };
        case game_action.FETCH_GAME_SESSION_SUCCESS:
            return {
                ...state,
                currentSession: action.payload
            };
        default:
            return state;
    }
}

export function loadGame(state) {
    if (state.game) {
        return state.game
    }
}
