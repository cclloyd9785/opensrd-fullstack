import React from 'react';
import { withRouter } from 'react-router'

import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';

import { Toolbar, Typography} from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { dark } from '../App'
import {authErrors} from "../../reducers";
import { connect } from 'react-redux'
import compose from 'recompose/compose';

import {switchNavLeft} from "../../actions/navLeft";
import TopBarRight from './TopBarRight';

const drawerWidth = 200;
const drawerWidthTools=300;

const styles = theme => ({
    menuButton: {
        marginLeft: 12,
        marginRight: 20,
    },
    hide: {
        display: 'none',
    },
    nested: {
        paddingLeft: theme.spacing.unit * 4,
    },
    hidden: {
        display: 'none',
    },
    brandLogo: {
        width: '175px',
        height: 60,
        position: 'relative',
        overflow: 'hidden',
        'pointer-events': 'none',
        'z-index': '0 !important',
    },
    brandLogoImage: {
        width: '100%',
        height: 'auto',
        filter: 'brightness(180%)',
        position: 'absolute',
        top: '-115%',
        left: '0%',
    },
    topBarRight: {
        position: 'absolute',
        right: 0,
    }
});



class TopBarLeft extends React.Component {
    state = {
        mobile: false,
        navLeft: true,
    };

    constructor(props) {
        super(props);

        const mobile = (window.innerWidth <= 1024);

        this.state = {
            navLeft: !mobile,
        }
    }

    componentDidMount() {
        this.props.toggleNavLeft(this.state.navLeft);
    }

    toggleNavLeft = () => {
        const { nav } = this.props;
        const newState = !nav.navLeft;
        this.setState({ navLeft: newState });
        this.props.toggleNavLeft(newState);
    };


    render() {
        const { classes } = this.props;

        return (
            <Toolbar className={classNames(classes.topBar)}>
                <IconButton
                    color="inherit"
                    aria-label="open drawer"
                    onClick={this.toggleNavLeft}
                    className={classNames(classes.menuButton)}
                >
                    <MenuIcon />
                </IconButton>
                <div className={classNames(classes.brandLogo)}>
                    <img className={classNames(classes.brandLogoImage)} src={'/logo.png'} />
                </div>
                <div className={classNames(classes.topBarRight)}>
                    <TopBarRight />
                </div>
            </Toolbar>
        );
    }
}

const mapStateToProps = (state) => ({
    errors: authErrors(state),
    user: state.auth.access,
    page: {
        type: 'spell',
        spell: state.spell,
    },
    device: {
        mobile: state.mobile,
    },
    nav: {
        navLeft: state.navLeft,
    }
});

const mapDispatchToProps = (dispatch) => {
    return {
        toggleNavLeft: (newState) => dispatch(switchNavLeft(newState)),
    }
};


export default compose(
    withRouter,
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
    withStyles(styles, {
            withTheme: true
        }
    ))(TopBarLeft);


