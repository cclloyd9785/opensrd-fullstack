import React, { Component } from 'react';
import {authErrors, isAuthenticated, withAuth, userId} from '../../../reducers/index'

import { withStyles } from '@material-ui/core/styles'
import {connect} from "react-redux";
import compose from "recompose/compose";
import { loadRecentArtifacts } from "../../../actions/artifact";
import { setSection } from "../../../actions/page";
import { Table, TableRow, TableBody, TableCell, TableHead, Paper } from '@material-ui/core';
import { Link } from 'react-router-dom';
import {setView} from "../../../actions/view";

const styles = theme => ({
    tableLink: {
        color: theme.palette.primary.contrastText,
        fontSize: 16,
        'text-decoration': 'none',
    },
});



class LayoutArtifactAll extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.setSection('artifacts');
        this.props.setView('all');
        this.props.fetchRecentArtifacts(1);
    }

    render() {
        const {classes} = this.props;

        return (
            <Paper className={classes.root}>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell>Name</TableCell>
                            <TableCell>Prerequisites</TableCell>
                            <TableCell>Benefit</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.props.page.artifact.list.map(n => {
                            return (
                                <TableRow key={n.id}>
                                    <TableCell component="th" scope="row">
                                        <Link className={classes.tableLink} to={"/artifacts/"+n.id}>{n.Name}</Link>
                                    </TableCell>
                                    <TableCell>{n.Type}</TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </Paper>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        errors: authErrors(state),
        user: state.auth.access,
        user_id: userId(state),
        page: {
            artifact: state.artifact,
        }

    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchRecentArtifacts: (pageNum) => dispatch(loadRecentArtifacts(pageNum)),
        setSection: (section) => dispatch(setSection(section)),
        setView: (view) => dispatch(setView(view)),
    }
};


export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),
    withStyles(styles, {
            withTheme: true
        }
    ))(LayoutArtifactAll);