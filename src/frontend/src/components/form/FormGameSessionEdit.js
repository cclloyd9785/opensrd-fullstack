import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, formValueSelector, getFormValues } from 'redux-form'
import { TextField, Paper, Typography, Divider, Card, CardHeader, CardContent, Button, IconButton, Checkbox } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import { Grid, Row, Col } from 'react-flexbox-grid'
import { dark } from '../App'
import {connect} from "react-redux";
import compose from "recompose/compose";
import {userId, authErrors, accessToken, currentGameSession} from "../../reducers";
import {updateGameSession} from "../../actions/game";
import SaveIcon from '@material-ui/icons/Save';
import CachedIcon from '@material-ui/icons/Cached';
import { renderCheckbox, renderSelectField, renderTextField } from "./reduxFormMaterialUi";

const ReactMarkdown = require('react-markdown');

const styles = theme => ({
    textInput: {
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    longPaper: {
        padding: theme.spacing.unit * 3,
        backgroundColor: dark[5],
    },
    gamesessionTitle: {
        fontSize: 48,
        fontFamily: [
            'Balthazar',
        ],
    },
    gamesessionContents: {
        fontSize: 22,
    },
    row: {
        'padding-top': theme.spacing.unit * 3,
        'padding-bottom': theme.spacing.unit * 3,
    },
    divider: {
        //'padding-top': theme.spacing.unit * 2
    },
    sectionTop: {
        'margin-top': theme.spacing.unit * 4,
    },
    sectionBottom: {
        'margin-bottom': theme.spacing.unit * 1,
    },
    sectionText: {
        'background-color': dark[5],
        'padding-left': theme.spacing.unit * 1,
    },
    markdownBody: {

    },
    pageContent: {},
    contentPage: {},
    sectionHeader: {
        'font-size': 24,
    },
    sectionPaper: {
        'margin-top': theme.spacing.unit * 3,
        'margin-bottom': theme.spacing.unit * 3,
    },
    gamesessionCardContent: {
        'padding-left': 0,
        'padding-right': 0,
    }
});

class FormGameSessionEdit extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        //this.props.initialize(this.state)
    }

    render() {
        const {classes} = this.props;

        const { submit, handleSubmit, pristine, reset, submitting } = this.props;


        return (
            <form
                onSubmit={handleSubmit(this.props.sendSubmit)}
            >
                <Button
                    variant="fab"
                    color="primary"
                    aria-label="Save"
                    disabled={pristine || submitting}
                    onClick={handleSubmit(this.props.sendSubmit)}
                >
                    <SaveIcon/>
                </Button>

                <Paper className={classes.sectionPaper}>
                    <CardContent>
                        <Typography className={classes.sectionHeader}>Session Info</Typography>
                        <Divider />
                    </CardContent>
                    <CardContent>
                        <Grid container spacing={8}>
                            <Grid item xs={12} lg={4}>
                                <CardContent className={classes.gamesessionCardContent}>
                                    <Typography>Session Name</Typography>
                                    <Divider />
                                    <Field
                                        fullWidth
                                        name="Name"
                                        component={renderTextField}
                                        label="Session Name"
                                    />
                                </CardContent>
                            </Grid>
                        </Grid>
                    </CardContent>
                </Paper>

                <Paper className={classes.sectionPaper}>
                    <CardContent>
                        <Typography className={classes.sectionHeader}>Summary</Typography>
                        <Divider />
                    </CardContent>
                    <CardContent>
                        <Grid container spacing={8}>
                            <Grid item xs={12}>
                                <CardContent className={classes.gamesessionCardContent}>
                                    <Typography>Short Summary (markdown supported)</Typography>
                                    <Divider />
                                    <Field
                                        fullWidth
                                        multiline
                                        rows={5}
                                        name="Summary"
                                        component={renderTextField}
                                        label="Summary"
                                    />
                                </CardContent>
                            </Grid>
                        </Grid>
                    </CardContent>
                </Paper>

                <Paper className={classes.sectionPaper}>
                    <CardContent>
                        <Typography className={classes.sectionHeader}>Description</Typography>
                        <Divider />
                    </CardContent>
                    <CardContent>
                        <Grid container spacing={8}>
                            <Grid item xs={12}>
                                <CardContent className={classes.gamesessionCardContent}>
                                    <Typography>Full journal of the session (markdown supported)</Typography>
                                    <Divider />
                                    <Field
                                        fullWidth
                                        rows={20}
                                        multiline
                                        name="Description"
                                        component={renderTextField}
                                        label="Description"
                                    />
                                </CardContent>
                            </Grid>
                        </Grid>
                    </CardContent>
                </Paper>


            </form>
        );
    }
}

FormGameSessionEdit.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
    return {
        errors: authErrors(state),
        user: state.auth.access,
        user_id: userId(state),
        page: {
            game: state.game,
        },
        nav: {
            section: state.currentSection,
            view: state.currentView,
        },
        initialValues: state.game.currentSession,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        sendSubmit: (values) => dispatch(updateGameSession(values)),
    }
};


export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
    reduxForm({
        form: 'FormGameSessionEdit',
        enableReinitialize: true
    }),
    withStyles(styles, {
            withTheme: true
        },
    ))(FormGameSessionEdit);


