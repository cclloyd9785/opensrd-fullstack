import * as mobile_action from '../actions/mobile'

const initialState = false;

export default (state=initialState, action) => {
    switch(action.type) {
        case mobile_action.SET_MOBILE:
            return action.payload;
        default:
            return state;
    }
}

export function isMobile(state) {
    return state.mobile;
}

