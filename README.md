# Django & React application

Bootstrapped with create-react-app

## Running

1. `./dev.bash build`
2. `./dev.bash up`
3. There should now be two servers running:

  - [http://127.0.0.1:8000](http://127.0.0.1:8000) is the Django app
  - [http://127.0.0.1:3000](http://127.0.0.1:3000) is the React app

## Using `docker-compose run` to issue one-off commands

If you want to run a one-off command, like installing dependencies, you can use the `docker-compose run <service_name> <cmd>`.

For example, to install a Javascript dependency and save that information to `package.json` we could run:
`docker-compose run --rm frontend npm install --save axios`

If you want to be on a shell for one of the Docker services, you can do something like:
`docker-compose run --rm frontend bash`

## Running in production
Same as running dev, but use `prod.bash` instead.  Make sure to run `npm run build` on the dev frontend container before running in prod!
