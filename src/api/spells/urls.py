from rest_framework import routers
from .viewsets import SpellViewSet

router = routers.SimpleRouter()

router.register(r'^api/spells', SpellViewSet, 'spells')
