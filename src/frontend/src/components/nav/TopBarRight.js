import React from 'react';
import { withRouter } from 'react-router'

import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';

import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { dark } from '../App'
import {authErrors} from "../../reducers";
import { connect } from 'react-redux'
import compose from 'recompose/compose';

import {switchNavRight} from "../../actions/navRight";

const drawerWidth = 200;
const drawerWidthTools=300;

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    appFrame: {
        height: '100%',
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%',
    },
    topBar: {
        justifyContent: 'right',
    },
    logo: {
        flexGrow: 1,
        flex: 1,
    },
    appBar: {
        position: 'fixed',
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    'appBarShift-left': {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
    },
    'appBarShift-right': {
        width: `calc(100% - ${drawerWidthTools}px)`,
    },
    'appBarShift-both': {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth+drawerWidthTools}px)`,
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 20,
    },
    hide: {
        display: 'none',
    },
    drawerPaper: {
        position: 'relative',
        width: drawerWidth,
        backgroundColor: dark[3],
    },
    drawerPaperTools: {
        position: 'relative',
        width: drawerWidthTools,
        backgroundColor: dark[3],
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'left',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        backgroundColor: dark[4],
        marginLeft: -drawerWidth,
        marginRight: -drawerWidthTools,
        'overflow-x': 'hidden',
        'overflow-y': 'auto',
        padding: theme.spacing.unit * 3,

        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    contentMobile: {
        marginLeft: 'auto',
        marginRight: 'auto',
        //width: '90%',
        padding: theme.spacing.unit * 1,

    },
    contentShift: {
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    'contentShift-left': {
        marginLeft: 0,
    },
    'contentShift-right': {
        marginRight: 0,
    },
    'contentShift-both': {
        marginLeft: 0,
        marginRight: 0,
    },
    nested: {
        paddingLeft: theme.spacing.unit * 4,
    },
    hidden: {
        display: 'none',
    },
});



class TopBarRight extends React.Component {
    state = {
        mobile: false,
        navRight: true,
    };

    constructor(props) {
        super(props);

        const mobile = (window.innerWidth <= 1024);

        this.state = {
            navRight: !mobile,
        }
    }

    componentDidMount() {
        this.props.toggleNavRight(this.state.navRight);
    }

    toggleNavRight = () => {
        const { nav } = this.props;
        const newState = !nav.navRight;
        this.setState({ navRight: newState });
        this.props.toggleNavRight(newState);
    };

    handleSpells = () => {
        this.setState({
            spells: !this.state.spells,
        });
    };

    render() {
        const { classes } = this.props;

        return (
            <IconButton
                color="inherit"
                aria-label="open tool drawer"
                onClick={this.toggleNavRight}
                className={classNames(classes.menuButton)}
            >
                <MenuIcon />
            </IconButton>
        );
    }
}


const mapStateToProps = (state) => ({
    errors: authErrors(state),
    user: state.auth.access,
    page: {
        spell: state.spell,
    },
    device: {
        mobile: state.mobile,
    },
    nav: {
        navRight: state.navRight,
    }
});

const mapDispatchToProps = (dispatch) => {
    return {
        toggleNavRight: (newState) => dispatch(switchNavRight(newState)),
    }
};


export default compose(
    withRouter,
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
    withStyles(styles, {
            withTheme: true
        }
    ))(TopBarRight);


