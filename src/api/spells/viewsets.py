from rest_framework import viewsets, mixins
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework.decorators import action

from .serializers import SpellSerializer
from .models import Spell
from django.contrib.auth.models import User

from api.serializers import UserSerializer, UserPublicSerializer
from api.viewsets import UserViewSet


class SpellViewSet(
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):

    queryset = Spell.objects.all()
    serializer_class = SpellSerializer

    @action(methods=['get'], detail=True)
    def owner(self, request, pk=None):
        spell = self.get_object()
        owner = User.objects.get(id=spell.owner)
        serializer = UserPublicSerializer(owner)
        return Response(serializer.data)

    @action(methods=['get'], detail=False)
    def newest(self, request):
        queryset = Spell.objects.order_by('-id')[:20]
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(methods=['get'], detail=False)
    def oldest(self, request):
        queryset = Spell.objects.order_by('id')[:20]
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(methods=['get', 'post'], detail=False)
    def mine(self, request):
        if request.method == 'POST':
            queryset = Spell.objects.filter(owner=request.POST.get('id'))
            page = self.paginate_queryset(queryset)
            if page is not None:
                serializer = self.get_serializer(page, many=True)
                return self.get_paginated_response(serializer.data)
        queryset = Spell.objects.filter(owner=1)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
