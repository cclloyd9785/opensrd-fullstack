import React, { Component } from 'react';
import {authErrors, isAuthenticated, withAuth, userId} from '../../../reducers/index'

import { withStyles } from '@material-ui/core/styles'
import axios from 'axios'
import { dark } from '../../App'
import {connect} from "react-redux";
import compose from "recompose/compose";
import { loadRecentSpells } from "../../../actions/spell";
import {setPage, loadPage, setSection} from "../../../actions/page";
import { Table, TableRow, TableBody, TableCell, TableHead, Paper } from '@material-ui/core';
import { Link } from 'react-router-dom';
import {setView} from "../../../actions/view";
import { Grid, Row, Col } from 'react-flexbox-grid'

const styles = theme => ({
    tableLink: {
        color: theme.palette.primary.contrastText,
        fontSize: 16,
        'text-decoration': 'none',
    },
});



class LayoutSpellAll extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.setSection('spells');
        this.props.setView('all');
        this.props.fetchRecentSpells(1);
    }

    render() {
        const {classes} = this.props;

        return (
            <Paper className={classes.root}>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell>Name</TableCell>
                            <TableCell>Level</TableCell>
                            <TableCell>Description</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.props.page.spell.list.map(n => {
                            return (
                                <TableRow key={n.id}>
                                    <TableCell component="th" scope="row">
                                        <Link className={classes.tableLink} to={"/spells/"+n.id}>{n.Name}</Link>
                                    </TableCell>
                                    <TableCell>{n.Level}</TableCell>
                                    <TableCell>{n.DescriptionShort}</TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </Paper>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        errors: authErrors(state),
        user: state.auth.access,
        user_id: userId(state),
        page: {
            spell: state.spell,
        }

    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchRecentSpells: (pageNum) => dispatch(loadRecentSpells(pageNum)),
        setSection: (section) => dispatch(setSection(section)),
        setView: (view) => dispatch(setView(view)),
    }
};


export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),
    withStyles(styles, {
            withTheme: true
        }
    ))(LayoutSpellAll);