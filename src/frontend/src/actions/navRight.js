
export const TOGGLE_NAV_RIGHT = '@@nav/TOGGLE_NAV_RIGHT';

const navRightSuccess = (newState) => {
    return {
        type: TOGGLE_NAV_RIGHT,
        payload: newState,
    }
};

export const switchNavRight = (newState) => {
    return function (dispatch) {
        dispatch(navRightSuccess(newState));
        return newState;
    }
};
