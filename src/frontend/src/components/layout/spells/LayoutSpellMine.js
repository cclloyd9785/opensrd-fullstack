import React, { Component } from 'react';
import {authErrors, isAuthenticated, withAuth, userId, currentUser} from '../../../reducers/index'

import { withStyles } from '@material-ui/core/styles'
import { dark } from '../../App'
import {connect} from "react-redux";
import compose from "recompose/compose";
import { loadMySpells } from "../../../actions/spell";
import { setSection } from "../../../actions/page";
import { setView } from "../../../actions/view";
import { Table, TableBody, TableCell, TableHead, TableRow, Paper }from '@material-ui/core';
import { Link } from 'react-router-dom';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
    menu: {
        width: 200,
    },
    formLogin: {
        width: '100%',
    },
    textInput: {
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    longPaper: {
        padding: theme.spacing.unit * 3,
        backgroundColor: dark[5],
    },
    spellTitle: {
        fontSize: 48,
        fontFamily: [
            'Balthazar',
        ],
    },
    spellContents: {
        fontSize: 22,
    },
    Test: {
        display: 'none',
    },
    row: {
        'padding-top': theme.spacing.unit * 3,
        'padding-bottom': theme.spacing.unit * 3,
    },
    divider: {
        //'padding-top': theme.spacing.unit * 2
    },
    sectionTop: {
        'margin-top': theme.spacing.unit * 4,
    },
    sectionBottom: {
        'margin-bottom': theme.spacing.unit * 1,
    },
    sectionText: {
        'background-color': dark[5],
        'padding-left': theme.spacing.unit * 1,
    },
    markdownBody: {

    },
    pageContent: {},
    contentPage: {},
    tableLink: {
        color: theme.palette.primary.contrastText,
        fontSize: 16,
        'text-decoration': 'none',
    },
});



class LayoutSpellMine extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.setSection('spells');
        this.props.setView('mine');
        this.props.fetchMySpells(1);
    }

    render() {
        const {classes} = this.props;

        return (
            <Paper className={classes.root}>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell>Name</TableCell>
                            <TableCell>Level</TableCell>
                            <TableCell>Description</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.props.page.spell.list.map(n => {
                            return (
                                <TableRow key={n.id}>
                                    <TableCell component="th" scope="row">
                                        <Link className={classes.tableLink} to={"/spells/"+n.id}>{n.Name}</Link>
                                    </TableCell>
                                    <TableCell>{n.Level}</TableCell>
                                    <TableCell>{n.DescriptionShort}</TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </Paper>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        errors: authErrors(state),
        page: {
            spell: state.spell,
        },
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchMySpells: (pageNum) => dispatch(loadMySpells(pageNum)),
        setSection: (section) => dispatch(setSection(section)),
        setView: (view) => dispatch(setView(view)),
    }
};


export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),
    withStyles(styles, {
            withTheme: true
        }
    ))(LayoutSpellMine);