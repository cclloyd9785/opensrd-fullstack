import React, { Component } from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles'
import { dark } from '../../App'
import {connect} from "react-redux";
import compose from "recompose/compose";
import {clearGameSessions, createGameSession, loadGame, loadGameSessions} from "../../../actions/game";
import { setView } from "../../../actions/view";
import { setSection} from "../../../actions/page";
import {authErrors, userId} from "../../../reducers/index";
import {
    TextField, Grid,
    Paper,
    Typography,
    Divider,
    Card,
    CardHeader,
    CardContent,
    Button,
    IconButton,
    Checkbox,
    TableRow, TableCell, CircularProgress
} from '@material-ui/core'
import {Link} from "react-router-dom";
import {isAuthenticated} from "../../../reducers";
import ErrorIcon from '@material-ui/icons/Error';


const ReactMarkdown = require('react-markdown');


const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
    menu: {
        width: 200,
    },
    formLogin: {
        width: '100%',
    },
    textInput: {
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    longPaper: {
        padding: theme.spacing.unit * 3,
        backgroundColor: dark[5],
    },
    gameTitle: {
        fontSize: 48,
        fontFamily: [
            'Balthazar',
        ],
    },
    gameContents: {
        //fontSize: 22,
    },
    Test: {
        display: 'none',
    },
    row: {
        'padding-top': theme.spacing.unit * 3,
        'padding-bottom': theme.spacing.unit * 3,
    },
    divider: {
        //'padding-top': theme.spacing.unit * 2
    },
    sectionTop: {
        'margin-top': theme.spacing.unit * 4,
    },
    sectionBottom: {
        'margin-bottom': theme.spacing.unit * 1,
    },
    sectionText: {
        'background-color': dark[5],
        'padding-left': theme.spacing.unit * 1,
    },
    markdownBody: {

    },
    pageContent: {},
    contentPage: {},
    flavorText: {
        fontStyle: 'italic',
    },
    flavorIndent: {
        paddingLeft: theme.spacing.unit * 5,
    },
    levelCard: {
        paddingBottom: '0 !important',
    },
    sectionPaper: {
        marginBottom: theme.spacing.unit * 3,
    },
    circleProgress: {
        color: theme.palette.text.primary,
    },
    circleProgressError: {
        color: theme.palette.error.dark,
    },
    unloaded: {
        color: dark[6],
        backgroundColor: dark[6],
    },
});



class LayoutGameView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loaded: {
                sessions: false,
            },
            loading: {
                sessions: true,
            },
        }
    }

    componentDidMount() {
        let gameId = this.props.match.params.gameId;

        this.props.clearGameSessions();

        this.props.fetchGame(gameId);
        this.props.setSection('games');
        this.props.setView('view');
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.isAuthenticated) {
            if (!this.state.loaded.sessions) {
                const gameId = this.props.match.params.gameId;
                this.setState({
                    loading: {
                        sessions: true
                    }
                });
                this.props.fetchGameSessions(gameId)
                    .then(response => {return response})
                    .then(response => {
                        console.log('response', response);
                        if (response !== undefined) {
                            if (!response.error) {
                                this.setState({
                                    loading: {
                                        sessions: false
                                    },
                                    loaded: {
                                        sessions: true
                                    }
                                });
                            }
                        }
                    });
            }
        }
    }

    handleChange = name => event => {
        const game = {
            ...this.props.page.game.model,
            [name]: event.target.value,
        };

        this.setState({
            game: game,
        });
    };


    render() {
        const {classes} = this.props;

        const gameId = this.props.match.params.gameId;
        const sessionId = this.props.match.params.sessionId;


        let gameTitle = undefined;
        if (this.state.loading.main) {
            gameTitle =
                <Typography
                    variant="display3"
                    className={classNames(classes.gameTitle, classes.unloaded)}
                >
                    {this.props.page.game.model.Name}
                </Typography>
        }
        else {
            gameTitle =
                <Typography
                    variant="display3"
                    className={classes.gameTitle}
                >
                    {this.props.page.game.model.Name}
                </Typography>
        }




        let sessionsWindow = undefined;

        if (this.state.loading.sessions) {
            sessionsWindow =
                <Grid item xs={12}>
                    <CircularProgress className={classes.circleProgress}/>
                </Grid>
        }
        else {
            if (this.state.loaded.sessions) {
                sessionsWindow =
                    this.props.page.game.sessions.map((n, index) => {
                        return (
                            <Grid item xs={12} lg={6} className={classes.sectionPaper} key={index}>
                                <Paper key={index}>
                                    <Grid container item xs={12} spacing={16}>
                                        <Grid item xs={12} lg={8}>
                                            <CardContent>
                                                <Link className={classes.tableLink} to={"/games/"+this.props.page.game.model.id+'/sessions/'+n.id}>
                                                    <Typography variant="title">{n.Name}</Typography>
                                                </Link>
                                            </CardContent>
                                        </Grid>
                                        <Grid item lg={2}>
                                            <Button
                                                fullWidth
                                                variant='raised'
                                                component={Link}
                                                to={'/games/' + gameId + '/sessions/' + n.id + '/edit'}
                                                color="primary"
                                            >
                                                Edit
                                            </Button>
                                        </Grid>
                                        <Grid item lg={2}>
                                            <Button
                                                fullWidth
                                                variant='raised'
                                                component={Link}
                                                to={'/games/' + gameId + '/sessions/' + n.id + '/play'}
                                                color="primary"
                                            >
                                                Join
                                            </Button>
                                        </Grid>
                                        <Divider/>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <CardContent>
                                            <Typography variant="subheading">{n.Summary}</Typography>
                                        </CardContent>
                                    </Grid>
                                </Paper>
                            </Grid>
                        );
                    });
            }
            else {
                sessionsWindow =
                    <Grid item xs={12}>
                        <ErrorIcon className={classes.circleProgressError}/>
                    </Grid>
            }
        }


        return (
            <div>
                <Grid container className={classes.sectionPaper}>
                    <Grid item xs={12} className={classes.sectionPaper}>
                        {gameTitle}
                        <Divider className={classes.sectionPaper}/>

                        <Grid container spacing={16}>
                            <Grid item xs={6} lg={2}>
                                <Button
                                    fullWidth
                                    variant='raised'
                                    onClick={() => { this.props.createGameSession(this.props.page.game.model.id) }}
                                    className={classes.sectionPaper}
                                    color="primary"
                                >
                                    Create Session
                                </Button>
                            </Grid>
                            <Grid item xs={6} lg={2}>
                                <Button
                                    fullWidth
                                    variant='raised'
                                    component={Link}
                                    to={'/games/' + gameId + '/edit'}
                                    color="primary"
                                >
                                    Edit Game
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>

                    <Grid container item spacing={16}>
                        {sessionsWindow}
                    </Grid>

                </Grid>



            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        errors: authErrors(state),
        isAuthenticated: isAuthenticated(state),
        user: state.user,
        user_id: userId(state),
        device: {
            mobile: state.mobile,
        },
        page: {
            game: state.game,
        },
        nav: {
            section: state.currentSection,
            view: state.currentView,
        },
    }
};



const mapDispatchToProps = (dispatch) => {
    return {
        clearGameSessions: () => dispatch(clearGameSessions()),
        createGameSession: (gameId) => dispatch(createGameSession(gameId)),
        fetchGame: (gameId) => dispatch(loadGame(gameId)),
        fetchGameSessions: (gameId) => dispatch(loadGameSessions(gameId)),
        setSection: (section) => dispatch(setSection(section)),
        setView: (view) => dispatch(setView(view)),
    }
};


export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),
    withStyles(styles, {
            withTheme: true
        }
    ))(LayoutGameView);