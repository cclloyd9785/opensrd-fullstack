#!/usr/bin/env bash

#echo "${@:1}"
docker-compose -f compose/common/db.yml -f compose/prod/django.yml -f compose/prod/nginx.yml -p opensrd "${@:1}"
