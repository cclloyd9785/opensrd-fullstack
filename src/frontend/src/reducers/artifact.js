import * as artifact_action from '../actions/artifact'

const initialState = {
    model: {},
    list: [],
};

export default (state=initialState, action) => {
    switch(action.type) {
        case artifact_action.FETCH_ARTIFACT_SUCCESS:
            return {
                model: action.payload,
                list: [],
            };
        case artifact_action.UPDATE_ARTIFACT_SUCCESS:
            return {
                model: action.payload,
                list: [],
            };
        case artifact_action.CREATE_ARTIFACT_SUCCESS:
            return {
                model: action.payload,
                list: [],
            };
        case artifact_action.FETCH_RECENT_ARTIFACTS_SUCCESS:
            return {
                model: {},
                list: action.payload,
            };
        case artifact_action.FETCH_MY_ARTIFACTS_SUCCESS:
            return {
                model: {},
                list: action.payload,
            };
        default:
            return state;
    }
}

export function loadArtifact(state) {
    if (state.artifact) {
        return state.artifact
    }
}
