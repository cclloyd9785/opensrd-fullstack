import React, { Component } from 'react';
import classNames from 'classnames';

import { withStyles } from '@material-ui/core/styles'
import { Grid, Row, Col } from 'react-flexbox-grid'
import { dark } from '../../App'
import {connect} from "react-redux";
import compose from "recompose/compose";
import { loadArtifact } from "../../../actions/artifact";
import { setView } from "../../../actions/view";
import { setSection} from "../../../actions/page";
import {authErrors, userId} from "../../../reducers/index";
import { TextField, Paper, Typography, Divider, Card, CardHeader, CardContent, Button, IconButton, Checkbox } from '@material-ui/core'


const ReactMarkdown = require('react-markdown');


const styles = theme => ({
    hidden: {
        display: 'none',
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
    menu: {
        width: 200,
    },
    formLogin: {
        width: '100%',
    },
    textInput: {
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    longPaper: {
        padding: theme.spacing.unit * 3,
        backgroundColor: dark[5],
    },
    artifactTitle: {
        fontSize: 48,
        fontFamily: [
            'Balthazar',
        ],
    },
    artifactContents: {
        //fontSize: 22,
    },
    Test: {
        display: 'none',
    },
    row: {
        'padding-top': theme.spacing.unit * 3,
        'padding-bottom': theme.spacing.unit * 3,
    },
    divider: {
        //'padding-top': theme.spacing.unit * 2
    },
    sectionTop: {
        'margin-top': theme.spacing.unit * 4,
    },
    sectionBottom: {
        'margin-bottom': theme.spacing.unit * 1,
    },
    sectionText: {
        'background-color': dark[5],
        'padding-left': theme.spacing.unit * 1,
    },
    markdownBody: {

    },
    pageContent: {},
    contentPage: {},
    flavorText: {
        fontStyle: 'italic',
    },
    flavorIndent: {
        paddingLeft: theme.spacing.unit * 5,
    },
    levelCard: {
        paddingBottom: '0 !important',
    },
    inlineBlock: {
        display: 'inline-block',
        paddingRight: theme.spacing.unit * 0.5,
    }
});



class LayoutArtifactView extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        let regex = /\/artifacts\/(\d+)/;
        let location = this.props.location.pathname;
        let artifactId = location.match(regex)[1];

        this.props.fetchArtifact(artifactId);
        this.props.setSection('artifacts');
        this.props.setView('view');
    }

    handleChange = name => event => {
        const artifact = {
            ...this.props.page.artifact.model,
            [name]: event.target.value,
        };

        this.setState({
            artifact: artifact,
        });
    };


    render() {
        const {classes} = this.props;

        return (
            <div>
                <Grid fluid>

                    <Row className={classes.row}>
                        <Col md={12} xs={12} >
                            <Typography
                                variant="display3"
                                className={classes.artifactTitle}
                            >{this.props.page.artifact.model.Name}</Typography>
                        </Col>
                    </Row>

                    <Divider className={classes.divider}/>

                    <Row className={classes.row}>
                        <Col md={12} xs={12} >
                            <Typography className={classNames(classes.flavorText, {
                                [classes.flavorIndent]: !this.props.device.mobile,
                            })} variant="title">{this.props.page.artifact.model.Flavor}</Typography>
                        </Col>
                    </Row>

                    <Paper>
                        <Row>
                            <Col md={12} xs={12}>
                                <CardContent>
                                    <Typography variant="title">Info</Typography>
                                </CardContent>
                                <Divider/>
                            </Col>
                            <Col md={2} xs={6} >
                                <CardContent>
                                    <Typography variant="title">Aura</Typography>
                                    <Typography variant="subheading">{this.props.page.artifact.model.Aura}</Typography>
                                </CardContent>
                            </Col>
                            <Col md={1} xs={6} >
                                <CardContent>
                                    <Typography variant="title">CL</Typography>
                                    <Typography variant="subheading">{this.props.page.artifact.model.CL}</Typography>
                                </CardContent>
                            </Col>
                            <Col md={1} xs={6} >
                                <CardContent>
                                    <Typography variant="title">Slot</Typography>
                                    <Typography variant="subheading">{this.props.page.artifact.model.Slot}</Typography>
                                </CardContent>
                            </Col>
                            <Col md={2} xs={6} >
                                <CardContent>
                                    <Typography variant="title">Weight</Typography>
                                    <Typography variant="subheading">{this.props.page.artifact.model.Weight}</Typography>
                                </CardContent>
                            </Col>
                            <Col md={3} xs={12} >
                                <CardContent>
                                    <Typography variant="title">Rarity</Typography>

                                    <Typography
                                        className={classNames(classes.inlineBlock, {
                                            [classes.hidden]: !this.props.page.artifact.model.Mythic,
                                        })}
                                        variant="subheading"
                                    >
                                        Mythic
                                    </Typography>

                                    <Typography
                                        className={classes.inlineBlock}
                                        variant="subheading"
                                    >
                                        {this.props.page.artifact.model.Rarity}
                                    </Typography>

                                    <Typography
                                        className={classNames(classes.inlineBlock, {
                                            [classes.hidden]: !this.props.page.artifact.model.Artifact,
                                        })}
                                        variant="subheading"
                                    >
                                        artifact
                                    </Typography>
                                </CardContent>
                            </Col>
                            <Col md={3} xs={6} className={classNames({
                                [classes.hidden]: this.props.page.artifact.model.SpecialPower === '',
                            })}>
                                <CardContent>
                                    <Typography variant="title">Special Power</Typography>
                                    <Typography variant="subheading">{this.props.page.artifact.model.SpecialPower}</Typography>
                                </CardContent>
                            </Col>
                        </Row>
                    </Paper>

                    <Row><Col xs={12}>&nbsp;</Col></Row>

                    <Paper className={classNames({
                        [classes.hidden]: this.props.page.artifact.model.Alignment === '',
                    })}>
                        <Row>
                            <Col md={12} xs={12}>
                                <CardContent>
                                    <Typography variant="title">Sentient Item</Typography>
                                </CardContent>
                                <Divider/>
                            </Col>
                            <Col md={2} xs={4} >
                                <CardContent>
                                    <Typography variant="title">Alignment</Typography>
                                    <Typography variant="subheading">{this.props.page.artifact.model.Alignment}</Typography>
                                </CardContent>
                            </Col>
                            <Col md={2} xs={6} >
                                <CardContent>
                                    <Typography variant="title">Communication</Typography>
                                    <Typography variant="subheading">{this.props.page.artifact.model.Communication}</Typography>
                                </CardContent>
                            </Col>
                            <Col md={3} xs={12} >
                                <CardContent>
                                    <Typography variant="title">Senses</Typography>
                                    <Typography variant="subheading">{this.props.page.artifact.model.Senses}</Typography>
                                </CardContent>
                            </Col>
                            <Col md={1} xs={3} >
                                <CardContent>
                                    <Typography variant="title">Ego</Typography>
                                    <Typography variant="subheading">{this.props.page.artifact.model.Ego}</Typography>
                                </CardContent>
                            </Col>
                            <Col md={1} xs={3} >
                                <CardContent>
                                    <Typography variant="title">Int</Typography>
                                    <Typography variant="subheading">{this.props.page.artifact.model.IntScore}</Typography>
                                </CardContent>
                            </Col>
                            <Col md={1} xs={3} >
                                <CardContent>
                                    <Typography variant="title">Wis</Typography>
                                    <Typography variant="subheading">{this.props.page.artifact.model.IntScore}</Typography>
                                </CardContent>
                            </Col>
                            <Col md={1} xs={3} >
                                <CardContent>
                                    <Typography variant="title">Cha</Typography>
                                    <Typography variant="subheading">{this.props.page.artifact.model.ChaScore}</Typography>
                                </CardContent>
                            </Col>

                            <Col md={12} xs={12} className={classNames({
                                [classes.hidden]: this.props.page.artifact.model.SpecialPurpose === '',
                            })}>
                                <CardContent>
                                    <Typography variant="title">Special Purpose</Typography>
                                    <Typography variant="subheading">{this.props.page.artifact.model.SpecialPurpose}</Typography>
                                </CardContent>
                            </Col>
                            <Col md={12} xs={12} className={classNames({
                                [classes.hidden]: this.props.page.artifact.model.SpellLikeAbilities === '',
                            })}>
                                <CardContent>
                                    <Typography variant="title">Spell-Like Abilities</Typography>
                                    <Typography variant="subheading">{this.props.page.artifact.model.SpellLikeAbilities}</Typography>
                                </CardContent>
                            </Col>

                        </Row>
                    </Paper>

                    <Row><Col xs={12}>&nbsp;</Col></Row>

                    <Paper>
                        <Row>
                            <Col md={12} xs={12}>
                                <CardContent>
                                    <Typography variant="title">Description</Typography>
                                </CardContent>
                                <Divider/>
                            </Col>
                            <Col md={12} xs={12} >
                                <CardContent>
                                    <Typography
                                        paragraph
                                        variant="subheading"
                                        className={classes.markdownBody}
                                    ><ReactMarkdown source={this.props.page.artifact.model.Description} /></Typography>
                                </CardContent>
                            </Col>
                        </Row>
                    </Paper>

                    <Row><Col xs={12}>&nbsp;</Col></Row>

                    <Paper className={classNames({
                        [classes.hidden]: this.props.page.artifact.model.Destruction === '',
                    })}>
                        <Row>
                            <Col md={12} xs={12}>
                                <CardContent>
                                    <Typography variant="title">Destruction</Typography>
                                </CardContent>
                                <Divider/>
                            </Col>
                            <Col md={12} xs={12} >
                                <CardContent>
                                    <Typography
                                        paragraph
                                        variant="subheading"
                                        className={classes.markdownBody}
                                    ><ReactMarkdown source={this.props.page.artifact.model.Destruction} /></Typography>
                                </CardContent>
                            </Col>
                        </Row>
                    </Paper>

                    <Row><Col xs={12}>&nbsp;</Col></Row>

                    <Paper className={classNames({
                        [classes.hidden]: this.props.page.artifact.model.Ramifications === '',
                    })}>
                        <Row>
                            <Col md={12} xs={12}>
                                <CardContent>
                                    <Typography variant="title">Ramifications</Typography>
                                </CardContent>
                                <Divider/>
                            </Col>
                            <Col md={12} xs={12} >
                                <CardContent>
                                    <Typography
                                        paragraph
                                        variant="subheading"
                                        className={classes.markdownBody}
                                    ><ReactMarkdown source={this.props.page.artifact.model.Ramifications} /></Typography>
                                </CardContent>
                            </Col>
                        </Row>
                    </Paper>

                    <Row><Col xs={12}>&nbsp;</Col></Row>

                    <Paper className={classNames({
                        [classes.hidden]: this.props.page.artifact.model.Construction === '',
                    })}>
                        <Row>
                            <Col md={12} xs={12}>
                                <CardContent>
                                    <Typography variant="title">Construction</Typography>
                                </CardContent>
                                <Divider/>
                            </Col>
                            <Col md={8} xs={12} >
                                <CardContent>
                                    <Typography variant="title">Requirements</Typography>
                                    <Typography variant="subheading">{this.props.page.artifact.model.ConstructionRequirements}</Typography>
                                </CardContent>
                            </Col>
                            <Col md={2} xs={6} >
                                <CardContent>
                                    <Typography variant="title">Cost</Typography>
                                    <Typography variant="subheading">{this.props.page.artifact.model.Cost}</Typography>
                                </CardContent>
                            </Col>
                            <Col md={2} xs={6} >
                                <CardContent>
                                    <Typography variant="title">Price</Typography>
                                    <Typography variant="subheading">{this.props.page.artifact.model.Price}</Typography>
                                </CardContent>
                            </Col>
                            <Col md={12} xs={12} >
                                <CardContent>
                                    <Typography
                                        paragraph
                                        variant="subheading"
                                        className={classes.markdownBody}
                                    ><ReactMarkdown source={this.props.page.artifact.model.Construction} /></Typography>
                                </CardContent>
                            </Col>
                        </Row>
                    </Paper>


                </Grid>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        errors: authErrors(state),
        user: state.auth.access,
        user_id: userId(state),
        device: {
            mobile: state.mobile,
        },
        page: {
            artifact: state.artifact,
        },
        nav: {
            section: state.currentSection,
            view: state.currentView,
        },
    }
};



const mapDispatchToProps = (dispatch) => {
    return {
        fetchArtifact: (artifactId) => dispatch(loadArtifact(artifactId)),
        setSection: (section) => dispatch(setSection(section)),
        setView: (view) => dispatch(setView(view)),
    }
};


export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),
    withStyles(styles, {
            withTheme: true
        }
    ))(LayoutArtifactView);