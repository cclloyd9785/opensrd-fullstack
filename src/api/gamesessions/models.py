from django.db import models
from django.utils import timezone


class GameSession(models.Model):
    owner = models.IntegerField('owner', default=0)
    game = models.IntegerField('game', default=0)

    Name = models.CharField('Name', max_length=256, default="", blank=True)
    Summary = models.TextField('Summary', default="", blank=True)
    Description = models.TextField('Description', default="", blank=True)
    CoverImage = models.CharField('CoverImage', max_length=128, default="", blank=True)

    created = models.DateTimeField('created_at', default=timezone.now)
    modified = models.DateTimeField('modified_at', default=timezone.now)
    deleted = models.DateTimeField('deleted_at', default=None, null=True, blank=True)

    def __str__(self):
        return 'GameSession: %s' % self.Name

