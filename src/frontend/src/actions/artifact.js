import { RSAA } from 'redux-api-middleware';
import { withAuth } from '../reducers';

export const FETCH_ARTIFACT = '@@artifact/FETCH_ARTIFACT';
export const FETCH_ARTIFACT_SUCCESS = '@@artifact/FETCH_ARTIFACT_SUCCESS';
export const FETCH_ARTIFACT_FAILURE = '@@artifact/FETCH_ARTIFACT_FAILURE';

export const UPDATE_ARTIFACT = '@@artifact/UPDATE_ARTIFACT';
export const UPDATE_ARTIFACT_SUCCESS = '@@artifact/UPDATE_ARTIFACT_SUCCESS';
export const UPDATE_ARTIFACT_FAILURE = '@@artifact/UPDATE_ARTIFACT_FAILURE';

export const CREATE_ARTIFACT = '@@artifact/CREATE_ARTIFACT';
export const CREATE_ARTIFACT_SUCCESS = '@@artifact/CREATE_ARTIFACT_SUCCESS';
export const CREATE_ARTIFACT_FAILURE = '@@artifact/CREATE_ARTIFACT_FAILURE';

export const FETCH_RECENT_ARTIFACTS = '@@artifact/FETCH__RECENT_ARTIFACTS';
export const FETCH_RECENT_ARTIFACTS_SUCCESS = '@@artifact/FETCH_RECENT_ARTIFACTS_SUCCESS';
export const FETCH_RECENT_ARTIFACTS_FAILURE = '@@artifact/FETCH_RECENT_ARTIFACTS_FAILURE';

export const FETCH_MY_ARTIFACTS = '@@artifact/FETCH__MY_ARTIFACTS';
export const FETCH_MY_ARTIFACTS_SUCCESS = '@@artifact/FETCH_MY_ARTIFACTS_SUCCESS';
export const FETCH_MY_ARTIFACTS_FAILURE = '@@artifact/FETCH_MY_ARTIFACTS_FAILURE';


export const updateArtifact = (artifact) => ({
    [RSAA]: {
        endpoint: '/api/artifacts/' + artifact.id + '/',
        method: 'PUT',
        body: JSON.stringify(artifact),
        //body: artifact,
        headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            UPDATE_ARTIFACT, UPDATE_ARTIFACT_SUCCESS, UPDATE_ARTIFACT_FAILURE
        ]
    }
});

export const createArtifact = (artifact) => ({
    [RSAA]: {
        endpoint: '/api/artifacts/',
        method: 'POST',
        body: JSON.stringify(artifact),
        headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            CREATE_ARTIFACT, CREATE_ARTIFACT_SUCCESS, CREATE_ARTIFACT_FAILURE
        ]
    }
});

export const loadArtifact = (artifactId) => ({
    [RSAA]: {
        endpoint: '/api/artifacts/' + artifactId + '/',
        method: 'GET',
        //headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            FETCH_ARTIFACT, FETCH_ARTIFACT_SUCCESS, FETCH_ARTIFACT_FAILURE
        ]
    }
});

export const loadRecentArtifacts = (page) => ({
    [RSAA]: {
        endpoint: '/api/artifacts/newest/',
        method: 'GET',
        //headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            FETCH_RECENT_ARTIFACTS, FETCH_RECENT_ARTIFACTS_SUCCESS, FETCH_RECENT_ARTIFACTS_FAILURE
        ]
    }
});

export const loadMyArtifacts = (page) => ({
    [RSAA]: {
        endpoint: '/api/artifacts/mine/',
        method: 'GET',
        headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            FETCH_MY_ARTIFACTS, FETCH_MY_ARTIFACTS_SUCCESS, FETCH_MY_ARTIFACTS_FAILURE
        ]
    }
});
