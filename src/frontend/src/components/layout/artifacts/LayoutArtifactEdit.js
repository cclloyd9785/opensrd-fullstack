import React, { Component } from 'react';
import { reduxForm } from 'redux-form'
import { withStyles } from '@material-ui/core/styles'
import {connect} from "react-redux";
import compose from "recompose/compose";
import { loadArtifact } from "../../../actions/artifact";
import { setView } from "../../../actions/view";
import { setSection} from "../../../actions/page";
import { userId, authErrors } from "../../../reducers/index";
import FormArtifactEdit from '../../form/FormArtifactEdit';


const styles = theme => ({

});

class LayoutArtifactEdit extends Component {
    constructor(props) {
        super(props);

    }

    componentDidMount() {
        let regex = /\/artifacts\/(\d+)/;
        let location = this.props.location.pathname;
        let artifactId = location.match(regex)[1];

        this.props.fetchArtifact(artifactId);
        this.props.setSection('artifacts');
        this.props.setView('edit');
    }
    onSubmit = (e) => {
        e.preventDefault();
        this.props.onSubmit(this.state.artifact)
    };



    render() {
        return (
            <div>
                <FormArtifactEdit />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        errors: authErrors(state),
        user: state.auth.access,
        user_id: userId(state),
        page: {
            artifact: state.artifact
        },
        nav: {
            section: state.currentSection,
            view: state.currentView,
        },
        initialValues: state.artifact,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchArtifact: (artifactId) => dispatch(loadArtifact(artifactId)),
        setSection: (section) => dispatch(setSection(section)),
        setView: (view) => dispatch(setView(view)),
    }
};


export default compose(
    reduxForm({
        form: 'ArtifactEditForm',
    }),
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
    withStyles(styles, {
            withTheme: true
        },
    ))(LayoutArtifactEdit);