from django.db import models
from django.utils import timezone
import datetime
# Create your models here.


class Feat(models.Model):
    owner = models.IntegerField('owner', default=0)

    Name =          models.CharField('Name', max_length=128, default="", blank=True)
    Prerequisite =  models.CharField('Prerequisite', max_length=255, default="", blank=True)
    Flavor =        models.CharField('Flavor', max_length=255, default="", blank=True)
    Benefit =           models.TextField('Benefit', default="", blank=True)
    BenefitShort =      models.CharField('BenefitShort', max_length=255, default="", blank=True)
    Normal =            models.CharField('Normal', max_length=255, default="", blank=True)
    MetaFeat =          models.BooleanField('MetaFeat', default=False)
    MetaLevelIncrease = models.CharField('MetaLevelIncrease', max_length=32, default="", blank=True)


    Mythic = models.BooleanField('Mythic', default=False)
    MythicFlavor = models.CharField('MythicFlavor', max_length=255, default="", blank=True)
    MythicPrerequisite = models.CharField('MythicPrerequisite', max_length=255, default="", blank=True)
    MythicBenefit = models.TextField('MythicBenefit', default="", blank=True)
    MythicBenefitShort = models.CharField('MythicBenefitShort', max_length=255, default="", blank=True)

    Views = models.IntegerField('Views', default=0)


    created = models.DateTimeField('created_at', default=timezone.now)
    modified = models.DateTimeField('modified_at', default=timezone.now)
    deleted = models.DateTimeField('deleted_at', default=None, null=True, blank=True)

    def __str__(self):
        return 'Feat: %s' % self.Name

