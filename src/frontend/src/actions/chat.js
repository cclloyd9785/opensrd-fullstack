import { RSAA } from 'redux-api-middleware';
import { withAuth } from '../reducers';
import {SET_SECTION} from "./page";

export const CREATE_CHAT_MESSAGE = '@@chat/CREATE_CHAT_MESSAGE';
export const CREATE_CHAT_MESSAGE_SUCCESS = '@@chat/CREATE_CHAT_MESSAGE_SUCCESS';
export const CREATE_CHAT_MESSAGE_FAILURE = '@@chat/CREATE_CHAT_MESSAGE_FAILURE';

export const CREATE_CHAT_SESSION = '@@chat/CREATE_CHAT_SESSION';
export const CREATE_CHAT_SESSION_SUCCESS = '@@chat/CREATE_CHAT_SESSION_SUCCESS';
export const CREATE_CHAT_SESSION_FAILURE = '@@chat/CREATE_CHAT_SESSION_FAILURE';

export const FETCH_CHAT_SESSION = '@@chat/FETCH_CHAT_SESSION';
export const FETCH_CHAT_SESSION_SUCCESS = '@@chat/FETCH_CHAT_SESSION_SUCCESS';
export const FETCH_CHAT_SESSION_FAILURE = '@@chat/FETCH_CHAT_SESSION_FAILURE';

export const JOIN_CHAT_SESSION = '@@chat/JOIN_CHAT_SESSION';
export const JOIN_CHAT_SESSION_SUCCESS = '@@chat/JOIN_CHAT_SESSION_SUCCESS';
export const JOIN_CHAT_SESSION_FAILURE = '@@chat/JOIN_CHAT_SESSION_FAILURE';

export const FETCH_RECENT_CHAT_MESSAGES = '@@chat/FETCH_RECENT_CHAT_MESSAGES';
export const FETCH_RECENT_CHAT_MESSAGES_SUCCESS = '@@chat/FETCH_RECENT_CHAT_MESSAGES_SUCCESS';
export const FETCH_RECENT_CHAT_MESSAGES_FAILURE = '@@chat/FETCH_RECENT_CHAT_MESSAGES_FAILURE';

export const FETCH_ALL_CHAT_MESSAGES = '@@chat/FETCH_ALL_CHAT_MESSAGES';
export const FETCH_ALL_CHAT_MESSAGES_SUCCESS = '@@chat/FETCH_ALL_CHAT_MESSAGES_SUCCESS';
export const FETCH_ALL_CHAT_MESSAGES_FAILURE = '@@chat/FETCH_ALL_CHAT_MESSAGES_FAILURE';

export const PUSHER_ADD_MESSAGE_LIST_SUCCESS = '@@chat/PUSHER_ADD_MESSAGE_LIST_SUCCESS';
export const PUSHER_SET_CHAT_MESSAGE_LIST_SUCCESS = '@@chat/PUSHER_SET_CHAT_MESSAGE_LIST_SUCCESS';
export const PUSHER_SET_CURRENT_ROOM_SUCCESS = '@@chat/PUSHER_SET_CURRENT_ROOM_SUCCESS';
export const PUSHER_SET_CURRENT_USER_SUCCESS = '@@chat/PUSHER_SET_CURRENT_USER_SUCCESS';

export const CLEAR_CHAT_MESSAGES_SUCCESS = '@@chat/CLEAR_CHAT_MESSAGES_SUCCESS';
export const CLEAR_CHAT_SESSION_SUCCESS = '@@chat/CLEAR_CHAT_SESSION_SUCCESS';
export const ADD_MESSAGE_TO_INPUT_LOG_SUCCESS = '@@chat/ADD_MESSAGE_TO_INPUT_LOG_SUCCESS';
export const CLEAR_INPUT_LOG_SUCCESS = '@@chat/CLEAR_INPUT_LOG_SUCCESS';
export const ADD_CURRENT_INPUT_TO_INPUT_LOG_SUCCESS = '@@chat/ADD_CURRENT_INPUT_TO_INPUT_LOG_SUCCESS';
export const REPLACE_CURRENT_INPUT_IN_INPUT_LOG_SUCCESS = '@@chat/REPLACE_CURRENT_INPUT_IN_INPUT_LOG_SUCCESS';


export const createChatMessage = (chatMessage) => ({
    [RSAA]: {
        endpoint: '/api/chatmessages/',
        method: 'POST',
        body: JSON.stringify(chatMessage),
        headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            CREATE_CHAT_MESSAGE, CREATE_CHAT_MESSAGE_SUCCESS, CREATE_CHAT_MESSAGE_FAILURE
        ]
    }
});

export const createChatSession = (gamesession) => ({
    [RSAA]: {
        endpoint: '/api/chatsessions/',
        method: 'POST',
        body: JSON.stringify(gamesession),
        headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            CREATE_CHAT_SESSION, CREATE_CHAT_SESSION_SUCCESS, CREATE_CHAT_SESSION_FAILURE
        ]
    }
});

export const joinChatSession = (chatSessionId) => ({
    [RSAA]: {
        endpoint: '/api/chatsessions/' + chatSessionId + '/join',
        method: 'GET',
        //body: JSON.stringify(gamesession),
        headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            JOIN_CHAT_SESSION, JOIN_CHAT_SESSION_SUCCESS, JOIN_CHAT_SESSION_FAILURE
        ]
    }
});

export const loadChatSession = (gamesession) => ({
    [RSAA]: {
        endpoint: '/api/gamesessions/' + gamesession.id + '/chatsession/',
        method: 'GET',
        //body: JSON.stringify(gamesession),
        headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            FETCH_CHAT_SESSION, FETCH_CHAT_SESSION_SUCCESS, FETCH_CHAT_SESSION_FAILURE
        ]
    }
});

export const loadRecentChatMessages = (gameSessionId) => ({
    [RSAA]: {
        endpoint: '/api/gamesessions/' + gameSessionId + '/messagesrecent/',
        method: 'GET',
        headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            FETCH_RECENT_CHAT_MESSAGES, FETCH_RECENT_CHAT_MESSAGES_SUCCESS, FETCH_RECENT_CHAT_MESSAGES_FAILURE
        ]
    }
});

export const loadAllChatMessages = (sessionId) => ({
    [RSAA]: {
        endpoint: '/api/gamesessions/' + sessionId + '/messages/',
        method: 'GET',
        headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            FETCH_ALL_CHAT_MESSAGES, FETCH_ALL_CHAT_MESSAGES_SUCCESS, FETCH_ALL_CHAT_MESSAGES_FAILURE
        ]
    }
});


const pusherSetChatMessageListSuccess = (messages) => {
    return {
        type: PUSHER_SET_CHAT_MESSAGE_LIST_SUCCESS,
        payload: messages,
    }
};
export const pusherSetChatMessageList = (messages) => {
    return function (dispatch) {
        dispatch(pusherSetChatMessageListSuccess(messages));
    }
};



const addMessageToInputLogSuccess = (message) => {
    return {
        type: ADD_MESSAGE_TO_INPUT_LOG_SUCCESS,
        payload: message,
    }
};
export const addMessageToInputLog = (message) => {
    return function (dispatch) {
        dispatch(addMessageToInputLogSuccess(message));
    }
};



const clearChatSessionSuccess = () => {
    return {
        type: CLEAR_CHAT_SESSION_SUCCESS,
        payload: null,
    }
};
export const clearChatSession = () => {
    return function (dispatch) {
        dispatch(clearChatSessionSuccess());
    }
};



const clearInputLogSuccess = () => {
    return {
        type: CLEAR_INPUT_LOG_SUCCESS,
        payload: null,
    }
};
export const clearInputLog = () => {
    return function (dispatch) {
        dispatch(clearInputLogSuccess());
    }
};



const clearChatMessagesSuccess = () => {
    return {
        type: CLEAR_CHAT_MESSAGES_SUCCESS,
        payload: null,
    }
};
export const clearChatMessages = () => {
    return function (dispatch) {
        dispatch(clearChatMessagesSuccess());
    }
};




const pusherAddMessageToListSuccess = (message) => {
    return {
        type: PUSHER_ADD_MESSAGE_LIST_SUCCESS,
        payload: message,
    }
};
export const pusherAddMessageToList = (message) => {
    return function (dispatch) {
        dispatch(pusherAddMessageToListSuccess(message));
    }
};



const addCurrentInputToInputLogSuccess = (message) => {
    return {
        type: ADD_CURRENT_INPUT_TO_INPUT_LOG_SUCCESS,
        payload: message,
    }
};
export const addCurrentInputToInputLog = (message) => {
    return function (dispatch) {
        dispatch(addCurrentInputToInputLogSuccess(message));
    }
};



const replaceCurrentInputInInputLogSuccess = (message) => {
    return {
        type: REPLACE_CURRENT_INPUT_IN_INPUT_LOG_SUCCESS,
        payload: message,
    }
};
export const replaceCurrentInputInInputLog = (message) => {
    return function (dispatch) {
        dispatch(replaceCurrentInputInInputLogSuccess(message));
    }
};



const pusherSetCurrentRoomSuccess = (currentRoom) => {
    return {
        type: PUSHER_SET_CURRENT_ROOM_SUCCESS,
        payload: currentRoom,
    }
};
export const pusherSetCurrentRoom = (currentRoom) => {
    return function (dispatch) {
        dispatch(pusherSetCurrentRoomSuccess(currentRoom));
    }
};



const pusherSetCurrentUserSuccess = (currentUser) => {
    return {
        type: PUSHER_SET_CURRENT_USER_SUCCESS,
        payload: currentUser,
    }
};
export const pusherSetCurrentUser = (currentUser) => {
    return function (dispatch) {
        dispatch(pusherSetCurrentUserSuccess(currentUser));
    }
};

