from django.apps import AppConfig


class GameSessionsConfig(AppConfig):
    name = 'gamesessions'
