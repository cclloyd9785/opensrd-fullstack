import React, { Component } from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles'
import { dark } from '../../../App'
import {connect} from "react-redux";
import compose from "recompose/compose";
import {
    Grid,
    Paper,
    Hidden,
    Divider, Avatar
} from '@material-ui/core'
import {loadRecentChatMessages} from "../../../../actions/chat";

const Color = require('color');
const ReactMarkdown = require('react-markdown');


const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    sectionPaper: {
        marginBottom: theme.spacing.unit * 3,
    },
    chatContainer: {
        width: '100%',
        height: '100%',
    },
    full: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        top: 0,
        left: 0,
    },
    fullContent: {
        width: '100%',
        height: '100%',
        position: 'relative',
    },
    chatMessageContainer: {
        width: '100%',
        backgroundColor: '#' + Color(dark[4]).darken(0.05).rgbNumber().toString(16),
    },
    alternateColor: {
        backgroundColor: '#' + Color(dark[4]).lighten(0.05).rgbNumber().toString(16),
    },
    message: {
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 1,
    },
    avatar: {
        width: 60,
        textAlign: 'center',
        display: 'inline-flex',
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    messageContent: {
        width: 'calc(100% - 60px)',
        display: 'inline-block',
        minHeight: 'calc(100%)',
        paddingRight: theme.spacing.unit * 2,
        color: theme.palette.text.primary,
    },
});



class ChatMessage extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        //let gameId = this.props.match.params.gameId;
        //let sessionId = this.props.match.params.sessionId;
    }

    render() {
        const {classes} = this.props;

        return (
            <div className={classNames(classes.chatMessageContainer, {
                [classes.alternateColor]: this.props.alternate === 1,
            })}>
                <div className={classes.message}>
                    <div className={classes.avatar}>
                        <Avatar src={this.props.user.gravatar} />
                    </div>
                    <div className={classes.messageContent}>
                        {this.props.children}
                    </div>
                </div>
                <Divider/>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
    }
};



const mapDispatchToProps = (dispatch) => {
    return {
        //fetchGameSession: (sessionId) => dispatch(loadGameSession(sessionId)),
        //setSection: (section) => dispatch(setSection(section)),
        //setView: (view) => dispatch(setView(view)),
    }
};


export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),
    withStyles(styles, {
            withTheme: true
        }
    ))(ChatMessage);