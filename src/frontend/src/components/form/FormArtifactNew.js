import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form'
import { TextField, Paper, Typography, Divider, CardContent, Button, } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import { Grid, Row, Col } from 'react-flexbox-grid'
import { dark } from '../App'
import {connect} from "react-redux";
import compose from "recompose/compose";
import {userId, authErrors} from "../../reducers";
import {createArtifact} from "../../actions/artifact";
import SaveIcon from '@material-ui/icons/Save';
import CachedIcon from '@material-ui/icons/Cached';
import { renderCheckbox, renderSelectField, renderTextField } from "./reduxFormMaterialUi";


const styles = theme => ({
    textInput: {
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    longPaper: {
        padding: theme.spacing.unit * 3,
        backgroundColor: dark[5],
    },
    artifactTitle: {
        fontSize: 48,
        fontFamily: [
            'Balthazar',
        ],
    },
    artifactContents: {
        fontSize: 22,
    },
    row: {
        'padding-top': theme.spacing.unit * 3,
        'padding-bottom': theme.spacing.unit * 3,
    },
    divider: {
        //'padding-top': theme.spacing.unit * 2
    },
    sectionTop: {
        'margin-top': theme.spacing.unit * 4,
    },
    sectionBottom: {
        'margin-bottom': theme.spacing.unit * 1,
    },
    sectionText: {
        'background-color': dark[5],
        'padding-left': theme.spacing.unit * 1,
    },
    pageContent: {},
    contentPage: {},
    sectionHeader: {
        'font-size': 24,
    },
    sectionPaper: {
        'margin-top': theme.spacing.unit * 3,
        'margin-bottom': theme.spacing.unit * 3,
    },
    artifactCardContent: {
        'padding-left': 0,
        'padding-right': 0,
    }
});

class FormArtifactNew extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.initialize(this.state)
    }

    render() {
        const {classes} = this.props;

        // eslint-disable-next-line
        const { submit, handleSubmit, pristine, reset, submitting } = this.props;

        return (
            <form
                onSubmit={handleSubmit(this.props.sendSubmit)}
            >
                <Button
                    variant="fab"
                    color="primary"
                    aria-label="Save"
                    onClick={handleSubmit(this.props.sendSubmit)}
                >
                    <SaveIcon/>
                </Button>

                <Paper className={classes.sectionPaper}>
                    <CardContent>
                        <Typography className={classes.sectionHeader}>Artifact Info</Typography>
                        <Divider />
                    </CardContent>
                    <CardContent>
                        <Grid fluid>
                            <Row>
                                <Col xs={12} lg={12}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Artifact Name</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Name"
                                            component={renderTextField}
                                            label="Artifact Name"
                                        />
                                    </CardContent>
                                </Col>
                            </Row>
                        </Grid>
                    </CardContent>
                </Paper>

            </form>
        );
    }
}

FormArtifactNew.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
    return {
        errors: authErrors(state),
        user: state.auth.access,
        user_id: userId(state),
        page: {
            //artifact: state.artifact
        },
        nav: {
            section: state.currentSection,
            view: state.currentView,
        },
        initialValues: state.artifact,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        sendSubmit: (values) => dispatch(createArtifact(values)),
    }
};


export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
    reduxForm({
        form: 'FormArtifactNew',
        enableReinitialize: true
    }),
    withStyles(styles, {
            withTheme: true
        },
    ))(FormArtifactNew);
