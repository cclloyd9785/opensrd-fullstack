import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';

import LayoutHome from "./layout/LayoutHome";
import LayoutLogin from "./layout/LayoutLogin";
import LayoutRegister from "./layout/LayoutRegister";

import LayoutSpellAll from "./layout/spells/LayoutSpellAll";
import LayoutSpellView from "./layout/spells/LayoutSpellView";
import LayoutSpellEdit from "./layout/spells/LayoutSpellEdit";
import LayoutSpellNew from "./layout/spells/LayoutSpellNew";
import LayoutSpellMine from "./layout/spells/LayoutSpellMine";

import LayoutFeatAll from "./layout/feats/LayoutFeatAll";
import LayoutFeatView from "./layout/feats/LayoutFeatView";
import LayoutFeatEdit from "./layout/feats/LayoutFeatEdit";
import LayoutFeatNew from "./layout/feats/LayoutFeatNew";
import LayoutFeatMine from "./layout/feats/LayoutFeatMine";

import LayoutGameAll from "./layout/games/LayoutGameAll";
import LayoutGameView from "./layout/games/LayoutGameView";
import LayoutGameEdit from "./layout/games/LayoutGameEdit";
import LayoutGameNew from "./layout/games/LayoutGameNew";
import LayoutGameMine from "./layout/games/LayoutGameMine";
import LayoutGameSessionView from "./layout/games/gamesessions/LayoutGameSessionView";
import LayoutGameSessionPlay from "./layout/games/gamesessions/LayoutGameSessionPlay";

import LayoutArtifactAll from "./layout/artifacts/LayoutArtifactAll";
import LayoutArtifactView from "./layout/artifacts/LayoutArtifactView";
import LayoutArtifactEdit from "./layout/artifacts/LayoutArtifactEdit";
import LayoutArtifactNew from "./layout/artifacts/LayoutArtifactNew";
import LayoutArtifactMine from "./layout/artifacts/LayoutArtifactMine";


import LayoutSheetEdit from "./layout/sheets/LayoutSheetEdit";
import LayoutSheetNew from "./layout/sheets/LayoutSheetNew";
import LayoutSheetMine from "./layout/sheets/LayoutSheetMine";

import {Route, Switch} from 'react-router'
import { withRouter } from 'react-router'


import Login from './layout/LayoutLogin';
import PrivateRoute from './PrivateRoute'
import { connect } from 'react-redux'
import compose from 'recompose/compose';
import { authErrors } from "../reducers";
import classNames from 'classnames';
import {dark} from "./App";

import { drawerWidth as drawerWidthLeft }  from './nav/left/NavLeft'
import { drawerWidth as drawerWidthRight } from './nav/NavRight'
import { height as appBarHeight } from './nav/TopBar'
import LayoutGameSessionEdit from "./layout/games/gamesessions/LayoutGameSessionEdit";

const styles = theme => ({
    appContentContainer: {
        flexGrow: 1,
    },
    content: {
        flexGrow: 1,
        position: 'relative',
        backgroundColor: dark[4],
        minHeight: window.innerHeight - appBarHeight,
        marginLeft: drawerWidthLeft,
        marginRight: drawerWidthRight,
        marginTop: appBarHeight,
        'overflow-x': 'hidden',
        //'overflow-y': 'auto',
        padding: theme.spacing.unit * 3,
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    contentMobile: {
        marginLeft: 'auto',
        marginRight: 'auto',
        padding: theme.spacing.unit * 1,

    },
    contentShift: {
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    'contentShiftLeft': {
        marginLeft: 0,
    },
    'contentShiftRight': {
        marginRight: 0,
    },
});

class AppContent extends Component {

    render() {
        const { classes } = this.props;
        const { mobile } = this.props.device;
        const { navLeft, navRight } = this.props.nav;


        return (
            <main className={classNames(classes.content, {
                  [classes.contentMobile]:      mobile,
                  [classes.contentShift]:       (!mobile && (navLeft || navRight)),
                  [classes.contentShiftLeft]:   (!mobile && !navLeft),
                  [classes.contentShiftRight]: (!mobile && !navRight),
              })}>
                <Switch>
                    <Route
                        exact
                        path="/"
                        component={LayoutHome}
                    />
                    <Route
                        path="/login"
                        component={LayoutLogin}
                    />
                    <Route
                        path="/register"
                        component={LayoutRegister}
                    />
                    <Route
                        path="/spells/recent"
                        component={LayoutSpellAll}
                    />
                    <PrivateRoute
                        path="/spells/mine"
                        component={LayoutSpellMine}
                    />
                    <PrivateRoute
                        path="/spells/new"
                        component={LayoutSpellNew}
                    />
                    <Route
                        exact
                        path="/spells/:spellId"
                        component={LayoutSpellView}
                    />
                    <Route
                        path="/spells/:spellId/edit"
                        component={LayoutSpellEdit}
                    />

                    
                    <Route
                        path="/feats/recent"
                        component={LayoutFeatAll}
                    />
                    <PrivateRoute
                        path="/feats/mine"
                        component={LayoutFeatMine}
                    />
                    <PrivateRoute
                        path="/feats/new"
                        component={LayoutFeatNew}
                    />
                    <Route
                        exact
                        path="/feats/:featId"
                        component={LayoutFeatView}
                    />
                    <Route
                        path="/feats/:featId/edit"
                        component={LayoutFeatEdit}
                    />


                    <Route
                        path="/artifacts/recent"
                        component={LayoutArtifactAll}
                    />
                    <PrivateRoute
                        path="/artifacts/mine"
                        component={LayoutArtifactMine}
                    />
                    <PrivateRoute
                        path="/artifacts/new"
                        component={LayoutArtifactNew}
                    />
                    <Route
                        exact
                        path="/artifacts/:artifactId"
                        component={LayoutArtifactView}
                    />
                    <Route
                        path="/artifacts/:artifactId/edit"
                        component={LayoutArtifactEdit}
                    />

                    <Route
                        path="/games/recent"
                        component={LayoutGameAll}
                    />
                    <PrivateRoute
                        path="/games/mine"
                        component={LayoutGameMine}
                    />
                    <PrivateRoute
                        path="/games/new"
                        component={LayoutGameNew}
                    />
                    <Route
                        exact
                        path="/games/:gameId"
                        component={LayoutGameView}
                    />
                    <PrivateRoute
                        path="/games/:gameId/edit"
                        component={LayoutGameEdit}
                    />
                    <Route
                        exact
                        path="/games/:gameId/sessions/:sessionId"
                        component={LayoutGameSessionView}
                    />
                    <Route
                        path="/games/:gameId/sessions/:sessionId/play"
                        component={LayoutGameSessionPlay}
                    />
                    <Route
                        path="/games/:gameId/sessions/:sessionId/edit"
                        component={LayoutGameSessionEdit}
                    />


                    <PrivateRoute
                        path="/sheets/mine"
                        component={LayoutSheetMine}
                    />
                    <PrivateRoute
                        path="/sheets/new"
                        component={LayoutSheetNew}
                    />
                    <Route
                        path="/sheets/:sheetId/edit"
                        component={LayoutSheetEdit}
                    />
                </Switch>
            </main>
        );
    }
}

const mapStateToProps = (state) => ({
    errors: authErrors(state),
    user: state.auth.access,
    device: {
        mobile: state.mobile,
    },
    nav: {
        navLeft: state.navLeft,
        navRight: state.navRight,
    }
});

export default compose(
    withRouter,
    connect(
        mapStateToProps,
    ),
    withStyles(styles, {
            withTheme: true
        }
    ))(AppContent);

