#!/usr/bin/env bash

#echo "${@:1}"
docker-compose -f compose/common/db.yml -f compose/dev/django.yml -f compose/dev/frontend.yml -f compose/dev/rabbitmq.yml -p opensrd "${@:1}"
