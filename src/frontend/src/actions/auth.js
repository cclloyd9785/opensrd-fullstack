import { RSAA } from 'redux-api-middleware';
import {FETCH_SPELL_SUCCESS} from "./spell";
import axios from "axios/index";

export const LOGIN_REQUEST = '@@jwt/LOGIN_REQUEST';
export const LOGIN_SUCCESS = '@@jwt/LOGIN_SUCCESS';
export const LOGIN_FAILURE = '@@jwt/LOGIN_FAILURE';

export const LOGOUT_REQUEST = '@@jwt/LOGOUT_REQUEST';
export const LOGOUT_SUCCESS = '@@jwt/LOGOUT_SUCCESS';
export const LOGOUT_FAILURE = '@@jwt/LOGOUT_FAILURE';


export const REGISTER_REQUEST = '@@jwt/REGISTER_REQUEST';
export const REGISTER_SUCCESS = '@@jwt/REGISTER_SUCCESS';
export const REGISTER_FAILURE = '@@jwt/REGISTER_FAILURE';

export const USERNAME_REQUEST = '@@jwt/USERNAME_REQUEST';
export const USERNAME_SUCCESS = '@@jwt/USERNAME_SUCCESS';
export const USERNAME_FAILURE = '@@jwt/USERNAME_FAILURE';

export const TOKEN_REQUEST = '@@jwt/TOKEN_REQUEST';
export const TOKEN_RECEIVED = '@@jwt/TOKEN_RECEIVED';
export const TOKEN_FAILURE = '@@jwt/TOKEN_FAILURE';

export const login = (username, password) => ({
    [RSAA]: {
        endpoint: '/api/auth/token/obtain/',
        method: 'POST',
        body: JSON.stringify({username, password}),
        headers: { 'Content-Type': 'application/json' },
        types: [
            LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE
        ]
    }
});

//localStorage.clear();
const logoutSuccess = () => {
    return {
        type: LOGOUT_SUCCESS,
        payload: null,
    }
};


export const logout = () => {
    return function (dispatch) {
        logoutSuccess();
    }
};


export const registerError = (error) => {
    return {
        type: REGISTER_FAILURE,
        payload: error,
    }
};

export const register = (username, email, password) => ({
    [RSAA]: {
        endpoint: '/api/register/',
        method: 'POST',
        body: JSON.stringify({username, email, password}),
        headers: { 'Content-Type': 'application/json' },
        types: [
            REGISTER_REQUEST, REGISTER_SUCCESS, REGISTER_FAILURE
        ]
    }
});

export const refreshAccessToken = (token) => ({
    [RSAA]: {
        endpoint: '/api/auth/token/refresh/',
        method: 'POST',
        body: JSON.stringify({refresh: token}),
        headers: { 'Content-Type': 'application/json' },
        types: [
            TOKEN_REQUEST, TOKEN_RECEIVED, TOKEN_FAILURE
        ]
    }
});