import React, { Component } from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles'
import { dark } from '../../../App'
import {connect} from "react-redux";
import compose from "recompose/compose";
import { loadGame, loadGameSession } from "../../../../actions/game";
import { setView } from "../../../../actions/view";
import { setSection} from "../../../../actions/page";
import {authErrors, isAuthenticated, userId} from "../../../../reducers/index";
import {
    TextField,
    Grid, GridList, GridListTile,
    Paper,
    Typography,
    Divider,
    Card,
    CardHeader,
    CardContent,
    Button,
    IconButton,
    Checkbox,
    TableRow, TableCell,
    Hidden,
} from '@material-ui/core'
import {Link} from "react-router-dom";

import {appBarHeight} from '../../../App'
import ChatMessage from "./ChatMessage";
import {
    createChatSession,
    joinChatSession,
    loadChatSession,
    loadRecentChatMessages, pusherAddMessageToList,
    pusherSetChatMessageList, pusherSetCurrentRoom, pusherSetCurrentUser
} from "../../../../actions/chat";
import FormChat from "../../../form/FormChat";
import {login} from "../../../../actions/auth";
import {loadUser} from "../../../../actions/user";
import { ChatManager, TokenProvider } from '@pusher/chatkit'
import Chatkit from '@pusher/chatkit'
import Pusher from "pusher-js";

Pusher.logToConsole = true;


const ReactMarkdown = require('react-markdown');
const Color = require('color');

const chatInputWrapperHeight = 100;

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    sectionPaper: {
        marginBottom: theme.spacing.unit * 3,
    },
    chatContainer: {
        width: '100%',
        height: '100%',
    },
    full: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        top: 0,
        left: 0,
    },
    fullContent: {
        width: '100%',
        height: '100%',
        position: 'relative',
    },
    fullWidth: {
        width: '100%',
        maxWidth: '100%',
    },
    fullHeight: {
        height: '100%',
    },
    scrollable: {
        overflowX: 'hidden',
        overflowY: 'scroll'
    },
    chatMessages: {
        alignItems: 'flex-end',
    },
    chatInput: {
        backgroundColor: '#' + Color(dark[3]).lighten(0.00).rgbNumber().toString(16),
    },
});




class LayoutGameSessionPlay extends Component {
    constructor(props) {
        super(props);

        this.state = {
            messagesLoaded: false,
            pusherConnected: false,
        }
    }


    componentDidMount() {
        const gameId = this.props.match.params.gameId;
        const sessionId = this.props.match.params.sessionId;

        this.props.setSection('games');
        this.props.setView('view');

        let list = document.getElementById("chat-list");
        if (list.scrollTop <= list.offsetHeight-200) {
            list.scrollTop = list.offsetHeight;
        }

    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.isAuthenticated || this.props.isAuthenticated) {
            if (!this.state.pusherConnected) {
                this.props.fetchChatSession(nextProps.page.game.currentSession);
                this.setState({pusherConnected: true});
            }
            if (!this.state.messagesLoaded) {
                const sessionId = this.props.match.params.sessionId;
                this.props.fetchRecentMessages(sessionId);
                this.setState({messagesLoaded: true});
            }
        }
    }

    scrollToBottom = () => {
        this.messagesEnd.scrollIntoView({ behavior: "smooth" });
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    render() {
        const {classes} = this.props;

        const gameId = this.props.match.params.gameId;
        const sessionId = this.props.match.params.sessionId;

        return (
            <div className={classes.full}>
                <Grid container className={classes.chatContainer}>
                    <Grid item xs={false} lg={8}>
                        <Button
                            variant={'raised'}
                            color="primary"
                            onClick={() => { this.props.fetchChatSession(this.props.page.game.currentSession) }}
                        >
                            Fetch Chat Session
                        </Button>
                        <Button
                            variant='raised'
                            component={Link}
                            to={'/games/' + gameId + '/sessions/' + sessionId + '/edit'}
                            color="primary"
                        >
                            Join
                        </Button>
                    </Grid>

                    <Grid
                        item
                        xs={12}
                        lg={4}
                        className={classes.chatContainer}
                    >
                        <Paper
                            square
                            className={classes.chatContainer}
                        >
                            <Grid
                                container
                                direction="column"
                                justify="flex-start"
                                alignItems="stretch"
                                spacing={0}
                                className={classes.fullHeight}
                            >
                                <Grid
                                    item xs={10} md={10}
                                    className={classNames(classes.fullWidth, classes.scrollable, classes.chatMessages)}
                                    id="chat-list"
                                >
                                    {this.props.page.chat.messages.map((n, index) => {
                                        return (
                                            <ChatMessage key={index} className={classes.fullWidth} alternate={index % 2}>{n.Message}</ChatMessage>
                                        );
                                    })}
                                    <div style={{ float:"left", clear: "both" }}
                                         ref={(el) => { this.messagesEnd = el; }}>
                                    </div>
                                </Grid>
                                <Grid item xs={2} md={2} className={classNames(classes.chatInput, classes.fullWidth)}>
                                    <FormChat/>
                                </Grid>
                            </Grid>
                        </Paper>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        errors: authErrors(state),
        isAuthenticated: isAuthenticated(state),
        user: state.user,
        user_id: userId(state),
        device: {
            mobile: state.mobile,
        },
        page: {
            game: state.game,
            chat: state.chat,
        },
        nav: {
            section: state.currentSection,
            view: state.currentView,
        },
    }
};



const mapDispatchToProps = (dispatch) => {
    return {
        addMessageToList(message) {
            dispatch(pusherAddMessageToList(message));
        },
        fetchChatSession: (currentSession) => {
            dispatch(loadChatSession(currentSession)).then(response => {return response})
                .then(response => {
                    if (response) {
                        if (response.payload) {
                            if (response.payload.uri) {
                                let pusher = new Pusher('3b9082ea8fc933fac004', {
                                    cluster: 'us2',
                                    encrypted: true
                                });
                                console.log('pusher', pusher);
                                console.log('subscribing to:', response.payload.uri);

                                let channel = pusher.subscribe(response.payload.uri);
                                channel.bind('new_message', function (message) {
                                    dispatch(pusherAddMessageToList(message));
                                    console.log('pusher message received', message);
                                });
                            }
                        }
                    }
                })
        },
        subscribeToChat: (chatSessionId) => dispatch(joinChatSession(chatSessionId)),
        setSection: (section) => dispatch(setSection(section)),
        setView: (view) => dispatch(setView(view)),
        fetchRecentMessages: (gameSessionId) => dispatch(loadRecentChatMessages(gameSessionId)),
    }
};


export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),
    withStyles(styles, {
            withTheme: true
        }
    ))(LayoutGameSessionPlay);