import React from 'react';
import { withRouter } from 'react-router'
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import {AppBar} from '@material-ui/core';
import {authErrors} from "../../reducers";
import { connect } from 'react-redux'
import compose from 'recompose/compose';
import {switchNavLeft} from "../../actions/navLeft";
import {switchNavRight} from "../../actions/navRight";
import TopBarLeft from './TopBarLeft';
import { drawerWidth as drawerWidthLeft }  from './left/NavLeft'
import { drawerWidth as drawerWidthRight } from './NavRight'

export const height = 60;

const styles = theme => ({
    topBar: {
        justifyContent: 'right',
    },
    logo: {
        flexGrow: 1,
        flex: 1,
    },
    appBar: {
        position: 'fixed',
        top: 0,
        overflow: 'hidden',
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        'background-image': "url('/bluefire.jpg')",
        'background-position': 'center center',
        height: height,
    },
    appBarShift: {
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    'appBarShift-left': {
        marginLeft: drawerWidthLeft,
        width: `calc(100% - ${drawerWidthLeft}px)`,
    },
    'appBarShift-right': {
        width: `calc(100% - ${drawerWidthRight}px)`,
    },
    'appBarShift-both': {
        marginLeft: drawerWidthLeft,
        width: `calc(100% - ${drawerWidthLeft+drawerWidthRight}px)`,
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 20,
    },
    hide: {
        display: 'none',
    },
    nested: {
        paddingLeft: theme.spacing.unit * 4,
    },
    hidden: {
        display: 'none',
    },
});



class TopBar extends React.Component {

    componentDidMount() {

    }

    render() {
        const { classes } = this.props;
        const { mobile } = this.props.device;
        const { navLeft, navRight } = this.props.nav;

        return (
            <AppBar
                className={classNames(classes.appBar, {
                    [classes.appBarShift]: (!mobile && (navLeft || navRight)),
                    [classes[`appBarShift-left`]]: (!mobile && (navLeft && !navRight)),
                    [classes[`appBarShift-right`]]: (!mobile && (navRight && !navLeft)),
                    [classes[`appBarShift-both`]]: (!mobile && (navRight && navLeft)),
                })}
                position='static'
                color='primary'
            >
                <TopBarLeft />
            </AppBar>
        );
    }
}

const mapStateToProps = (state) => ({
    errors: authErrors(state),
    user: state.auth.access,
    page: {
        type: 'spell',
        spell: state.spell,
    },
    device: {
        mobile: state.mobile,
    },
    nav: {
        navLeft: state.navLeft,
        navRight: state.navRight,
    }
});

const mapDispatchToProps = (dispatch) => {
    return {

    }
};


export default compose(
    withRouter,
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
    withStyles(styles, {
            withTheme: true
        }
    ))(TopBar);


