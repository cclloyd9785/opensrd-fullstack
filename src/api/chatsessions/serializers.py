from rest_framework import views, serializers, status
from rest_framework.exceptions import PermissionDenied
from rest_framework.response import Response
import json

from .models import ChatSession


class ChatSessionSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChatSession
        fields = '__all__'

    def create(self, validated_data):
        # Create and return a new `Snippet` instance, given the validated data.

        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
        else:
            return PermissionDenied()

        validated_data['owner'] = user.id
        return ChatSession.objects.create(**validated_data)

    def patch(self, instance, request, *args, **kwargs):
        """Add a user to a chat session."""
        user = request.user
        uri = kwargs['uri']

        chat_session = ChatSession.objects.get(uri=uri)
        current_users = json.loads(chat_session.Users)
        current_users.append(user.id)
        instance.Users = json.dumps(current_users)
        instance.save()

        return instance

    def update(self, instance, validated_data):
        # Update and return an existing `Snippet` instance, given the validated data.

        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user

            if user.id != instance.owner and not request.user.is_staff:
                raise PermissionDenied({"message": "You don't have permission to access this chat"})

        [setattr(instance, k, v) for k, v in validated_data.items()]
        instance.save()
        return instance
