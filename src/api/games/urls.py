from rest_framework import routers
from .viewsets import GameViewSet

router = routers.SimpleRouter()

router.register(r'^api/games', GameViewSet, 'games')
