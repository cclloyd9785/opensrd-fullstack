from django.apps import AppConfig


class ChatSessionsConfig(AppConfig):
    name = 'chatsessions'
