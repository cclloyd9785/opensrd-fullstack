from rest_framework import views, serializers, status
from rest_framework.exceptions import PermissionDenied
from rest_framework.exceptions import Throttled
import pusher

from .models import ChatMessage

#from gamesessions.viewsets import GameSessionViewSet
from chatsessions.models import ChatSession
from gamesessions.models import GameSession

class ChatMessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChatMessage
        fields = '__all__'

    def create(self, validated_data):
        # Create and return a new `Snippet` instance, given the validated data.

        if validated_data['Message'] == '':
            return Throttled()

        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
        else:
            return PermissionDenied()

        validated_data['owner'] = user.id
        validated_data['Character'] = 0

        pusher_client = pusher.Pusher(
            app_id='585979',
            key='3b9082ea8fc933fac004',
            secret='bc50faf8710b47d7ddcc',
            cluster='us2',
            ssl=True
        )

        game_session = GameSession.objects.get(id=validated_data['session'])
        chat_session = ChatSession.objects.filter(session=game_session.id)[0]

        validated_data['session'] = chat_session.id

        chat_message = ChatMessage.objects.create(**validated_data)

        channel = chat_session.uri
        if not channel:
            channel = 'nochannel'

        message = dict()
        message['Message'] = chat_message.Message
        message['channel'] = channel

        pusher_client.trigger(channel, 'new_message', ChatMessageSerializer(chat_message, many=False).data)

        return chat_message

    def update(self, instance, validated_data):
        # Update and return an existing `Snippet` instance, given the validated data.

        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user

            if user.id != instance.owner and not request.user.is_staff:
                raise PermissionDenied({"message": "You don't have permission to access this game"})

        [setattr(instance, k, v) for k, v in validated_data.items()]
        instance.save()
        return instance
