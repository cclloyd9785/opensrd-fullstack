from rest_framework import views, serializers, status

from .models import Character


class CharacterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Character
        fields = '__all__'

    def create(self, validated_data):
        # Create and return a new `Snippet` instance, given the validated data.

        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user

        validated_data['owner'] = user.id
        return Character.objects.create(**validated_data)

    def update(self, instance, validated_data):
        # Update and return an existing `Snippet` instance, given the validated data.
        [setattr(instance, k, v) for k, v in validated_data.items()]
        instance.save()
        return instance
