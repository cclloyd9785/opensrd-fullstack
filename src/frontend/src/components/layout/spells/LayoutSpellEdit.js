import React, { Component } from 'react';
import { reduxForm } from 'redux-form'
import { withStyles } from '@material-ui/core/styles'
import {connect} from "react-redux";
import compose from "recompose/compose";
import { loadSpell } from "../../../actions/spell";
import { setView } from "../../../actions/view";
import { setSection} from "../../../actions/page";
import { userId, authErrors } from "../../../reducers/index";
import FormSpellEdit from '../../form/FormSpellEdit';


const styles = theme => ({

});

class LayoutSpellEdit extends Component {
    constructor(props) {
        super(props);

    }

    componentDidMount() {
        let regex = /\/spells\/(\d+)/;
        let location = this.props.location.pathname;
        let spellId = location.match(regex)[1];

        this.props.fetchSpell(spellId);
        this.props.setSection('spells');
        this.props.setView('edit');
    }
    onSubmit = (e) => {
        e.preventDefault();
        this.props.onSubmit(this.state.spell)
    };



    render() {
        return (
            <div>
                <FormSpellEdit />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        errors: authErrors(state),
        user: state.auth.access,
        user_id: userId(state),
        page: {
            spell: state.spell
        },
        nav: {
            section: state.currentSection,
            view: state.currentView,
        },
        initialValues: state.spell,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchSpell: (spellId) => dispatch(loadSpell(spellId)),
        setSection: (section) => dispatch(setSection(section)),
        setView: (view) => dispatch(setView(view)),
    }
};


export default compose(
    reduxForm({
        form: 'SpellEditForm',
    }),
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
    withStyles(styles, {
        withTheme: true
    },
    ))(LayoutSpellEdit);