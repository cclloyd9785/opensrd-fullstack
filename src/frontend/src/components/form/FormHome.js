import React, { Component } from 'react';

import { TextField, Typography, Button } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles';
import { Grid, Row, Col } from 'react-flexbox-grid';
import axios from 'axios';
import { dark } from '../App'
import { connect } from 'react-redux'
import { Redirect } from 'react-router'
import { login } from '../../actions/auth'
import {authErrors, isAuthenticated, userId } from '../../reducers/index'
import compose from 'recompose/compose';
import {loadUser} from "../../actions/user";



const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
    menu: {
        width: 200,
    },
    formLogin: {
        width: '100%',
    },
    textInput: {
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    longPaper: {
        padding: theme.spacing.unit * 3,
        backgroundColor: dark[5],
    },
    pageContent: {},
    contentPage: {},
});



class FormHome extends Component {
    constructor(props) {
        super(props);
        this.state = {
            spacing: '16',
            username: '',
            password: '',
            remember: false,
        };

        let host = window.location.hostname;
        if (host === 'localhost') {
            this.state = {
                username: 'dm',
                password: 'notpassword',
            }
        }


        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }


    /*
    handleChange(event) {
        this.setState({value: event.target.value});
    }*/

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };


    onSubmit = (event) => {
        event.preventDefault();
        this.props.onSubmit(this.state.username, this.state.password)
    };


    handleSubmit(e) {
        console.log('Form state: ', this.state);
        e.preventDefault();


        const username = this.state.username;
        const password = this.state.password;
        const description = this.state.description;
        const formData = {
            username: username,
            password: password,
        };
        axios({
            method: 'POST',
            url: 'http://localhost:8000/api/auth/token/obtain/',
            data: formData,
            headers: {
                //'X-CSRFToken': csrftoken,
                //'Content-type': 'application/x-www-form-urlencoded'
            }
        }).then(res => {
            window.location.reload();
        });


    }

    render() {
        const {classes, access, user_id} = this.props;

        if (!user_id)
            return (

                <form
                    className={classes.formLogin}
                    onSubmit={this.onSubmit}>
                    <Grid fluid>

                        <Row>
                            <Col sm={12}>
                                <Typography variant="display2">Login</Typography>
                            </Col>
                        </Row>

                        <Row>
                            <Col md={3} xs={12} className={classes.textInput}>
                                <TextField
                                    id='username'
                                    name='username'
                                    label='Username or email'
                                    required fullWidth
                                    value={this.state.username}
                                    onChange={this.handleChange('username')}
                                />
                            </Col>
                            <Col md={2} xs={12} className={classes.textInput}>
                                <TextField
                                    id='password'
                                    type='password'
                                    name='username'
                                    label='Password'
                                    required fullWidth
                                    value={this.state.password}
                                    onChange={this.handleChange('password')}
                                />
                            </Col>
                            <Col md={2} mdOffset={0} xs={6} xsOffset={3} className={classes.textInput}>
                                <Button
                                    variant='raised'
                                    color='primary'
                                    fullWidth
                                    onClick={this.onSubmit}>Submit
                                </Button>
                            </Col>
                        </Row>
                    </Grid>
                </form>
            );
        else {
            return(
                <div>
                    <Typography variant="display1">Welcome.</Typography>
                </div>
            );
        }
    }
}

const mapStateToProps = (state) => ({
    errors: authErrors(state),
    user: state.user,
});

const mapDispatchToProps = (dispatch) => ({
    onSubmit: (username, password) => {
        dispatch(login(username, password))
            .then(response => {
                if (response !== undefined) {
                    if (!response.error) {
                        dispatch(loadUser())
                            .then(response => {
                                window.location.reload();
                            });
                    }
                }
            });
    },
    fetchUser: () => dispatch(loadUser()),
});

export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),
    withStyles(styles, {
            withTheme: true
        }
    ))(FormHome);

//export default withStyles(styles, { withTheme: true })(FormHome);