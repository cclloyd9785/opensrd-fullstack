import { RSAA } from 'redux-api-middleware';
import { withAuth } from '../reducers';
import {PUSHER_SET_CURRENT_USER_SUCCESS} from "./chat";

export const FETCH_GAME = '@@game/FETCH_GAME';
export const FETCH_GAME_SUCCESS = '@@game/FETCH_GAME_SUCCESS';
export const FETCH_GAME_FAILURE = '@@game/FETCH_GAME_FAILURE';

export const UPDATE_GAME = '@@game/UPDATE_GAME';
export const UPDATE_GAME_SUCCESS = '@@game/UPDATE_GAME_SUCCESS';
export const UPDATE_GAME_FAILURE = '@@game/UPDATE_GAME_FAILURE';

export const CREATE_GAME = '@@game/CREATE_GAME';
export const CREATE_GAME_SUCCESS = '@@game/CREATE_GAME_SUCCESS';
export const CREATE_GAME_FAILURE = '@@game/CREATE_GAME_FAILURE';

export const FETCH_RECENT_GAMES = '@@game/FETCH__RECENT_GAMES';
export const FETCH_RECENT_GAMES_SUCCESS = '@@game/FETCH_RECENT_GAMES_SUCCESS';
export const FETCH_RECENT_GAMES_FAILURE = '@@game/FETCH_RECENT_GAMES_FAILURE';

export const FETCH_MY_GAMES = '@@game/FETCH__MY_GAMES';
export const FETCH_MY_GAMES_SUCCESS = '@@game/FETCH_MY_GAMES_SUCCESS';
export const FETCH_MY_GAMES_FAILURE = '@@game/FETCH_MY_GAMES_FAILURE';

export const FETCH_GAME_SESSIONS = '@@game/FETCH_GAME_SESSIONS';
export const FETCH_GAME_SESSIONS_SUCCESS = '@@game/FETCH_GAME_SESSIONS_SUCCESS';
export const FETCH_GAME_SESSIONS_FAILURE = '@@game/FETCH_GAME_SESSIONS_FAILURE';

export const CLEAR_GAME_SESSIONS = '@@game/CLEAR_GAME_SESSIONS';
export const CLEAR_GAME_SESSIONS_SUCCESS = '@@game/CLEAR_GAME_SESSIONS_SUCCESS';
export const CLEAR_GAME_SESSIONS_FAILURE = '@@game/CLEAR_GAME_SESSIONS_FAILURE';

export const CREATE_GAME_SESSION = '@@game/CREATE_GAME_SESSION';
export const CREATE_GAME_SESSION_SUCCESS = '@@game/CREATE_GAME_SESSION_SUCCESS';
export const CREATE_GAME_SESSION_FAILURE = '@@game/CREATE_GAME_SESSION_FAILURE';

export const UPDATE_GAME_SESSION = '@@game/UPDATE_GAME_SESSION';
export const UPDATE_GAME_SESSION_SUCCESS = '@@game/UPDATE_GAME_SESSION_SUCCESS';
export const UPDATE_GAME_SESSION_FAILURE = '@@game/UPDATE_GAME_SESSION_FAILURE';

export const FETCH_GAME_SESSION = '@@game/FETCH_GAME_SESSION';
export const FETCH_GAME_SESSION_SUCCESS = '@@game/FETCH_GAME_SESSION_SUCCESS';
export const FETCH_GAME_SESSION_FAILURE = '@@game/FETCH_GAME_SESSION_FAILURE';


export const updateGame = (game) => ({
    [RSAA]: {
        endpoint: '/api/games/' + game.id + '/',
        method: 'PUT',
        body: JSON.stringify(game),
        //body: game,
        headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            UPDATE_GAME, UPDATE_GAME_SUCCESS, UPDATE_GAME_FAILURE
        ]
    }
});

export const createGame = (game) => ({
    [RSAA]: {
        endpoint: '/api/games/',
        method: 'POST',
        body: JSON.stringify(game),
        headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            CREATE_GAME, CREATE_GAME_SUCCESS, CREATE_GAME_FAILURE
        ]
    }
});

export const createGameSession = (gameId) => ({
    [RSAA]: {
        endpoint: '/api/games/' + gameId + '/newsession/',
        method: 'GET',
        headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            CREATE_GAME_SESSION, CREATE_GAME_SESSION_SUCCESS, CREATE_GAME_SESSION_FAILURE
        ]
    }
});

export const updateGameSession = (game) => ({
    [RSAA]: {
        endpoint: '/api/games/' + game.id + '/',
        method: 'PUT',
        body: JSON.stringify(game),
        headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            UPDATE_GAME_SESSION, UPDATE_GAME_SESSION_SUCCESS, UPDATE_GAME_SESSION_FAILURE
        ]
    }
});

export const loadGame = (gameId) => ({
    [RSAA]: {
        endpoint: '/api/games/' + gameId + '/',
        method: 'GET',
        //headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            FETCH_GAME, FETCH_GAME_SUCCESS, FETCH_GAME_FAILURE
        ]
    }
});

export const loadRecentGames = (page) => ({
    [RSAA]: {
        endpoint: '/api/games/newest/',
        method: 'GET',
        //headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            FETCH_RECENT_GAMES, FETCH_RECENT_GAMES_SUCCESS, FETCH_RECENT_GAMES_FAILURE
        ]
    }
});

export const loadGameSessions = (gameId) => ({
    [RSAA]: {
        endpoint: '/api/games/' + gameId + '/sessions/',
        method: 'GET',
        headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            FETCH_GAME_SESSIONS, FETCH_GAME_SESSIONS_SUCCESS, FETCH_GAME_SESSIONS_FAILURE
        ]
    }
});

const clearGameSessionsSuccess = () => {
    return {
        type: CLEAR_GAME_SESSIONS_SUCCESS,
        payload: null,
    }
};

export const clearGameSessions = () => {
    return function (dispatch) {
        dispatch(clearGameSessionsSuccess());
    }
};

export const loadGameSession = (sessionId) => ({
    [RSAA]: {
        endpoint: '/api/gamesessions/' + sessionId + "/",
        method: 'GET',
        //headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            FETCH_GAME_SESSION, FETCH_GAME_SESSION_SUCCESS, FETCH_GAME_SESSION_FAILURE
        ]
    }
});

export const loadMyGames = (page) => ({
    [RSAA]: {
        endpoint: '/api/games/mine/',
        method: 'GET',
        headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            FETCH_MY_GAMES, FETCH_MY_GAMES_SUCCESS, FETCH_MY_GAMES_FAILURE
        ]
    }
});
