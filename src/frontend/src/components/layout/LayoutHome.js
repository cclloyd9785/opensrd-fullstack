import React, { Component } from 'react';

import { withStyles } from '@material-ui/core/styles';
import compose from 'recompose/compose';
import appBarHeight from '../MaterialUiApp'
import { dark } from '../App'
import { connect } from 'react-redux'
import {authErrors, userId} from '../../reducers'
import { setView } from "../../actions/view";
import { setSection } from "../../actions/page";
import { loadRecentSpells } from "../../actions/spell";
import { loadRecentFeats } from "../../actions/feat";
import { loadRecentArtifacts } from "../../actions/artifact";
import { Table, TableRow, TableBody, TableCell, TableHead, Paper, CardContent, Typography, Divider } from '@material-ui/core';
import classNames from 'classnames';
import { Link } from 'react-router-dom';
import { Grid, Row, Col } from 'react-flexbox-grid'


const styles = theme => ({
    pageContent: {
        flexGrow: 1,
        backgroundColor: dark[4],
        maxHeight: `calc(100% - ${appBarHeight}px)`,
        height: `calc(100% - ${appBarHeight}px)`,
        'overflow-x': 'hidden',
        'overflow-y': 'scroll',
    },
    contentPage: {
        //width: '100%',
        flexGrow: 1,
    },
    homeTable: {
        //width: 500,
    },
    homeSpells: {

    },
    homeFeats: {

    },
    homeArtifacts: {

    },
    tableLink: {
        color: theme.palette.primary.contrastText,
        fontSize: 16,
    }
});

class LayoutHome extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.setSection('home');
        this.props.setView('home');
        this.props.fetchRecentSpells(1);
        this.props.fetchRecentFeats(1);
        this.props.fetchRecentArtifacts(1);

    }

    render() {
        const {classes} = this.props;

        return (
            <Grid>
                <Row>
                    <Col xs={12} lg={6}>
                        <Paper className={classNames(classes.homeTable, classes.homeSpells)}>
                            <CardContent>
                                <Typography variant="display1">Recent Spells</Typography>
                            </CardContent>
                            <Table className={classes.table}>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Name</TableCell>
                                        <TableCell>Level</TableCell>
                                        <TableCell>Description</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {this.props.page.spell.list.map(n => {
                                        return (
                                            <TableRow key={n.id}>
                                                <TableCell component="th" scope="row">
                                                    <Link className={classes.tableLink} to={"/spells/"+n.id}>{n.Name}</Link>
                                                </TableCell>
                                                <TableCell>{n.Level}</TableCell>
                                                <TableCell>{n.DescriptionShort}</TableCell>
                                            </TableRow>
                                        );
                                    })}
                                </TableBody>
                            </Table>
                        </Paper>
                    </Col>

                    <Col xs={12}>&nbsp;</Col>
                    <Col xs={12}>&nbsp;</Col>

                    <Col xs={12} lg={6}>
                        <Paper className={classNames(classes.homeTable, classes.homeFeats)}>
                            <CardContent>
                                <Typography variant="display1">Recent Feats</Typography>
                            </CardContent>
                            <Table className={classes.table}>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Name</TableCell>
                                        <TableCell>Benefit</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {this.props.page.feat.list.map(n => {
                                        return (
                                            <TableRow key={n.id}>
                                                <TableCell component="th" scope="row">
                                                    <Link className={classes.tableLink} to={"/feats/"+n.id}>{n.Name}</Link>
                                                </TableCell>
                                                <TableCell>{n.BenefitShort}</TableCell>
                                            </TableRow>
                                        );
                                    })}
                                </TableBody>
                            </Table>
                        </Paper>
                    </Col>

                    <Col xs={12}>&nbsp;</Col>
                    <Col xs={12}>&nbsp;</Col>

                    <Col xs={12} lg={6}>
                        <Paper className={classNames(classes.homeTable, classes.homeArtifacts)}>
                            <CardContent>
                                <Typography variant="display1">Recent Items</Typography>
                            </CardContent>
                            <Table className={classes.table}>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Name</TableCell>
                                        <TableCell>Type</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {this.props.page.artifact.list.map(n => {
                                        return (
                                            <TableRow key={n.id}>
                                                <TableCell component="th" scope="row">
                                                    <Link className={classes.tableLink} to={"/artifacts/"+n.id}>{n.Name}</Link>
                                                </TableCell>
                                                <TableCell>{n.Type}</TableCell>
                                            </TableRow>
                                        );
                                    })}
                                </TableBody>
                            </Table>
                        </Paper>
                    </Col>
                </Row>
            </Grid>
        );
    };
}


const mapStateToProps = (state) => ({
    errors: authErrors(state),
    //user_id: userId(state),
    page: {
        spell: state.spell,
        feat: state.feat,
        artifact: state.artifact,
    },
});

const mapDispatchToProps = (dispatch) => ({
    setSection: (section) => dispatch(setSection(section)),
    setView: (view) => dispatch(setView(view)),
    fetchRecentSpells: (pageNum) => dispatch(loadRecentSpells(pageNum)),
    fetchRecentFeats: (pageNum) => dispatch(loadRecentFeats(pageNum)),
    fetchRecentArtifacts: (pageNum) => dispatch(loadRecentArtifacts(pageNum)),
});

export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),
    withStyles(styles, {
        withTheme: true
    }
    ))(LayoutHome);


//export default withStyles(styles, { withTheme: true })(LayoutHome);


