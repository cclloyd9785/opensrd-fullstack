import { RSAA } from 'redux-api-middleware';
import { withAuth } from '../reducers';

export const FETCH_FEAT = '@@feat/FETCH_FEAT';
export const FETCH_FEAT_SUCCESS = '@@feat/FETCH_FEAT_SUCCESS';
export const FETCH_FEAT_FAILURE = '@@feat/FETCH_FEAT_FAILURE';

export const UPDATE_FEAT = '@@feat/UPDATE_FEAT';
export const UPDATE_FEAT_SUCCESS = '@@feat/UPDATE_FEAT_SUCCESS';
export const UPDATE_FEAT_FAILURE = '@@feat/UPDATE_FEAT_FAILURE';

export const CREATE_FEAT = '@@feat/CREATE_FEAT';
export const CREATE_FEAT_SUCCESS = '@@feat/CREATE_FEAT_SUCCESS';
export const CREATE_FEAT_FAILURE = '@@feat/CREATE_FEAT_FAILURE';

export const FETCH_RECENT_FEATS = '@@feat/FETCH__RECENT_FEATS';
export const FETCH_RECENT_FEATS_SUCCESS = '@@feat/FETCH_RECENT_FEATS_SUCCESS';
export const FETCH_RECENT_FEATS_FAILURE = '@@feat/FETCH_RECENT_FEATS_FAILURE';

export const FETCH_MY_FEATS = '@@feat/FETCH__MY_FEATS';
export const FETCH_MY_FEATS_SUCCESS = '@@feat/FETCH_MY_FEATS_SUCCESS';
export const FETCH_MY_FEATS_FAILURE = '@@feat/FETCH_MY_FEATS_FAILURE';


export const updateFeat = (feat) => ({
    [RSAA]: {
        endpoint: '/api/feats/' + feat.id + '/',
        method: 'PUT',
        body: JSON.stringify(feat),
        //body: feat,
        headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            UPDATE_FEAT, UPDATE_FEAT_SUCCESS, UPDATE_FEAT_FAILURE
        ]
    }
});

export const createFeat = (feat) => ({
    [RSAA]: {
        endpoint: '/api/feats/',
        method: 'POST',
        body: JSON.stringify(feat),
        headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            CREATE_FEAT, CREATE_FEAT_SUCCESS, CREATE_FEAT_FAILURE
        ]
    }
});

export const loadFeat = (featId) => ({
    [RSAA]: {
        endpoint: '/api/feats/' + featId + '/',
        method: 'GET',
        //headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            FETCH_FEAT, FETCH_FEAT_SUCCESS, FETCH_FEAT_FAILURE
        ]
    }
});

export const loadRecentFeats = (page) => ({
    [RSAA]: {
        endpoint: '/api/feats/newest/',
        method: 'GET',
        //headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            FETCH_RECENT_FEATS, FETCH_RECENT_FEATS_SUCCESS, FETCH_RECENT_FEATS_FAILURE
        ]
    }
});

export const loadMyFeats = (page) => ({
    [RSAA]: {
        endpoint: '/api/feats/mine/',
        method: 'GET',
        headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            FETCH_MY_FEATS, FETCH_MY_FEATS_SUCCESS, FETCH_MY_FEATS_FAILURE
        ]
    }
});
