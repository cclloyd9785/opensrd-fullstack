import React, { Component } from 'react';
import {authErrors, isAuthenticated, withAuth, userId} from '../../../reducers/index'

import { withStyles } from '@material-ui/core/styles'
import { dark } from '../../App'
import {connect} from "react-redux";
import compose from "recompose/compose";
import { loadMyGames } from "../../../actions/game";
import {setSection} from "../../../actions/page";
import { Table, TableBody, TableCell, TableHead, TableRow, Paper }from '@material-ui/core';
import { Link } from 'react-router-dom';
import {setView} from "../../../actions/view";


const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
    menu: {
        width: 200,
    },
    formLogin: {
        width: '100%',
    },
    textInput: {
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    longPaper: {
        padding: theme.spacing.unit * 3,
        backgroundColor: dark[5],
    },
    gameTitle: {
        fontSize: 48,
        fontFamily: [
            'Balthazar',
        ],
    },
    gameContents: {
        fontSize: 22,
    },
    Test: {
        display: 'none',
    },
    row: {
        'padding-top': theme.spacing.unit * 3,
        'padding-bottom': theme.spacing.unit * 3,
    },
    divider: {
        //'padding-top': theme.spacing.unit * 2
    },
    sectionTop: {
        'margin-top': theme.spacing.unit * 4,
    },
    sectionBottom: {
        'margin-bottom': theme.spacing.unit * 1,
    },
    sectionText: {
        'background-color': dark[5],
        'padding-left': theme.spacing.unit * 1,
    },
    markdownBody: {

    },
    pageContent: {},
    contentPage: {},
    tableLink: {
        color: theme.palette.primary.contrastText,
        fontSize: 16,
        'text-decoration': 'none',
    },
});



class LayoutGameMine extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.setSection('games');
        this.props.setView('mine');
        this.props.fetchMyGames(1);
    }

    render() {
        const {classes} = this.props;

        return (
            <Paper className={classes.root}>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell>Name</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.props.page.game.list.map(n => {
                            return (
                                <TableRow key={n.id}>
                                    <TableCell component="th" scope="row">
                                        <Link className={classes.tableLink} to={"/games/"+n.id}>{n.Name}</Link>
                                    </TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </Paper>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        errors: authErrors(state),
        user: state.auth.access,
        user_id: userId(state),
        page: {
            game: state.game,
        }

    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchMyGames: (pageNum) => dispatch(loadMyGames(pageNum)),
        setSection: (section) => dispatch(setSection(section)),
        setView: (view) => dispatch(setView(view)),
    }
};


export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),
    withStyles(styles, {
            withTheme: true
        }
    ))(LayoutGameMine);