import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import '../css/App.css';
import CssBaseline from '@material-ui/core/CssBaseline';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import { createMuiTheme } from '@material-ui/core/styles';
import { blueGrey, blue } from '@material-ui/core/colors';
import compose from 'recompose/compose';
import { withRouter } from 'react-router'
import { Cookies, CookiesProvider } from 'react-cookie'
import MaterialUiApp from './MaterialUiApp'


export const dark = {
    1: '#202225',
    2: '#2b2c31',
    3: '#2f3136',
    4: '#36393e',
    5: '#42464d',
    6: '#484b52',
};

const theme = createMuiTheme({
    palette: {
        type: 'dark',
        primary: {
            main: '#1b3c72',
            contrastThreshold: 3,
        },
        secondary: {
            main: blue[800],
        },
        text: {
            primary: '#b3b3b3',
        },
        dark: {
            1: '#202225',
            2: '#2b2c31',
            3: '#2f3136',
            4: '#36393e',
            5: '#42464d',
            6: '#484b52',
        }
    },
    appBar: {
        height: 55,
    },
});

const styles = {};


class App extends Component {
    render() {
        return (
            <CookiesProvider>
                <CssBaseline />
                <MuiThemeProvider theme={theme}>
                    <MaterialUiApp />
                </MuiThemeProvider>
            </CookiesProvider>
        );
    }
}

export default compose(
    withRouter,
    withStyles(styles, {
            withTheme: true
        }
    ))(App);