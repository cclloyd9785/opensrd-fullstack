import React, { Component } from 'react';
import {authErrors, isAuthenticated, withAuth, userId} from '../../../reducers/index'

import { withStyles } from '@material-ui/core/styles'
import {connect} from "react-redux";
import compose from "recompose/compose";
import { loadRecentGames } from "../../../actions/game";
import {setSection} from "../../../actions/page";
import { Table, TableBody, TableCell, TableHead, TableRow, Paper }from '@material-ui/core';
import { Link } from 'react-router-dom';
import {setView} from "../../../actions/view";

const styles = theme => ({
    tableLink: {
        color: theme.palette.primary.contrastText,
        fontSize: 16,
        'text-decoration': 'none',
    },
});



class LayoutGameAll extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.setSection('games');
        this.props.setView('all');
        this.props.fetchRecentGames(1);
    }

    render() {
        const {classes} = this.props;

        return (
            <Paper className={classes.root}>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell>Name</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.props.page.game.list.map(n => {
                            return (
                                <TableRow key={n.id}>
                                    <TableCell component="th" scope="row">
                                        <Link className={classes.tableLink} to={"/games/"+n.id}>{n.Name}</Link>
                                    </TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </Paper>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        errors: authErrors(state),
        user: state.auth.access,
        user_id: userId(state),
        page: {
            game: state.game,
        }

    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchRecentGames: (pageNum) => dispatch(loadRecentGames(pageNum)),
        setSection: (section) => dispatch(setSection(section)),
        setView: (view) => dispatch(setView(view)),
    }
};


export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),
    withStyles(styles, {
            withTheme: true
        }
    ))(LayoutGameAll);