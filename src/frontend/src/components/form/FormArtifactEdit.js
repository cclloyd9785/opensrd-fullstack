import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, formValueSelector, getFormValues } from 'redux-form'
import { TextField, Paper, Typography, Divider, Card, CardHeader, CardContent, Button, IconButton, Checkbox, Select, NativeSelect } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import { Grid, Row, Col } from 'react-flexbox-grid'
import { dark } from '../App'
import {connect} from "react-redux";
import compose from "recompose/compose";
import {userId, authErrors, accessToken, currentArtifact} from "../../reducers";
import {updateArtifact} from "../../actions/artifact";
import SaveIcon from '@material-ui/icons/Save';
import CachedIcon from '@material-ui/icons/Cached';
import { renderCheckbox, renderSelectField, renderTextField } from "./reduxFormMaterialUi";

const ReactMarkdown = require('react-markdown');

/*

TODO:  Add api endpoint to get user info (verify that user being requised is requested by that user)
TODO:  Add user info to NavRight, including username and avatar, with a dropdown for logging in and out and settings.


TODO:  Add recaptcha to login/register.

 */



const styles = theme => ({
    textInput: {
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    longPaper: {
        padding: theme.spacing.unit * 3,
        backgroundColor: dark[5],
    },
    artifactTitle: {
        fontSize: 48,
        fontFamily: [
            'Balthazar',
        ],
    },
    artifactContents: {
        fontSize: 22,
    },
    row: {
        'padding-top': theme.spacing.unit * 3,
        'padding-bottom': theme.spacing.unit * 3,
    },
    divider: {
        //'padding-top': theme.spacing.unit * 2
    },
    sectionTop: {
        'margin-top': theme.spacing.unit * 4,
    },
    sectionBottom: {
        'margin-bottom': theme.spacing.unit * 1,
    },
    sectionText: {
        'background-color': dark[5],
        'padding-left': theme.spacing.unit * 1,
    },
    markdownBody: {

    },
    pageContent: {},
    contentPage: {},
    sectionHeader: {
        'font-size': 24,
    },
    sectionPaper: {
        'margin-top': theme.spacing.unit * 3,
        'margin-bottom': theme.spacing.unit * 3,
    },
    artifactCardContent: {
        'padding-left': 0,
        'padding-right': 0,
    }
});

class FormArtifactEdit extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        //this.props.initialize(this.state)
    }

    updateCheck = () => {
        this.setState((oldState) => {
            return {
                checked: !oldState.checked,
            };
        });
    };

    render() {
        const {classes} = this.props;

        const { submit, handleSubmit, pristine, reset, submitting } = this.props;



        return (
            <form
                onSubmit={handleSubmit(this.props.sendSubmit)}
            >
                <Button
                    variant="fab"
                    color="primary"
                    aria-label="Save"
                    disabled={pristine || submitting}
                    onClick={handleSubmit(this.props.sendSubmit)}
                >
                    <SaveIcon/>
                </Button>

                <Paper className={classes.sectionPaper}>
                    <CardContent>
                        <Typography className={classes.sectionHeader}>Artifact Info</Typography>
                        <Divider />
                    </CardContent>
                    <CardContent>
                        <Grid fluid>
                            <Row>
                                <Col xs={12} lg={4}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Artifact Name</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Name"
                                            component={renderTextField}
                                            label="Artifact Name"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={12} lg={8}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Flavor Text</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Flavor"
                                            component={renderTextField}
                                            label="Flavor Text"
                                        />
                                    </CardContent>
                                </Col>
                            </Row>

                            <Row>
                                <Col xs={6} lg={2}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Aura</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Aura"
                                            component={renderTextField}
                                            label="Aura"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={6} lg={1}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>CL</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="CL"
                                            component={renderTextField}
                                            label="CL"
                                        />
                                    </CardContent>
                                </Col>

                                <Col xs={6} lg={2}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Slot</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth native
                                            name="Slot"
                                            component={renderSelectField}
                                            label="Slot"
                                        >
                                            <option value={"Head"}>Head</option>
                                            <option value={"Headband"}>Headband</option>
                                            <option value={"Eyes"}>Eyes</option>
                                            <option value={"Shoulders"}>Shoulders</option>
                                            <option value={"Neck"}>Neck</option>
                                            <option value={"Chest"}>Chest</option>
                                            <option value={"Body"}>Body</option>
                                            <option value={"Armor"}>Armor</option>
                                            <option value={"Belt"}>Belt</option>
                                            <option value={"Wrists"}>Wrists</option>
                                            <option value={"Hands"}>Hands</option>
                                            <option value={"Ring 1"}>Ring 1</option>
                                            <option value={"Ring 2"}>Ring 2</option>
                                            <option value={"Feet"}>Feet</option>
                                            <option value={"—"}>Slotless</option>
                                        </Field>
                                    </CardContent>
                                </Col>
                                <Col xs={6} lg={1}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Weight</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Weight"
                                            component={renderTextField}
                                            label="Weight"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={6} lg={1}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Rarity</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Rarity"
                                            component={renderTextField}
                                            label="Rarity"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={3} lg={1}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Mythic?</Typography>
                                        <Divider />
                                        <Field
                                            name="Mythic"
                                            component={renderCheckbox}
                                            label="Mythic"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={3} lg={1}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Artifact?</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Artifact"
                                            component={renderCheckbox}
                                            label="Artifact"
                                        />
                                    </CardContent>
                                </Col>

                                <Col xs={12} lg={3}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Type</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Type"
                                            component={renderTextField}
                                            label="Type"
                                        />
                                    </CardContent>
                                </Col>

                                <Col xs={12} lg={12}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Special Power</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="SpecialPower"
                                            component={renderTextField}
                                            label="SpecialPower"
                                        />
                                    </CardContent>
                                </Col>
                            </Row>
                        </Grid>
                    </CardContent>
                </Paper>

                <Paper className={classes.sectionPaper}>
                    <CardContent>
                        <Typography className={classes.sectionHeader}>Sentient Item (leave blank if not sentient)</Typography>
                        <Divider />
                    </CardContent>
                    <CardContent>
                        <Grid fluid>
                            <Row>
                                <Col xs={6} lg={1}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Sentient?</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Sentient"
                                            component={renderCheckbox}
                                            label="Sentient"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={6} lg={1}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Alignment</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Alignment"
                                            component={renderTextField}
                                            label="Alignment"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={12} lg={2}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Senses</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Senses"
                                            component={renderTextField}
                                            label="Senses"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={12} lg={2}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Communication</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Communication"
                                            component={renderTextField}
                                            label="Communication"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={3} lg={1}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Ego</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Ego"
                                            component={renderTextField}
                                            label="Ego"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={3} lg={1}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Int</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="IntScore"
                                            component={renderTextField}
                                            label="IntScore"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={3} lg={1}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Wis</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="WisScore"
                                            component={renderTextField}
                                            label="WisScore"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={3} lg={1}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Cha</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="ChaScore"
                                            component={renderTextField}
                                            label="ChaScore"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={12} lg={2}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Special Purpose</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="SpecialPurpose"
                                            component={renderTextField}
                                            label="SpecialPurpose"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={12} lg={12}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Spell-Like Abilities</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="SpellLikeAbilities"
                                            component={renderTextField}
                                            label="SpellLikeAbilities"
                                        />
                                    </CardContent>
                                </Col>
                            </Row>
                        </Grid>
                    </CardContent>
                </Paper>


                <Paper className={classes.sectionPaper}>
                    <CardContent>
                        <Typography className={classes.sectionHeader}>Description</Typography>
                        <Divider />
                    </CardContent>
                    <CardContent>
                        <Grid fluid>
                            <Row>
                                <Col xs={12} lg={12}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Description</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth multiline
                                            name="Description"
                                            component={renderTextField}
                                            label="Description"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={12} lg={12}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Description (short)</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="DescriptionShort"
                                            component={renderTextField}
                                            label="DescriptionShort"
                                        />
                                    </CardContent>
                                </Col>
                            </Row>
                        </Grid>
                    </CardContent>
                </Paper>

                <Paper className={classes.sectionPaper}>
                    <CardContent>
                        <Typography className={classes.sectionHeader}>Destruction</Typography>
                        <Divider />
                    </CardContent>
                    <CardContent>
                        <Grid fluid>
                            <Row>
                                <Col xs={12} lg={12}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Destruction</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth multiline
                                            name="Destruction"
                                            component={renderTextField}
                                            label="Destruction"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={12} lg={12}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Destruction (short)</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="DestructionShort"
                                            component={renderTextField}
                                            label="DestructionShort"
                                        />
                                    </CardContent>
                                </Col>
                            </Row>
                        </Grid>
                    </CardContent>
                </Paper>

                <Paper className={classes.sectionPaper}>
                    <CardContent>
                        <Typography className={classes.sectionHeader}>Ramifications</Typography>
                        <Divider />
                    </CardContent>
                    <CardContent>
                        <Grid fluid>
                            <Row>
                                <Col xs={12} lg={12}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Ramifications</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth multiline
                                            name="Ramifications"
                                            component={renderTextField}
                                            label="Ramifications"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={12} lg={12}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Ramifications (short)</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="RamificationsShort"
                                            component={renderTextField}
                                            label="RamificationsShort"
                                        />
                                    </CardContent>
                                </Col>
                            </Row>
                        </Grid>
                    </CardContent>
                </Paper>

                <Paper className={classes.sectionPaper}>
                    <CardContent>
                        <Typography className={classes.sectionHeader}>Construction</Typography>
                        <Divider />
                    </CardContent>
                    <CardContent>
                        <Grid fluid>
                            <Row>
                                <Col xs={12} lg={12}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Construction</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth multiline
                                            name="Construction"
                                            component={renderTextField}
                                            label="Construction"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={12} lg={8}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Construction requirements</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="ConstructionShort"
                                            component={renderTextField}
                                            label="ConstructionShort"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={12} lg={2}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Cost</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Cost"
                                            component={renderTextField}
                                            label="Cost"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={12} lg={2}>
                                    <CardContent className={classes.artifactCardContent}>
                                        <Typography>Price</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Price"
                                            component={renderTextField}
                                            label="Price"
                                        />
                                    </CardContent>
                                </Col>
                            </Row>
                        </Grid>
                    </CardContent>
                </Paper>



            </form>
        );
    }
}

FormArtifactEdit.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
    return {
        errors: authErrors(state),
        user: state.auth.access,
        user_id: userId(state),
        page: {
            artifact: state.artifact
        },
        nav: {
            section: state.currentSection,
            view: state.currentView,
        },
        initialValues: state.artifact,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        sendSubmit: (values) => dispatch(updateArtifact(values)),
    }
};


export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
    reduxForm({
        form: 'FormArtifactEdit',
        enableReinitialize: true
    }),
    withStyles(styles, {
            withTheme: true
        },
    ))(FormArtifactEdit);


