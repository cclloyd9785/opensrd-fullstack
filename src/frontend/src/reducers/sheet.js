import * as sheet_action from '../actions/sheet'

const initialState = {
    model: {},
    list: [],
};

export default (state=initialState, action) => {
    switch(action.type) {
        case sheet_action.FETCH_SHEET_SUCCESS:
            return {
                model: action.payload,
                list: [],
            };
        case sheet_action.UPDATE_SHEET_SUCCESS:
            return {
                model: action.payload,
                list: [],
            };
        case sheet_action.CREATE_SHEET_SUCCESS:
            return {
                model: action.payload,
                list: [],
            };
        case sheet_action.FETCH_RECENT_SHEETS_SUCCESS:
            return {
                model: {},
                list: action.payload,
            };
        case sheet_action.FETCH_MY_SHEETS_SUCCESS:
            return {
                model: {},
                list: action.payload,
            };
        default:
            return state;
    }
}

export function loadSheet(state) {
    if (state.sheet) {
        return state.sheet
    }
}
