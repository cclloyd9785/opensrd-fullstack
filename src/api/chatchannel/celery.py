"""Celery init."""

from __future__ import absolute_import, unicode_literals
import os

from celery import Celery

import api.settings as settings

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'api.settings')

app = Celery('chatsession', port=8001, broker=settings.broker_url)
app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks()