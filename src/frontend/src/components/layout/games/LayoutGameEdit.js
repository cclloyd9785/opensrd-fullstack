import React, { Component } from 'react';
import { reduxForm } from 'redux-form'
import { withStyles } from '@material-ui/core/styles'
import {connect} from "react-redux";
import compose from "recompose/compose";
import { loadGame } from "../../../actions/game";
import { setView } from "../../../actions/view";
import { setSection} from "../../../actions/page";
import { userId, authErrors } from "../../../reducers/index";
import FormGameEdit from '../../form/FormGameEdit';


const styles = theme => ({

});

class LayoutGameEdit extends Component {
    constructor(props) {
        super(props);

    }

    componentDidMount() {
        let regex = /\/games\/(\d+)/;
        let location = this.props.location.pathname;
        let gameId = location.match(regex)[1];

        this.props.fetchGame(gameId);
        this.props.setSection('games');
        this.props.setView('edit');
    }
    onSubmit = (e) => {
        e.preventDefault();
        this.props.onSubmit(this.state.game)
    };



    render() {
        return (
            <div>
                <FormGameEdit />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        errors: authErrors(state),
        user: state.auth.access,
        user_id: userId(state),
        page: {
            game: state.game
        },
        nav: {
            section: state.currentSection,
            view: state.currentView,
        },
        initialValues: state.game,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchGame: (gameId) => dispatch(loadGame(gameId)),
        setSection: (section) => dispatch(setSection(section)),
        setView: (view) => dispatch(setView(view)),
    }
};


export default compose(
    reduxForm({
        form: 'GameEditForm',
    }),
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
    withStyles(styles, {
            withTheme: true
        },
    ))(LayoutGameEdit);