from rest_framework import routers
from .viewsets import ArtifactViewSet

router = routers.SimpleRouter()

router.register(r'^api/artifacts', ArtifactViewSet, 'artifacts')
