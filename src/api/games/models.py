from django.db import models
from django.utils import timezone
from django.contrib.postgres.fields.jsonb import JSONField
import datetime


class Game(models.Model):
    owner = models.IntegerField('owner', default=0)

    Name = models.CharField('Name', max_length=128, default="", blank=True)
    Players = JSONField('Players', default="[]", blank=True)

    created = models.DateTimeField('created_at', default=timezone.now)
    modified = models.DateTimeField('modified_at', default=timezone.now)
    deleted = models.DateTimeField('deleted_at', default=None, null=True, blank=True)

    def __str__(self):
        return 'Game: %s' % self.Name

