from rest_framework import routers
from .viewsets import CharacterViewSet

router = routers.SimpleRouter()

router.register(r'^api/characters', CharacterViewSet, 'characters')
