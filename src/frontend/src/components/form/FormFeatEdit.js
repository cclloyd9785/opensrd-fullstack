import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, formValueSelector, getFormValues } from 'redux-form'
import { TextField, Paper, Typography, Divider, Card, CardHeader, CardContent, Button, IconButton, Checkbox } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import { Grid, Row, Col } from 'react-flexbox-grid'
import { dark } from '../App'
import {connect} from "react-redux";
import compose from "recompose/compose";
import {userId, authErrors, accessToken, currentFeat} from "../../reducers";
import {updateFeat} from "../../actions/feat";
import SaveIcon from '@material-ui/icons/Save';
import CachedIcon from '@material-ui/icons/Cached';
import { renderCheckbox, renderSelectField, renderTextField } from "./reduxFormMaterialUi";

import {drawerWidth as drawerWidthRight} from '../nav/NavRight'
const ReactMarkdown = require('react-markdown');


const styles = theme => ({
    textInput: {
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    longPaper: {
        padding: theme.spacing.unit * 3,
        backgroundColor: dark[5],
    },
    featTitle: {
        fontSize: 48,
        fontFamily: [
            'Balthazar',
        ],
    },
    featContents: {
        fontSize: 22,
    },
    row: {
        'padding-top': theme.spacing.unit * 3,
        'padding-bottom': theme.spacing.unit * 3,
    },
    divider: {
        //'padding-top': theme.spacing.unit * 2
    },
    sectionTop: {
        'margin-top': theme.spacing.unit * 4,
    },
    sectionBottom: {
        'margin-bottom': theme.spacing.unit * 1,
    },
    sectionText: {
        'background-color': dark[5],
        'padding-left': theme.spacing.unit * 1,
    },
    markdownBody: {

    },
    pageContent: {},
    contentPage: {},
    sectionHeader: {
        'font-size': 24,
    },
    sectionPaper: {
        'margin-top': theme.spacing.unit * 3,
        'margin-bottom': theme.spacing.unit * 3,
    },
    featCardContent: {
        'padding-left': 0,
        'padding-right': 0,
    },
    saveButton: {
        position: 'fixed',
        bottom: theme.spacing.unit * 2,
        right: theme.spacing.unit * 2 + drawerWidthRight,
    },
});

class FormFeatEdit extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        //this.props.initialize(this.state)
    }

    render() {
        const {classes} = this.props;

        const { submit, handleSubmit, pristine, reset, submitting } = this.props;


        return (
            <form
                onSubmit={handleSubmit(this.props.sendSubmit)}
            >
                <Button
                    variant="fab"
                    color="primary"
                    aria-label="Save"
                    disabled={pristine || submitting}
                    onClick={handleSubmit(this.props.sendSubmit)}
                    className={classes.saveButton}
                >
                    <SaveIcon/>
                </Button>

                <Paper className={classes.sectionPaper}>
                    <CardContent>
                        <Typography className={classes.sectionHeader}>Feat Info</Typography>
                        <Divider />
                    </CardContent>
                    <CardContent>
                        <Grid fluid>
                            <Row>
                                <Col xs={12} lg={4}>
                                    <CardContent className={classes.featCardContent}>
                                        <Typography>Feat Name</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Name"
                                            component={renderTextField}
                                            label="Feat Name"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={12} lg={8}>
                                    <CardContent className={classes.featCardContent}>
                                        <Typography>Flavor Text</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Flavor"
                                            component={renderTextField}
                                            label="Flavor Text"
                                        />
                                    </CardContent>
                                </Col>

                                <Col xs={12} lg={9}>
                                    <CardContent className={classes.featCardContent}>
                                        <Typography>Prerequisites</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Prerequisite"
                                            component={renderTextField}
                                            label="Prerequisite"
                                        />
                                    </CardContent>
                                </Col>

                                <Col xs={4} lg={1}>
                                    <CardContent className={classes.featCardContent}>
                                        <Typography>Meta Feat?</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="MetaFeat"
                                            component={renderCheckbox}
                                            label="MetaFeat"
                                        />
                                    </CardContent>
                                </Col>

                                <Col xs={8} lg={2}>
                                    <CardContent className={classes.featCardContent}>
                                        <Typography>Meta Increase</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="MetaLevelIncrease"
                                            component={renderTextField}
                                            label="Meta Level Increase"
                                        />
                                    </CardContent>
                                </Col>

                                <Col xs={12}>
                                    <Divider />
                                </Col>

                                <Col xs={12} lg={12}>
                                    <CardContent className={classes.featCardContent}>
                                        <Typography>Benefit (markdown supported)</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            multiline
                                            name="Benefit"
                                            component={renderTextField}
                                            label="Benefit (markdown supported)"
                                        />
                                    </CardContent>
                                </Col>

                                <Col xs={12} lg={12}>
                                    <CardContent className={classes.featCardContent}>
                                        <Typography>Benefit (short)</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="BenefitShort"
                                            component={renderTextField}
                                            label="Benefit Short"
                                        />
                                    </CardContent>
                                </Col>

                                <Col xs={12} lg={12}>
                                    <CardContent className={classes.featCardContent}>
                                        <Typography>Normal</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Normal"
                                            component={renderTextField}
                                            label="Normal"
                                        />
                                    </CardContent>
                                </Col>

                            </Row>
                        </Grid>
                    </CardContent>
                </Paper>


                <Paper className={classes.sectionPaper}>
                    <CardContent>
                        <Typography className={classes.sectionHeader}>Mythic</Typography>
                        <Divider />
                    </CardContent>
                    <CardContent>
                        <Grid fluid>
                            <Row>
                                <Col xs={12} lg={1}>
                                    <CardContent className={classes.featCardContent}>
                                        <Typography>Mythic?</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Mythic"
                                            component={renderCheckbox}
                                            label="Mythic"
                                        />
                                    </CardContent>
                                </Col>

                                <Col xs={12} lg={5}>
                                    <CardContent className={classes.featCardContent}>
                                        <Typography>Mythic Flavor Text</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="MythicFlavor"
                                            component={renderTextField}
                                            label="Mythic Flavor Text"
                                        />
                                    </CardContent>
                                </Col>

                                <Col xs={12} lg={6}>
                                    <CardContent className={classes.featCardContent}>
                                        <Typography>Mythic Prerequisites</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="MythicPrerequisite"
                                            component={renderTextField}
                                            label="Mythic Flavor Text"
                                        />
                                    </CardContent>
                                </Col>

                                <Col xs={12} lg={12}>
                                    <CardContent className={classes.featCardContent}>
                                        <Typography>Mythic Benefit (markdown supported)</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            multiline
                                            name="MythicBenefit"
                                            component={renderTextField}
                                            label="Mythic Benefit"
                                        />
                                    </CardContent>
                                </Col>

                                <Col xs={12} lg={12}>
                                    <CardContent className={classes.featCardContent}>
                                        <Typography>Mythic Benefit (short)</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="MythicBenefitShort"
                                            component={renderTextField}
                                            label="Mythic Benefit Short"
                                        />
                                    </CardContent>
                                </Col>
                            </Row>
                        </Grid>
                    </CardContent>
                </Paper>

            </form>
        );
    }
}

FormFeatEdit.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
    return {
        errors: authErrors(state),
        user: state.auth.access,
        user_id: userId(state),
        page: {
            feat: state.feat
        },
        nav: {
            section: state.currentSection,
            view: state.currentView,
        },
        initialValues: state.feat,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        sendSubmit: (values) => dispatch(updateFeat(values)),
    }
};


export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
    reduxForm({
        form: 'FormFeatEdit',
        enableReinitialize: true
    }),
    withStyles(styles, {
            withTheme: true
        },
    ))(FormFeatEdit);


