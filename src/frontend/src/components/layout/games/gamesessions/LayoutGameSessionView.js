import React, { Component } from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles'
import { dark } from '../../../App'
import {connect} from "react-redux";
import compose from "recompose/compose";
import { loadGame, loadGameSession } from "../../../../actions/game";
import { setView } from "../../../../actions/view";
import { setSection} from "../../../../actions/page";
import {authErrors, userId} from "../../../../reducers/index";
import {
    TextField,
    Grid,
    Paper,
    Typography, Hidden, CircularProgress,
    Divider,
    Card,
    CardHeader,
    CardContent,
    Button,
    IconButton,
    Checkbox,
    TableRow, TableCell
} from '@material-ui/core'
import {Link} from "react-router-dom";
import ChatMessage from "./ChatMessage";
import FormChat from "../../../form/FormChat";
import {clearChatMessages, loadAllChatMessages} from "../../../../actions/chat";
import {isAuthenticated} from "../../../../reducers";
import {Col, Row} from "react-flexbox-grid";

import ErrorIcon from '@material-ui/icons/Error';

const ReactMarkdown = require('react-markdown');


const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
    menu: {
        width: 200,
    },
    formLogin: {
        width: '100%',
    },
    textInput: {
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    longPaper: {
        padding: theme.spacing.unit * 3,
        backgroundColor: dark[5],
    },
    gameTitle: {
        fontSize: 48,
        fontFamily: [
            'Balthazar',
        ],
    },
    gameContents: {
        //fontSize: 22,
    },
    Test: {
        display: 'none',
    },
    row: {
        'padding-top': theme.spacing.unit * 3,
        'padding-bottom': theme.spacing.unit * 3,
    },
    divider: {
        //'padding-top': theme.spacing.unit * 2
    },
    sectionTop: {
        'margin-top': theme.spacing.unit * 4,
    },
    sectionBottom: {
        'margin-bottom': theme.spacing.unit * 1,
    },
    sectionText: {
        'background-color': dark[5],
        'padding-left': theme.spacing.unit * 1,
    },
    markdownBody: {

    },
    pageContent: {},
    contentPage: {},
    flavorText: {
        fontStyle: 'italic',
    },
    flavorIndent: {
        paddingLeft: theme.spacing.unit * 5,
    },
    levelCard: {
        paddingBottom: '0 !important',
    },
    sectionPaper: {
        marginBottom: theme.spacing.unit * 3,
    },
    circleProgress: {
        color: theme.palette.text.primary,
    },
    circleProgressError: {
        color: theme.palette.error.dark,
    },
});



class LayoutGameSessionView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loaded: {
                messages: false,
            },
            loading: {
                messages: true,
            },
        }
    }

    componentDidMount() {
        const gameId = this.props.match.params.gameId;
        const sessionId = this.props.match.params.sessionId;

        this.props.clearChatMessages();
        this.props.fetchGame(gameId);
        this.props.fetchGameSession(sessionId);
        this.props.setSection('games');
        this.props.setView('view');
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.isAuthenticated) {
            if (!this.state.loaded.messages) {
                const sessionId = this.props.match.params.sessionId;
                this.setState({
                    loading: {
                        messages: true
                    }
                });
                this.props.fetchAllChatMessages(sessionId)
                    .then(response => {return response})
                    .then(response => {
                        if (response !== undefined) {
                            if (!response.error) {
                                this.setState({
                                    loading: {
                                        messages: false
                                    },
                                    loaded: {
                                        messages: true
                                    }
                                });
                            }
                            else {
                                this.setState({
                                    loading: {
                                        messages: false
                                    },
                                    loaded: {
                                        messages: false
                                    }
                                });
                            }
                        }
                    });
            }
        }
    }

    handleChange = name => event => {
        const game = {
            ...this.props.page.game.model,
            [name]: event.target.value,
        };

        this.setState({
            game: game,
        });
    };


    render() {
        const {classes} = this.props;

        const gameId = this.props.match.params.gameId;
        const sessionId = this.props.match.params.sessionId;

        let chatMessageWindow = undefined;

        if (this.state.loading.messages)
            chatMessageWindow = <CircularProgress className={classes.circleProgress}/>
        else {
            if (this.state.loaded.messages) {
                if (this.props.page.chat.messages.length === 0)
                    chatMessageWindow = <ChatMessage>! This session has no messages</ChatMessage>
                else {
                    chatMessageWindow =
                        this.props.page.chat.messages.map((n, index) => {
                            return (
                                <ChatMessage key={index} className={classes.fullWidth}
                                             alternate={index % 2}>{n.Message}</ChatMessage>
                            );
                        })
                }
            }
            else {
                chatMessageWindow = <CircularProgress className={classes.circleProgressError}/>
            }
        }



        return (
            <div>
                <Grid container className={classes.sectionPaper} spacing={16}>
                    <Grid item lg={8} xs={12}>
                        <Typography
                            variant="display3"
                            className={classes.gameTitle}
                        >{this.props.page.game.model.Name}</Typography>
                        <Divider className={classes.sectionPaper}/>

                        <Button
                            variant='raised'
                            component={Link}
                            to={'/games/' + gameId + '/sessions/' + sessionId + '/play'}
                            className={classes.sectionPaper}
                            color="primary"
                        >
                            Play Game
                        </Button>


                        <Paper className={classes.sectionPaper}>
                            <Grid container>
                                <Grid item xs={12}>
                                    <CardContent>
                                        <Typography variant="title">Summary</Typography>
                                    </CardContent>
                                    <Divider/>
                                </Grid>
                                <Grid item xs={12}>
                                    <CardContent>
                                        <Typography variant="subheading">{this.props.page.game.currentSession.Summary}</Typography>
                                    </CardContent>
                                </Grid>
                            </Grid>
                        </Paper>

                        <Paper className={classes.sectionPaper}>
                            <Grid container>
                                <Grid item xs={12}>
                                    <CardContent>
                                        <Typography variant="title">Description</Typography>
                                    </CardContent>
                                    <Divider/>
                                </Grid>
                                <Grid item xs={12}>
                                    <CardContent>
                                        <Typography variant="body2"><ReactMarkdown source={this.props.page.game.currentSession.Description} /></Typography>
                                    </CardContent>
                                </Grid>
                            </Grid>
                        </Paper>
                    </Grid>

                    <Grid
                        item
                        xs={12}
                        lg={4}
                        className={classes.chatContainer}
                    >
                        <Paper
                            square
                            className={classes.chatContainer}
                        >
                            <Typography
                                variant="display3"
                                className={classes.gameTitle}
                            >Chat Log</Typography>
                            <Divider className={classes.divider}/>
                            <Grid
                                container
                                direction="column"
                                justify="flex-start"
                                alignItems="stretch"
                                spacing={0}
                                className={classes.fullHeight}
                            >
                                <Grid
                                    item xs={12}
                                    className={classNames(classes.fullWidth, classes.scrollable, classes.chatMessages)}
                                    id="chat-list"
                                >
                                    {chatMessageWindow}
                                    <div style={{ float:"left", clear: "both" }}
                                         ref={(el) => { this.messagesEnd = el; }}>
                                    </div>
                                </Grid>
                            </Grid>
                        </Paper>
                    </Grid>
                </Grid>

            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        errors: authErrors(state),
        isAuthenticated: isAuthenticated(state),
        user: state.user,
        user_id: userId(state),
        device: {
            mobile: state.mobile,
        },
        page: {
            game: state.game,
            chat: state.chat,
        },
        nav: {
            section: state.currentSection,
            view: state.currentView,
        },
    }
};



const mapDispatchToProps = (dispatch) => {
    return {
        clearChatMessages: () => dispatch(clearChatMessages()),
        fetchGame: (gameId) => dispatch(loadGame(gameId)),
        fetchGameSession: (sessionId) => dispatch(loadGameSession(sessionId)),
        fetchAllChatMessages: (sessionId) => dispatch(loadAllChatMessages(sessionId)),
        setSection: (section) => dispatch(setSection(section)),
        setView: (view) => dispatch(setView(view)),
    }
};


export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),
    withStyles(styles, {
            withTheme: true
        }
    ))(LayoutGameSessionView);