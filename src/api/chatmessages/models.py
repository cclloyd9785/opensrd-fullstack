from django.db import models
from django.utils import timezone


class ChatMessage(models.Model):
    owner = models.IntegerField('owner', default=0)
    session = models.IntegerField('session', default=0)

    Message = models.TextField('Message', default="", blank=True)
    Character = models.IntegerField('Character', default=0)
    UserMessage = models.BooleanField('UserMessage', default=True)

    created = models.DateTimeField('created_at', default=timezone.now)
    modified = models.DateTimeField('modified_at', default=timezone.now)
    deleted = models.DateTimeField('deleted_at', default=None, null=True, blank=True)

    def __str__(self):
        return 'ChatMessage: %s' % self.Message

