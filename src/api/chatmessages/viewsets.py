from rest_framework import viewsets, mixins
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework.decorators import action

from .serializers import ChatMessageSerializer
from .models import ChatMessage


class ChatMessageViewSet(
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
    ):

    queryset = ChatMessage.objects.all()
    serializer_class = ChatMessageSerializer

    @action(methods=['get'], detail=False)
    def newest(self, request):
        queryset = ChatMessage.objects.order_by('-id')[:30]
        page = self.paginate_queryset(reversed(queryset))
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
