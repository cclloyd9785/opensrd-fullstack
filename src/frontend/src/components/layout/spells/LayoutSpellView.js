import React, { Component } from 'react';

import { withStyles } from '@material-ui/core/styles'
import { Grid, Row, Col } from 'react-flexbox-grid'
import { dark } from '../../App'
import {connect} from "react-redux";
import compose from "recompose/compose";
import { loadSpell } from "../../../actions/spell";
import { setView } from "../../../actions/view";
import { setSection} from "../../../actions/page";
import {authErrors, userId} from "../../../reducers/index";
import { Typography, Divider, CardContent, Paper } from '@material-ui/core'
import classNames from 'classnames';


const ReactMarkdown = require('react-markdown');


const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
    menu: {
        width: 200,
    },
    formLogin: {
        width: '100%',
    },
    textInput: {
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    longPaper: {
        padding: theme.spacing.unit * 3,
        backgroundColor: dark[5],
    },
    spellTitle: {
        fontSize: 48,
        fontFamily: [
            'Balthazar',
        ],
    },
    spellContents: {
        //fontSize: 22,
    },
    Test: {
        display: 'none',
    },
    row: {
        'padding-top': theme.spacing.unit * 3,
        'padding-bottom': theme.spacing.unit * 3,
    },
    divider: {
        //'padding-top': theme.spacing.unit * 2
    },
    sectionTop: {
        'margin-top': theme.spacing.unit * 4,
    },
    sectionBottom: {
        'margin-bottom': theme.spacing.unit * 1,
    },
    sectionText: {
        'background-color': dark[5],
        'padding-left': theme.spacing.unit * 1,
    },
    markdownBody: {

    },
    pageContent: {},
    contentPage: {},
    hidden: {
        display: 'none',
    },
    sectionPaper: {
        marginBottom: theme.spacing.unit * 3,
    },
});



class LayoutSpellView extends Component {
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {

        let regex = /\/spells\/(\d+)/;
        let location = this.props.location.pathname;
        let spellId = location.match(regex)[1];

        this.props.fetchSpell(spellId);
        this.props.setSection('spells');
        this.props.setView('view');
    }

    handleChange = name => event => {
        console.log('event', event.target.value);
        const changes = {
            [name]: event.target.value,
        };
        const spell = {
            ...this.props.page.spell.model,
            [name]: event.target.value,
        };


        this.setState({
            spell: spell,
        });
        //console.log('state', this.state);
    };


    render() {
        const {classes} = this.props;

        return (
            <div>
                <Grid fluid>
                    <Row className={classes.row}>
                        <Col md={12} xs={12} >
                            <Typography
                                variant="display3"
                                className={classes.spellTitle}
                            >{this.props.page.spell.model.Name}</Typography>
                        </Col>
                    </Row>
                </Grid>

                <Paper className={classes.sectionPaper}>
                    <Grid fluid>
                        <Row>
                            <Col xs={12}>
                                <CardContent>
                                    <Typography variant="display1">Spell Info</Typography>
                                </CardContent>
                                <Divider/>
                            </Col>
                        </Row>
                        <Row className={classes.row}>
                            <Col md={5} xs={12} >
                                <CardContent>
                                    <Typography variant="title">Level:</Typography>
                                    <Typography variant="subheading">{this.props.page.spell.model.Level}</Typography>
                                </CardContent>
                            </Col>
                            <Col md={2} xs={6} >
                                <CardContent>
                                    <Typography variant="title">School:</Typography>
                                    <Typography variant="subheading">{this.props.page.spell.model.School} [{this.props.page.spell.model.Subschool}]</Typography>
                                </CardContent>
                            </Col>
                            <Col md={2} xs={6} >
                                <CardContent>
                                    <Typography variant="title">Domain:</Typography>
                                    <Typography variant="subheading">{this.props.page.spell.model.Domain}</Typography>
                                </CardContent>
                            </Col>
                            <Col md={3} xs={12} >
                                <CardContent>
                                    <Typography variant="title">Elemental School:</Typography>
                                    <Typography variant="subheading">{this.props.page.spell.model.ElementalSchool}</Typography>
                                </CardContent>
                            </Col>
                        </Row>
                    </Grid>
                </Paper>

                <Paper className={classes.sectionPaper}>
                    <Grid fluid>
                        <Row>
                            <Col xs={12}>
                                <CardContent>
                                    <Typography variant="display1">Casting</Typography>
                                </CardContent>
                                <Divider/>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={4} xs={12} >
                                <CardContent>
                                    <Typography variant="title">Casting Time:</Typography>
                                    <Typography variant="subheading">{this.props.page.spell.model.CastingTime}</Typography>
                                </CardContent>
                            </Col>
                            <Col md={4} xs={12} >
                                <CardContent>
                                    <Typography variant="title">Components:</Typography>
                                    <Typography variant="subheading">{this.props.page.spell.model.Components}</Typography>
                                </CardContent>
                            </Col>
                        </Row>
                    </Grid>
                </Paper>

                <Paper className={classes.sectionPaper}>
                    <Grid fluid>
                        <Row>
                            <Col xs={12}>
                                <CardContent>
                                    <Typography variant="display1">Effects</Typography>
                                </CardContent>
                                <Divider/>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={4} xs={12} >
                                <CardContent>
                                    <Typography variant="title">Range:</Typography>
                                    <Typography variant="subheading">{this.props.page.spell.model.Range}</Typography>
                                </CardContent>
                            </Col>
                            <Col md={4} xs={12} className={classNames({
                                [classes.hidden]: !this.props.page.spell.model.Area,
                            })}>
                                <CardContent>
                                    <Typography variant="title">Area:</Typography>
                                    <Typography variant="subheading">{this.props.page.spell.model.Area}</Typography>
                                </CardContent>
                            </Col>
                            <Col md={4} xs={12} className={classNames({
                                [classes.hidden]: this.props.page.spell.model.Target === "",
                            })}>
                                <CardContent>
                                    <Typography variant="title">Target:</Typography>
                                    <Typography variant="subheading">{this.props.page.spell.model.Target}</Typography>
                                </CardContent>
                            </Col>
                            <Col md={4} xs={12} >
                                <CardContent>
                                    <Typography variant="title">Duration:</Typography>
                                    <Typography variant="subheading">{this.props.page.spell.model.Duration}</Typography>
                                </CardContent>
                            </Col>
                            <Col md={4} xs={6} >
                                <CardContent>
                                    <Typography variant="title">Save:</Typography>
                                    <Typography variant="subheading">{this.props.page.spell.model.Save}</Typography>
                                </CardContent>
                            </Col>
                            <Col md={4} xs={6} >
                                <CardContent>
                                    <Typography variant="title">SR:</Typography>
                                    <Typography variant="subheading">{this.props.page.spell.model.SR}</Typography>
                                </CardContent>
                            </Col>
                        </Row>
                    </Grid>
                </Paper>

                <Paper className={classes.sectionPaper}>
                    <Grid fluid>
                        <Row>
                            <Col xs={12}>
                                <CardContent>
                                    <Typography variant="display1">Description</Typography>
                                </CardContent>
                                <Divider/>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12} >
                                <CardContent>
                                <Typography
                                    paragraph
                                    variant="subheading"
                                    className={classes.markdownBody}
                                ><ReactMarkdown source={this.props.page.spell.model.Description} /></Typography>
                                </CardContent>
                            </Col>
                        </Row>
                    </Grid>
                </Paper>

                <Paper className={classNames(classes.sectionPaper, {
                    [classes.hidden]: !this.props.page.spell.model.Mythic,
                })}>
                    <Grid>
                        <Row>
                            <Col md={12} xs={12}>
                                <CardContent>
                                    <Typography variant="display1">Mythic</Typography>
                                </CardContent>
                                <Divider/>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12} >
                                <CardContent>
                                    <Typography
                                        paragraph
                                        variant="subheading"
                                        className={classes.markdownBody}
                                    ><ReactMarkdown source={this.props.page.spell.model.Description} /></Typography>
                                </CardContent>
                            </Col>
                        </Row>
                    </Grid>
                </Paper>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        errors: authErrors(state),
        user: state.auth.access,
        user_id: userId(state),
        page: {
            spell: state.spell,
        },
        nav: {
            section: state.currentSection,
            view: state.currentView,
        },
    }
};



const mapDispatchToProps = (dispatch) => {
    return {
        fetchSpell: (spellId) => dispatch(loadSpell(spellId)),
        setSection: (section) => dispatch(setSection(section)),
        setView: (view) => dispatch(setView(view)),
    }
};


export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),
    withStyles(styles, {
            withTheme: true
        }
    ))(LayoutSpellView);