from django.db import models
from django.utils import timezone

from uuid import uuid4


def _generate_unique_uri():
    """Generates a unique uri for the chat session."""
    return str(uuid4()).replace('-', '')[:24]


class ChatSession(models.Model):
    owner = models.IntegerField('owner', default=0)
    game = models.IntegerField('game', default=0)
    session = models.IntegerField('session', default=0)
    uri = models.CharField(max_length=24, default=_generate_unique_uri)

    Name = models.CharField('Name', max_length=256, default="", blank=True)
    Users = models.CharField('Users', max_length=256, default="", blank=True)
    PrivateChat = models.BooleanField('PrivateChat', default=False)
    GroupChat = models.BooleanField('GroupChat', default=False)

    created = models.DateTimeField('created_at', default=timezone.now)
    modified = models.DateTimeField('modified_at', default=timezone.now)
    deleted = models.DateTimeField('deleted_at', default=None, null=True, blank=True)

    def __str__(self):
        return 'ChatSession: %s' % self.Name

