from rest_framework import viewsets, mixins
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.exceptions import Throttled

from .serializers import GameSerializer
from .models import Game
from gamesessions.models import GameSession
from gamesessions.serializers import GameSessionSerializer

import time, datetime

class GameViewSet(
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
    ):

    queryset = Game.objects.all()
    serializer_class = GameSerializer

    @action(methods=['get'], detail=True)
    def newsession(self, request, **id):
        game = self.get_object()

        session = GameSession.objects.filter(game=game.id).order_by('-created').first()
        serializer = GameSessionSerializer(session, many=False)

        # Only allowed to create 1 every 10 minutes
        if time.time() - session.created.timestamp() <= 6:
            raise Throttled(detail="You may only create 1 session every 10 minutes per game.")

        gamesession = GameSession.objects.create()
        gamesession.owner = game.owner
        gamesession.game = game.id
        gamesession.Name = "Session %d" % len(GameSession.objects.filter(game=game.id))
        gamesession.save()


        serializer = GameSessionSerializer(gamesession, many=False)
        return Response(serializer.data)

    @action(methods=['get'], detail=False)
    def newest(self, request):
        queryset = Game.objects.order_by('-id')[:20]
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(methods=['get'], detail=False)
    def oldest(self, request):
        queryset = Game.objects.order_by('id')[:20]
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(methods=['get', 'post'], detail=False)
    def mine(self, request):
        if request.method == 'POST':
            queryset = Game.objects.filter(owner=request.POST.get('id'))
            page = self.paginate_queryset(queryset)
            if page is not None:
                serializer = self.get_serializer(page, many=True)
                return self.get_paginated_response(serializer.data)
        queryset = Game.objects.filter(owner=1)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(methods=['get'], detail=True)
    def sessions(self, request, **game_id):
        game = self.get_object()
        sessions = GameSession.objects.filter(game=game.id)

        serializer = GameSessionSerializer(sessions, many=True)
        return Response(serializer.data)
