
export const SET_MOBILE = '@@mobile/SET_MOBILE';

const mobileSuccess = (mobile) => {
    return {
        type: SET_MOBILE,
        payload: mobile,
    }
};

export const isMobile = (mobile) => {
    return function (dispatch) {
        dispatch(mobileSuccess(mobile));
        return mobile;
    }
};
