from rest_framework import viewsets, mixins
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.exceptions import PermissionDenied
from rest_framework.exceptions import NotFound
from rest_framework.exceptions import Throttled

from .models import GameSession
from chatsessions.models import ChatSession
from chatmessages.models import ChatMessage
from .serializers import GameSessionSerializer
from chatsessions.serializers import ChatSessionSerializer
from chatmessages.serializers import ChatMessageSerializer


class GameSessionViewSet(
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
    ):

    queryset = GameSession.objects.all()
    serializer_class = GameSessionSerializer

    @action(methods=['get'], detail=True)
    def chatsession(self, request, **id):

        gamesession = self.get_object()
        session = ChatSession.objects.filter(session=gamesession.id)
        if len(session) == 0:
            return Response([])
        else:
            session = session[0]

        if not session.id:
            error = dict()
            error['error'] = True
            error['message'] = "Not found"
            return Response(error, status=404)

        serializer = ChatSessionSerializer(session, many=False)
        return Response(serializer.data)

    @action(methods=['get'], detail=True)
    def messages(self, request, **id):
        gamesession = self.get_object()
        chatsession = ChatSession.objects.filter(session=gamesession.id)
        if len(chatsession) == 0:
            return Response([])
        else:
            chatsession = chatsession[0]
        messages = ChatMessage.objects.filter(session=chatsession.id)

        if not chatsession.id:
            error = dict()
            error['error'] = True
            error['message'] = "Not found"
            return Response(error, status=404)

        serializer = ChatMessageSerializer(messages, many=True)
        return Response(serializer.data)

    @action(methods=['get'], detail=True)
    def messagesrecent(self, request, **id):
        gamesession = self.get_object()
        chatsession = ChatSession.objects.filter(session=gamesession.id)
        if len(chatsession) == 0:
            return Response([])
        else:
            chatsession = chatsession[0]
        messages = ChatMessage.objects.filter(session=chatsession.id).order_by('id')

        if len(messages) == 0:
            return Response([])
        else:
            messages = messages[:10]

        if not chatsession.id:
            error = dict()
            error['error'] = True
            error['message'] = "Not found"
            return Response(error, status=404)

        serializer = ChatMessageSerializer(messages, many=True)
        return Response(serializer.data)

