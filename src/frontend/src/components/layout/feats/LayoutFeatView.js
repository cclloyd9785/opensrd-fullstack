import React, { Component } from 'react';
import classNames from 'classnames';

import { withStyles } from '@material-ui/core/styles'
import { Grid, Row, Col } from 'react-flexbox-grid'
import { dark } from '../../App'
import {connect} from "react-redux";
import compose from "recompose/compose";
import { loadFeat } from "../../../actions/feat";
import { setView } from "../../../actions/view";
import { setSection} from "../../../actions/page";
import {authErrors, userId} from "../../../reducers/index";
import { TextField, Paper, Typography, Divider, Card, CardHeader, CardContent, Button, IconButton, Checkbox } from '@material-ui/core'


const ReactMarkdown = require('react-markdown');


const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
    menu: {
        width: 200,
    },
    formLogin: {
        width: '100%',
    },
    textInput: {
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    longPaper: {
        padding: theme.spacing.unit * 3,
        backgroundColor: dark[5],
    },
    featTitle: {
        fontSize: 48,
        fontFamily: [
            'Balthazar',
        ],
    },
    featContents: {
        //fontSize: 22,
    },
    Test: {
        display: 'none',
    },
    row: {
        'padding-top': theme.spacing.unit * 3,
        'padding-bottom': theme.spacing.unit * 3,
    },
    divider: {
        //'padding-top': theme.spacing.unit * 2
    },
    sectionTop: {
        'margin-top': theme.spacing.unit * 4,
    },
    sectionBottom: {
        'margin-bottom': theme.spacing.unit * 1,
    },
    sectionText: {
        'background-color': dark[5],
        'padding-left': theme.spacing.unit * 1,
    },
    markdownBody: {

    },
    pageContent: {},
    contentPage: {},
    flavorText: {
        fontStyle: 'italic',
    },
    flavorIndent: {
        paddingLeft: theme.spacing.unit * 5,
    },
    levelCard: {
        paddingBottom: '0 !important',
    },
    sectionPaper: {
        marginBottom: theme.spacing.unit * 3,
    },
});



class LayoutFeatView extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        let regex = /\/feats\/(\d+)/;
        let location = this.props.location.pathname;
        let featId = location.match(regex)[1];

        this.props.fetchFeat(featId);
        this.props.setSection('feats');
        this.props.setView('view');
    }

    handleChange = name => event => {
        const feat = {
            ...this.props.page.feat.model,
            [name]: event.target.value,
        };

        this.setState({
            feat: feat,
        });
    };


    render() {
        const {classes} = this.props;

        return (
            <div>
                <Grid fluid>
                    <Row className={classes.row}>
                        <Col md={12} xs={12} >
                            <Typography
                                variant="display3"
                                className={classes.featTitle}
                            >{this.props.page.feat.model.Name}</Typography>
                        </Col>
                    </Row>

                    <Divider className={classes.divider}/>

                    <Row className={classes.row}>
                        <Col md={12} xs={12} >
                            <Typography className={classNames(classes.flavorText, {
                                [classes.flavorIndent]: !this.props.device.mobile,
                            })} variant="title">{this.props.page.feat.model.Flavor}</Typography>
                        </Col>
                    </Row>
                </Grid>

                <Paper className={classes.sectionPaper}>
                    <Grid fluid>
                        <Row>
                            <Col md={12} xs={12}>
                                <CardContent>
                                    <Typography variant="title">Prerequisite:</Typography>
                                </CardContent>
                                <Divider/>
                            </Col>
                            <Col md={12} xs={12}>
                                <CardContent>
                                    <Typography variant="body2">{this.props.page.feat.model.Prerequisite}</Typography>
                                </CardContent>
                            </Col>
                        </Row>
                    </Grid>
                </Paper>


                <Paper className={classes.sectionPaper}>
                    <Grid>
                        <Row>
                            <Col md={12} xs={12}>
                                <CardContent>
                                    <Typography variant="title">Benefit</Typography>
                                </CardContent>
                                <Divider/>
                            </Col>
                            <Col md={12} xs={12} className={classNames({[classes.hidden]: this.props.page.feat.model.metaFeat})}>
                                <CardContent className={classes.levelCard}>
                                    <Typography variant="subheading">Level increase: {this.props.page.feat.model.MetaLevelIncrease}</Typography>
                                </CardContent>
                            </Col>
                            <Col md={12} xs={12}>
                                <CardContent>
                                    <Typography variant="body2"><ReactMarkdown source={this.props.page.feat.model.Benefit} /></Typography>
                                </CardContent>
                            </Col>
                        </Row>
                    </Grid>
                </Paper>


                <Paper className={classes.sectionPaper}>
                    <Grid>
                        <Row>
                            <Col md={12} xs={12}>
                                <CardContent>
                                    <Typography variant="title">Normal</Typography>
                                </CardContent>
                                <Divider/>
                            </Col>
                            <Col md={12} xs={12}>
                                <CardContent>
                                    <Typography variant="body2">{this.props.page.feat.model.Normal}</Typography>
                                </CardContent>
                            </Col>
                        </Row>
                    </Grid>
                </Paper>

                <Paper className={classNames(classes.sectionPaper, {
                    [classes.hidden]: !this.props.page.feat.model.Mythic,
                })}>
                    <Grid>
                        <Row>
                            <Col md={12} xs={12}>
                                <CardContent>
                                    <Typography variant="title">Mythic</Typography>
                                </CardContent>
                                <Divider/>
                            </Col>
                            <Divider/>
                        </Row>
                        <Row className={classes.row}>
                            <Col md={12} xs={12} >
                                <CardContent>
                                    <Typography className={classNames(classes.flavorText, {
                                        [classes.flavorIndent]: !this.props.device.mobile,
                                    })} variant="title">{this.props.page.feat.model.MythicFlavor}</Typography>
                                </CardContent>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} xs={12}>
                                <Divider/>
                                <CardContent>
                                    <Typography variant="title">Prerequisites:</Typography>
                                    <Typography variant="body2">{this.props.page.feat.model.MythicPrerequisite}</Typography>
                                </CardContent>
                            </Col>
                            <Col md={12} xs={12}>
                                <CardContent>
                                    <Typography variant="title">Benefit:</Typography>
                                    <Typography variant="body2"><ReactMarkdown source={this.props.page.feat.model.MythicBenefit} /></Typography>
                                </CardContent>
                            </Col>
                        </Row>
                    </Grid>
                </Paper>

            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        errors: authErrors(state),
        user: state.auth.access,
        user_id: userId(state),
        device: {
            mobile: state.mobile,
        },
        page: {
            feat: state.feat,
        },
        nav: {
            section: state.currentSection,
            view: state.currentView,
        },
    }
};



const mapDispatchToProps = (dispatch) => {
    return {
        fetchFeat: (featId) => dispatch(loadFeat(featId)),
        setSection: (section) => dispatch(setSection(section)),
        setView: (view) => dispatch(setView(view)),
    }
};


export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),
    withStyles(styles, {
            withTheme: true
        }
    ))(LayoutFeatView);