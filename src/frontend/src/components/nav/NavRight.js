import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router'
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { Toolbar, Drawer, Typography, Button, Hidden, List, ListItem, ListItemText, Divider, Avatar, CardHeader } from '@material-ui/core';
import ArrowForward from '@material-ui/icons/ArrowForward';
import { dark } from '../App'
import { connect } from 'react-redux'
import compose from 'recompose/compose';
import { authErrors } from "../../reducers";
import { switchNavRight } from "../../actions/navRight";
import {Link} from "react-router-dom";

export const drawerWidth = 300;

const styles = theme => ({
    NavTools: {
        justifyContent: 'right',
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 20,
    },
    hide: {
        display: 'none',
    },
    drawerPaper: {
        position: 'fixed',
        right: 0,
        top: 0,
        width: drawerWidth,
        backgroundColor: dark[3],
        '&::-webkit-scrollbar': {
            width: 2,
        }
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'left',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    hidden: {
        display: 'none',
    },
    navAvatar: {
        width: 30,
        height: 30,
    },
    username: {
        marginLeft: theme.spacing.unit * 2,
    },
    userToolbar: {
        justifyContent: 'flex-start',
        width: '100%',
    },
});



class NavRight extends React.Component {
    state = {
        navRight: true,
        mobile: false,
        drawerVariant: 'persistent',
    };

    constructor(props) {
        super(props);

        const mobile = (window.innerWidth <= 1024);

        this.state = {
            navRight: !mobile,
            mobile: mobile,
            drawerVariant: mobile ? 'temporary' : 'persistent',
        }
    }

    toggleNavRight = () => {
        const { nav } = this.props;
        const newState = !nav.navRight;
        this.setState({ navRight: !this.state.navRight });
        this.props.toggleNavRight(newState);
    };


    render() {
        const { classes } = this.props;
        const { navLeft, navRight, mobile, drawerVariant, spell } = this.state;

        let currentSession = undefined;
        if (this.props.page.game) {
            if (this.props.page.game.currentSession) {
                currentSession =
                    <div className={classes.drawerHeader}>
                        <Toolbar className={classes.userToolbar}>
                            <Button type='raised' component={Link}
                                    to={'/games/' + this.props.page.game.currentSession.game + '/sessions/' + this.props.page.game.currentSession.id + '/play'}>Join Current Session</Button>
                        </Toolbar>
                    </div>
            }
        }

        return (
            <Drawer
                variant={drawerVariant}
                anchor={'right'}
                open={this.props.nav.navRight}
                classes={{
                    paper: classes.drawerPaper,
                }}
            >
                <Hidden mdUp>
                    <div className={classes.drawerHeader}>
                        <Toolbar>
                            <Button
                                color="primary"
                                aria-label="close drawer"
                                variant="raised"
                                onClick={this.toggleNavRight}
                                className={classNames(classes.menuButton)}
                            >
                                <ArrowForward />
                            </Button>
                        </Toolbar>
                    </div>
                    <Divider />
                </Hidden>
                <div className={classes.drawerHeader}>
                    <Toolbar className={classes.userToolbar}>
                        <Avatar src={this.props.user.gravatar} />
                        <Typography variant="title" className={classes.username}>{this.props.user.username}</Typography>
                    </Toolbar>
                </div>
                <Divider />
                {currentSession}
                <List>
                    <ListItem button>
                        <ListItemText primary="Dice Roller" />
                    </ListItem>
                    <ListItem button>
                        <ListItemText primary="Sheet Quick Access" />
                    </ListItem>
                    <ListItem button>
                        <ListItemText primary="Macros" />
                    </ListItem>
                </List>
            </Drawer>
        );
    }
}

NavRight.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
    errors: authErrors(state),
    page: {
        spell: state.spell,
        game: state.game,
    },
    device: {
        mobile: state.mobile,
    },
    nav: {
        navRight: state.navRight,
    },
    user: state.user,
});

const mapDispatchToProps = (dispatch) => {
    return {
        toggleNavRight: (newState) => dispatch(switchNavRight(newState)),
    }
};

export default compose(
    withRouter,
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
    withStyles(styles, {
            withTheme: true
        }
    ))(NavRight);
