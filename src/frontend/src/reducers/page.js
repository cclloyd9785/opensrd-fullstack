import * as page_action from '../actions/page'
import {isRefreshTokenExpired} from "./auth";


const initialState = 'home';

export default (state=initialState, action) => {
    switch(action.type) {
        case page_action.SET_SECTION:
            return action.payload;
        case page_action.GET_SECTION:
            return state;
        default:
            return state;
    }
}

export function getSection(state) {
    if (state.currentSection) {
        return state.currentSection;
    }
}

