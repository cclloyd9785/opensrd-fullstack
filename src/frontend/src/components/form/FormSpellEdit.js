import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, formValueSelector, getFormValues } from 'redux-form'
import { TextField, Paper, Typography, Divider, Card, CardHeader, CardContent, Button, IconButton, Checkbox } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import { Grid, Row, Col } from 'react-flexbox-grid'
import { dark } from '../App'
import {connect} from "react-redux";
import compose from "recompose/compose";
import {userId, authErrors, accessToken, currentSpell} from "../../reducers";
import {updateSpell} from "../../actions/spell";
import SaveIcon from '@material-ui/icons/Save';
import CachedIcon from '@material-ui/icons/Cached';
import {renderCheckbox, renderSelectField, renderSwitch, renderTextField} from "./reduxFormMaterialUi";
import classNames from 'classnames';

const ReactMarkdown = require('react-markdown');

const styles = theme => ({
    textInput: {
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    longPaper: {
        padding: theme.spacing.unit * 3,
        backgroundColor: dark[5],
    },
    spellTitle: {
        fontSize: 48,
        fontFamily: [
            'Balthazar',
        ],
    },
    spellContents: {
        fontSize: 22,
    },
    row: {
        'padding-top': theme.spacing.unit * 3,
        'padding-bottom': theme.spacing.unit * 3,
    },
    divider: {
        //'padding-top': theme.spacing.unit * 2
    },
    sectionTop: {
        'margin-top': theme.spacing.unit * 4,
    },
    sectionBottom: {
        'margin-bottom': theme.spacing.unit * 1,
    },
    sectionText: {
        'background-color': dark[5],
        'padding-left': theme.spacing.unit * 1,
    },
    markdownBody: {

    },
    pageContent: {},
    contentPage: {},
    sectionHeader: {
        'font-size': 24,
    },
    sectionPaper: {
        'margin-top': theme.spacing.unit * 3,
        'margin-bottom': theme.spacing.unit * 3,
    },
    spellCardContent: {
        'padding-left': 0,
        'padding-right': 0,
    },
    inlineTypography: {
        display: 'inline-block',
    }
});

class FormSpellEdit extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        //this.props.initialize(this.state)
    }

    render() {
        const {classes} = this.props;

        const { submit, handleSubmit, pristine, reset, submitting } = this.props;

        return (
            <form
                onSubmit={handleSubmit(this.props.sendSubmit)}
            >
                <Button
                    variant="fab"
                    color="primary"
                    aria-label="Save"
                    disabled={pristine || submitting}
                    onClick={handleSubmit(this.props.sendSubmit)}
                >
                    <SaveIcon/>
                </Button>

                <Paper className={classes.sectionPaper}>
                    <CardContent>
                        <Typography className={classes.sectionHeader}>Spell Info</Typography>
                        <Divider />
                    </CardContent>
                    <CardContent>
                        <Grid fluid>
                            <Row>
                                <Col xs={12} lg={4}>
                                    <CardContent className={classes.spellCardContent}>
                                        <Typography>Spell Name</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Name"
                                            component={renderTextField}
                                            label="Spell Name"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={12} lg={8}>
                                    <CardContent className={classes.spellCardContent}>
                                        <Typography>Level</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Level"
                                            component={renderTextField}
                                            label="Level"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={12}>
                                    <Divider />
                                </Col>
                                <Col xs={6} lg={3}>
                                    <CardContent className={classes.spellCardContent}>
                                        <Typography>School</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="School"
                                            component={renderTextField}
                                            label="Spell School"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={6} lg={3}>
                                    <CardContent className={classes.spellCardContent}>
                                        <Typography>Subschool</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Subschool"
                                            component={renderTextField}
                                            label="Subschool"
                                        />
                                    </CardContent>
                                </Col>

                                <Col xs={6} lg={3}>
                                    <CardContent className={classes.spellCardContent}>
                                        <Typography>Domain</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Domain"
                                            component={renderTextField}
                                            label="Domain"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={6} lg={3}>
                                    <CardContent className={classes.spellCardContent}>
                                        <Typography>Elemental School</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="ElementalSchool"
                                            component={renderTextField}
                                            label="Elemental School"
                                        />
                                    </CardContent>
                                </Col>
                            </Row>
                        </Grid>
                    </CardContent>
                </Paper>

                <Paper className={classes.sectionPaper}>
                    <CardContent>
                        <Typography className={classes.sectionHeader}>Casting</Typography>
                        <Divider />
                    </CardContent>
                    <CardContent>
                        <Grid fluid>
                            <Row>
                                <Col xs={12} lg={2}>
                                    <CardContent className={classes.spellCardContent}>
                                        <Typography>Casting Time</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="CastingTime"
                                            component={renderTextField}
                                            label="Casting Time"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={12} lg={4}>
                                    <CardContent className={classes.spellCardContent}>
                                        <Typography>Components</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Components"
                                            component={renderTextField}
                                            label="Components"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={12} lg={2}>
                                    <CardContent className={classes.spellCardContent}>
                                        <Typography>Components (short)</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="ComponentsShort"
                                            component={renderTextField}
                                            label="Components (shorthand)"
                                        />
                                    </CardContent>
                                </Col>
                            </Row>
                        </Grid>
                    </CardContent>
                </Paper>

                <Paper className={classes.sectionPaper}>
                    <CardContent>
                        <Typography className={classes.sectionHeader}>Effect</Typography>
                        <Divider />
                    </CardContent>
                    <CardContent>
                        <Grid fluid>
                            <Row>
                                <Col xs={6} lg={2}>
                                    <CardContent className={classes.spellCardContent}>
                                        <Typography>Range</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Range"
                                            component={renderTextField}
                                            label="Range"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={6} lg={3}>
                                    <CardContent className={classes.spellCardContent}>
                                        <Typography>Area</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Area"
                                            component={renderTextField}
                                            label="Area"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={6} lg={3}>
                                    <CardContent className={classes.spellCardContent}>
                                        <Typography>Target</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Target"
                                            component={renderTextField}
                                            label="Target"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={6} lg={2}>
                                    <CardContent className={classes.spellCardContent}>
                                        <Typography>Duration</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Duration"
                                            component={renderTextField}
                                            label="Duration"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={6} lg={1}>
                                    <CardContent className={classes.spellCardContent}>
                                        <Typography>Saving Throw</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="Save"
                                            component={renderTextField}
                                            label="Saving Throw"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={6} lg={1}>
                                    <CardContent className={classes.spellCardContent}>
                                        <Typography>SR?</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="SR"
                                            component={renderTextField}
                                            label="Spell Resistance"
                                        />
                                    </CardContent>
                                </Col>
                            </Row>
                        </Grid>
                    </CardContent>
                </Paper>

                <Paper className={classes.sectionPaper}>
                    <CardContent>
                        <Typography className={classes.sectionHeader}>Description</Typography>
                        <Divider />
                    </CardContent>
                    <CardContent>
                        <Grid fluid>
                            <Row>
                                <Col xs={12} lg={12}>
                                    <CardContent className={classes.spellCardContent}>
                                        <Typography>Full Description</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            multiline
                                            name="Description"
                                            component={renderTextField}
                                            label="Description"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={12} lg={12}>
                                    <CardContent className={classes.spellCardContent}>
                                        <Typography>Short Description</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="DescriptionShort"
                                            component={renderTextField}
                                            label="Description (short)"
                                        />
                                    </CardContent>
                                </Col>
                            </Row>
                        </Grid>
                    </CardContent>
                </Paper>

                <Paper className={classes.sectionPaper}>
                    <CardContent>
                        <Typography className={classNames(classes.sectionHeader, classes.inlineTypography)}>Mythic</Typography>
                        <Field
                            name="Mythic"
                            component={renderSwitch}
                            label="Mythic?"
                        />
                        <Divider />
                    </CardContent>
                    <CardContent>
                        <Grid fluid>
                            <Row>
                                <Col xs={12} lg={12}>
                                    <CardContent className={classes.spellCardContent}>
                                        <Typography>Full Mythic Description</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            multiline
                                            name="MythicDescription"
                                            component={renderTextField}
                                            label="Mythic Description"
                                        />
                                    </CardContent>
                                </Col>
                                <Col xs={12} lg={12}>
                                    <CardContent className={classes.spellCardContent}>
                                        <Typography>Short Description</Typography>
                                        <Divider />
                                        <Field
                                            fullWidth
                                            name="MythicDescriptionShort"
                                            component={renderTextField}
                                            label="Mythic Description (short)"
                                        />
                                    </CardContent>
                                </Col>
                            </Row>
                        </Grid>
                    </CardContent>
                </Paper>

            </form>
        );
    }
}

FormSpellEdit.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
    return {
        errors: authErrors(state),
        user: state.auth.access,
        user_id: userId(state),
        page: {
            spell: state.spell
        },
        nav: {
            section: state.currentSection,
            view: state.currentView,
        },
        initialValues: state.spell,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        sendSubmit: (values) => dispatch(updateSpell(values)),
    }
};


export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
    reduxForm({
        form: 'FormSpellEdit',
        enableReinitialize: true
    }),
    withStyles(styles, {
            withTheme: true
        },
    ))(FormSpellEdit);


