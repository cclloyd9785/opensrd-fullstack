import React, { Component } from 'react';
import { reduxForm } from 'redux-form'
import { withStyles } from '@material-ui/core/styles'
import {connect} from "react-redux";
import compose from "recompose/compose";
import { loadFeat } from "../../../actions/feat";
import { setView } from "../../../actions/view";
import { setSection} from "../../../actions/page";
import { userId, authErrors } from "../../../reducers/index";
import FormFeatEdit from '../../form/FormFeatEdit';


const styles = theme => ({

});

class LayoutFeatEdit extends Component {
    constructor(props) {
        super(props);

    }

    componentDidMount() {
        let regex = /\/feats\/(\d+)/;
        let location = this.props.location.pathname;
        let featId = location.match(regex)[1];

        this.props.fetchFeat(featId);
        this.props.setSection('feats');
        this.props.setView('edit');
    }
    onSubmit = (e) => {
        e.preventDefault();
        this.props.onSubmit(this.state.feat)
    };



    render() {
        return (
            <div>
                <FormFeatEdit />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        errors: authErrors(state),
        user: state.auth.access,
        user_id: userId(state),
        page: {
            feat: state.feat
        },
        nav: {
            section: state.currentSection,
            view: state.currentView,
        },
        initialValues: state.feat,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchFeat: (featId) => dispatch(loadFeat(featId)),
        setSection: (section) => dispatch(setSection(section)),
        setView: (view) => dispatch(setView(view)),
    }
};


export default compose(
    reduxForm({
        form: 'FeatEditForm',
    }),
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
    withStyles(styles, {
            withTheme: true
        },
    ))(LayoutFeatEdit);