import React from "react";
import { Checkbox, Select, TextField, Switch, InputAdornment } from "@material-ui/core";
import {dark} from "../App";
import compose from "recompose/compose";
import connect from "react-redux/es/connect/connect";
import {Field, reduxForm} from "redux-form";
import {withStyles} from "@material-ui/core/styles";


export const renderTextField = ({input, label, meta: { touched, error }, ...custom}) => (
    <TextField
        error={touched && error}
        {...input}
        {...custom}
    />
);

export const renderTextFieldCentered = ({input, label, meta: { touched, error }, ...custom}) => (
    <TextField
        error={touched && error}
        {...input}
        {...custom}
        inputProps={{style: {textAlign: 'center',}}}
    />
);

export const renderModTextField = ({input, label, meta: { touched, error }, ...custom}) => (
    <TextField
        error={touched && error}
        {...input}
        {...custom}
        inputProps={{style: {textAlign: 'center',}}}
        InputProps={{
            startAdornment: <InputAdornment position="start" style={{position: 'absolute', bottom: 0, height: '100%',}}>{input.value >= 0 ? "+" : "-"}</InputAdornment>,
        }}
    />
);

//export const withStyles(styles, {withTheme: true})(renderModTextField)

// export default withStyles(styles, {withTheme: true})(FormSheetEdit);

export const renderCheckbox = ({ input, label, ...custom }) => (
    <Checkbox
        label={label}
        //defaultChecked={input.value ? true : false}
        checked={input.value ? true : false}
        {...custom}
        {...input}
    />
);

export const renderSwitch = ({ input, label, ...custom }) => (
    <Switch
        label={label}
        //defaultChecked={input.value ? true : false}
        checked={input.value ? true : false}
        {...custom}
        {...input}
    />
);


export const renderSelectField =
    ({
       input,
       label,
       meta: { touched, error },
       children,
       ...custom
   }) => (
    <Select
        {...input}
        //onChange={(event, index, value) => input.onChange(value)}
        children={children}
        {...custom}
    />
);