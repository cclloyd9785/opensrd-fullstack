from rest_framework import routers
from .viewsets import ChatMessageViewSet

router = routers.SimpleRouter()

router.register(r'^api/chatmessages', ChatMessageViewSet, 'chatmessages')
