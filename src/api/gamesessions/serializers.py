from rest_framework import views, serializers, status
from rest_framework.exceptions import PermissionDenied

from .models import GameSession

from chatsessions import serializers as chat_serializer

class GameSessionSerializer(serializers.ModelSerializer):
    class Meta:
        model = GameSession
        fields = '__all__'

    def create(self, validated_data):
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
        else:
            return PermissionDenied()

        validated_data['owner'] = user.id
        game_session = GameSession.objects.create(**validated_data)

        # Create chat session for this game session
        chat_session_data = dict()
        chat_session_data['owner'] = user.id
        chat_session_data['session'] = game_session.id
        chat_session_data['Name'] = game_session.Name
        chat_session = chat_serializer.ChatSessionSerializer.create(chat_serializer.ChatSessionSerializer.instance(), chat_session_data)

        game_session.session = chat_session
        game_session.save()

        return game_session

    def update(self, instance, validated_data):
        # Update and return an existing `Snippet` instance, given the validated data.

        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user

            if user.id != instance.owner and not request.user.is_staff:
                raise PermissionDenied({"message": "You don't have permission to access this game"})


        [setattr(instance, k, v) for k, v in validated_data.items()]
        instance.save()
        return instance
