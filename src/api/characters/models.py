from django.db import models
from django.utils import timezone
from django.contrib.postgres.fields.jsonb import JSONField
import datetime


class Character(models.Model):
    owner = models.IntegerField('owner', default=0)
    game = models.IntegerField('game', default=0)

    Name = models.CharField('Name', max_length=128, default="", blank=True)
    Avatar = models.CharField('Avatar', max_length=128, default="", blank=True)
    NPC = models.BooleanField('NPC', default=False)

    created = models.DateTimeField('created_at', default=timezone.now)
    modified = models.DateTimeField('modified_at', default=timezone.now)
    deleted = models.DateTimeField('deleted_at', default=None, null=True, blank=True)

    def __str__(self):
        return 'Character: %s' % self.Name

