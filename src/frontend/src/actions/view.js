
export const GET_VIEW = '@@view/GET_VIEW';
export const SET_VIEW = '@@view/SET_VIEW';

const viewSuccess = () => {
    return {
        type: GET_VIEW,
        payload: null,
    }
};

const setViewSuccess = (view) => {
    return {
        type: SET_VIEW,
        payload: view,
    }
};

export const loadView = () => {
    return function (dispatch) {
        dispatch(viewSuccess());
    }
};

export const setView = (view) => {
    return function (dispatch) {
        dispatch(setViewSuccess(view));
    }
};
