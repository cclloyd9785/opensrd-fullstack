import * as user_action from '../actions/user'

const md5 = require('md5');

const initialState = {};

export default (state=initialState, action) => {
    switch(action.type) {
        case user_action.FETCH_USER_SUCCESS:
            let user = action.payload;
            user.gravatar = "https://www.gravatar.com/avatar/" + md5(user.email) + '.png';
            return user;
        default:
            return state;
    }
}

export function loadUser(state) {
    if (state.user) {
        return state.user
    }
}
