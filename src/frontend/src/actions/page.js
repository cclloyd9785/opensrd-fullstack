
export const GET_SECTION = '@@page/GET_SECTION';
export const SET_SECTION = '@@page/SET_SECTION';

const sectionSuccess = () => {
    return {
        type: GET_SECTION,
        payload: null,
    }
};

const sectionSetSuccess = (section) => {
    return {
        type: SET_SECTION,
        payload: section,
    }
};

export const loadSection = () => {
    return function (dispatch) {
        dispatch(sectionSuccess());
    }
};

export const setSection = (section) => {
    return function (dispatch) {
        dispatch(sectionSetSuccess(section));
    }
};


