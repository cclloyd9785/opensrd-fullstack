from django.db import models
from django.utils import timezone
import datetime
# Create your models here.


class Sheet(models.Model):
    owner = models.IntegerField('owner', default=0)

    Name = models.CharField('Name', max_length=64, default="", blank=True)
    Player = models.CharField('Name', max_length=64, default="", blank=True)
    Alignment = models.CharField('Name', max_length=2, default="", blank=True)
    Class = models.CharField('Name', max_length=64, default="", blank=True)
    Race = models.CharField('Name', max_length=64, default="", blank=True)
    Deity = models.CharField('Name', max_length=64, default="", blank=True)

    Size = models.CharField('Name', max_length=16, default="", blank=True)
    Height = models.CharField('Name', max_length=16, default="", blank=True)
    Weight = models.CharField('Name', max_length=16, default="", blank=True)
    Age = models.CharField('Name', max_length=16, default="", blank=True)
    Gender = models.CharField('Name', max_length=16, default="", blank=True)
    Eyes = models.CharField('Name', max_length=32, default="", blank=True)
    Hair = models.CharField('Name', max_length=32, default="", blank=True)

    Level = models.CharField('Name', max_length=64, default="", blank=True)
    XPCurrent = models.CharField('Name', max_length=64, default="", blank=True)
    XPNext = models.CharField('Name', max_length=64, default="", blank=True)
    XPChange = models.CharField('Name', max_length=64, default="", blank=True)
    XPSpeed = models.CharField('Name', max_length=64, default="", blank=True)

    StrScore = models.IntegerField('StrScore', default=10, blank=True)
    StrMod = models.IntegerField('StrMod', default=0, blank=True)
    StrTemp = models.IntegerField('StrTemp', default=0, blank=True)
    StrTempMod = models.IntegerField('StrTempMod', default=0, blank=True)

    DexScore = models.IntegerField('DexScore', default=10, blank=True)
    DexMod = models.IntegerField('DexMod', default=0, blank=True)
    DexTemp = models.IntegerField('DexTemp', default=0, blank=True)
    DexTempMod = models.IntegerField('DexTempMod', default=0, blank=True)

    ConScore = models.IntegerField('ConScore', default=10, blank=True)
    ConMod = models.IntegerField('ConMod', default=0, blank=True)
    ConTemp = models.IntegerField('ConTemp', default=0, blank=True)
    ConTempMod = models.IntegerField('ConTempMod', default=0, blank=True)

    IntScore = models.IntegerField('IntScore', default=10, blank=True)
    IntMod = models.IntegerField('IntMod', default=0, blank=True)
    IntTemp = models.IntegerField('IntTemp', default=0, blank=True)
    IntTempMod = models.IntegerField('IntTempMod', default=0, blank=True)

    WisScore = models.IntegerField('WisScore', default=10, blank=True)
    WisMod = models.IntegerField('WisMod', default=0, blank=True)
    WisTemp = models.IntegerField('WisTemp', default=0, blank=True)
    WisTempMod = models.IntegerField('WisTempMod', default=0, blank=True)

    ChaScore = models.IntegerField('ChaScore', default=10, blank=True)
    ChaMod = models.IntegerField('ChaMod', default=0, blank=True)
    ChaTemp = models.IntegerField('ChaTemp', default=0, blank=True)
    ChaTempMod = models.IntegerField('ChaTempMod', default=0, blank=True)


    '''
    Prerequisite =  models.CharField('Prerequisite', max_length=255, default="", blank=True)
    Flavor =        models.CharField('Flavor', max_length=255, default="", blank=True)
    Benefit =           models.TextField('Benefit', default="", blank=True)
    BenefitShort =      models.CharField('BenefitShort', max_length=255, default="", blank=True)
    Normal =            models.CharField('Normal', max_length=255, default="", blank=True)
    MetaSheet =          models.BooleanField('MetaSheet', default=False)
    MetaLevelIncrease = models.CharField('MetaLevelIncrease', max_length=32, default="", blank=True)


    Mythic = models.BooleanField('Mythic', default=False)
    MythicFlavor = models.CharField('MythicFlavor', max_length=255, default="", blank=True)
    MythicPrerequisite = models.CharField('MythicPrerequisite', max_length=255, default="", blank=True)
    MythicBenefit = models.TextField('MythicBenefit', default="", blank=True)
    MythicBenefitShort = models.CharField('MythicBenefitShort', max_length=255, default="", blank=True)
    '''
    Views = models.IntegerField('Views', default=0)


    created = models.DateTimeField('created_at', default=timezone.now)
    modified = models.DateTimeField('modified_at', default=timezone.now)
    deleted = models.DateTimeField('deleted_at', default=None, null=True, blank=True)

    def __str__(self):
        return 'Sheet: %s' % self.Name

