import React from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router'
import compose from 'recompose/compose';
import { connect } from 'react-redux'

import { dark } from '../../App'
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import {Toolbar, Drawer, Button, Hidden, List, ListItem, ListItemText, Divider} from '@material-ui/core';
import ArrowBack from '@material-ui/icons/ArrowBack';
import { authErrors, userId } from "../../../reducers";
import { isMobile } from "../../../actions/mobile";
import { logout } from "../../../actions/auth";
import { loadSection } from "../../../actions/page";
import { loadView } from "../../../actions/view";
import NavLeftSpells from './NavLeftSpells';
import NavLeftFeats from './NavLeftFeats';
import NavLeftArtifacts from './NavLeftArtifacts';
import NavLeftHome from './NavLeftHome';
import { switchNavLeft } from "../../../actions/navLeft";
import NavLeftSheets from "./NavLeftSheets";
import NavLeftGames from "./NavLeftGames";

export const drawerWidth = 200;

const styles = theme => ({
    menuButton: {
        marginLeft: 12,
        marginRight: 20,
    },
    hide: {
        display: 'none',
    },
    drawerPaper: {
        position: 'fixed',
        left: 0,
        top: 0,
        height: '100%',
        width: drawerWidth,
        backgroundColor: dark[3],
        '&::-webkit-scrollbar': {
            width: 1,
        }
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'left',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    nested: {
        paddingLeft: theme.spacing.unit * 4,
    },
    hidden: {
        display: 'none',
    },
    active: {
        backgroundColor: dark[1],
    },
    navButton: {
        'text-decoration': 'none',
    },
    leftScrollbarWrapper: {
        direction: 'rtl',
    },
    leftScrollbar: {
        direction: 'ltr',
    },
});



class NavLeft extends React.Component {
    state = {
        open: true,
        mobile: false,
        drawerVariant: 'persistent',
    };

    constructor(props) {
        super(props);

        const mobile = (window.innerWidth <= 1024);

        this.state = {
            open: !mobile,
            mobile: mobile,
            drawerVariant: mobile ? 'temporary' : 'persistent',
        };
    }

    componentDidMount() {
        this.props.fetchCurrentSection();
    }

    toggleNavLeft = () => {
        const {nav} = this.props;
        const newState = !nav.navLeft;
        this.setState({ open: newState });
        this.props.toggleNavLeft(newState);
    };


    render() {
        const { classes } = this.props;
        const { mobile } = this.props.device;
        const { drawerVariant } = this.state;


        return (
            <Drawer
                color='primary'
                variant={drawerVariant}
                anchor={'left'}
                open={this.props.nav.navLeft}
                classes={{
                    paper: classes.drawerPaper,
                }}
                className={classes.leftScrollbarWrapper}
            >
                <Hidden mdUp>
                    <div className={classes.drawerHeader}>
                        <Toolbar>
                            <Button
                                color="primary"
                                aria-label="close drawer"
                                variant="raised"
                                onClick={this.toggleNavLeft}
                                className={classNames(classes.menuButton)}
                            >
                                <ArrowBack />
                            </Button>
                        </Toolbar>
                    </div>
                    <Divider/>
                </Hidden>
                <List className={classes.leftScrollbar}>
                    <Link className={classes.navButton} to="/">
                        <ListItem
                            button
                            className={classNames({
                                [classes.active]: this.props.nav.section === 'home' && this.props.nav.view === 'home',
                            })}
                        >
                            <ListItemText >Home</ListItemText>
                        </ListItem>
                    </Link>
                    <NavLeftHome/>
                    <NavLeftSpells/>
                    <NavLeftFeats/>
                    <NavLeftArtifacts/>
                    <NavLeftSheets/>
                    <NavLeftGames/>
                </List>
            </Drawer>
        );
    }
}


const mapStateToProps = (state) => ({
    errors: authErrors(state),
    user: state.auth.access,
    user_id: userId(state),
    device: {
        mobile: state.mobile,
    },
    nav: {
        navLeft: state.navLeft,
        navRight: state.navRight,
        section: state.currentSection,
        view: state.currentView,
    },
});

const mapDispatchToProps = (dispatch) => {
    return {
        fetchCurrentSection: () => dispatch(loadSection()),
        fetchCurrentView: () => dispatch(loadView()),
        toggleNavLeft: (newState) => dispatch(switchNavLeft(newState)),
    }
};

export default compose(
    withRouter,
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
    withStyles(styles, {
            withTheme: true
        }
    ))(NavLeft);

