import * as nav_action from '../actions/navRight'

const initialState = false;

export default (state=initialState, action) => {
    switch(action.type) {
        case nav_action.TOGGLE_NAV_RIGHT:
            return action.payload;
        default:
            return state;
    }
}

