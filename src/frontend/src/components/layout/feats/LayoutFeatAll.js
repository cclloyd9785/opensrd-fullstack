import React, { Component } from 'react';
import {authErrors, isAuthenticated, withAuth, userId} from '../../../reducers/index'

import { withStyles } from '@material-ui/core/styles'
import axios from 'axios'
import { dark } from '../../App'
import {connect} from "react-redux";
import compose from "recompose/compose";
import { loadRecentFeats } from "../../../actions/feat";
import {setSection} from "../../../actions/page";
import { Table, TableBody, TableCell, TableHead, TableRow, Paper }from '@material-ui/core';
import { Link } from 'react-router-dom';
import {setView} from "../../../actions/view";
import { Grid, Row, Col } from 'react-flexbox-grid'

const styles = theme => ({
    tableLink: {
        color: theme.palette.primary.contrastText,
        fontSize: 16,
        'text-decoration': 'none',
    },
});



class LayoutFeatAll extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.setSection('feats');
        this.props.setView('all');
        this.props.fetchRecentFeats(1);
    }

    render() {
        const {classes} = this.props;

        return (
            <Paper className={classes.root}>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell>Name</TableCell>
                            <TableCell>Prerequisites</TableCell>
                            <TableCell>Benefit</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.props.page.feat.list.map(n => {
                            return (
                                <TableRow key={n.id}>
                                    <TableCell component="th" scope="row">
                                        <Link className={classes.tableLink} to={"/feats/"+n.id}>{n.Name}</Link>
                                    </TableCell>
                                    <TableCell>{n.Prerequisite}</TableCell>
                                    <TableCell>{n.BenefitShort}</TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </Paper>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        errors: authErrors(state),
        user: state.auth.access,
        user_id: userId(state),
        page: {
            feat: state.feat,
        }

    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchRecentFeats: (pageNum) => dispatch(loadRecentFeats(pageNum)),
        setSection: (section) => dispatch(setSection(section)),
        setView: (view) => dispatch(setView(view)),
    }
};


export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),
    withStyles(styles, {
            withTheme: true
        }
    ))(LayoutFeatAll);