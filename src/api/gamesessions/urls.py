from rest_framework import routers
from .viewsets import GameSessionViewSet

router = routers.SimpleRouter()

router.register(r'^api/gamesessions', GameSessionViewSet, 'gamesessions')
