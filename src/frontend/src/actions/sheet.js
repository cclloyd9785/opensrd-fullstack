import { RSAA } from 'redux-api-middleware';
import { withAuth } from '../reducers';

export const FETCH_SHEET = '@@sheet/FETCH_SHEET';
export const FETCH_SHEET_SUCCESS = '@@sheet/FETCH_SHEET_SUCCESS';
export const FETCH_SHEET_FAILURE = '@@sheet/FETCH_SHEET_FAILURE';

export const UPDATE_SHEET = '@@sheet/UPDATE_SHEET';
export const UPDATE_SHEET_SUCCESS = '@@sheet/UPDATE_SHEET_SUCCESS';
export const UPDATE_SHEET_FAILURE = '@@sheet/UPDATE_SHEET_FAILURE';

export const CREATE_SHEET = '@@sheet/CREATE_SHEET';
export const CREATE_SHEET_SUCCESS = '@@sheet/CREATE_SHEET_SUCCESS';
export const CREATE_SHEET_FAILURE = '@@sheet/CREATE_SHEET_FAILURE';

export const FETCH_RECENT_SHEETS = '@@sheet/FETCH__RECENT_SHEETS';
export const FETCH_RECENT_SHEETS_SUCCESS = '@@sheet/FETCH_RECENT_SHEETS_SUCCESS';
export const FETCH_RECENT_SHEETS_FAILURE = '@@sheet/FETCH_RECENT_SHEETS_FAILURE';

export const FETCH_MY_SHEETS = '@@sheet/FETCH__MY_SHEETS';
export const FETCH_MY_SHEETS_SUCCESS = '@@sheet/FETCH_MY_SHEETS_SUCCESS';
export const FETCH_MY_SHEETS_FAILURE = '@@sheet/FETCH_MY_SHEETS_FAILURE';


export const updateSheet = (sheet) => ({
    [RSAA]: {
        endpoint: '/api/sheets/' + sheet.id + '/',
        method: 'PUT',
        body: JSON.stringify(sheet),
        //body: sheet,
        headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            UPDATE_SHEET, UPDATE_SHEET_SUCCESS, UPDATE_SHEET_FAILURE
        ]
    }
});

export const createSheet = (sheet) => ({
    [RSAA]: {
        endpoint: '/api/sheets/',
        method: 'POST',
        body: JSON.stringify(sheet),
        headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            CREATE_SHEET, CREATE_SHEET_SUCCESS, CREATE_SHEET_FAILURE
        ]
    }
});

export const loadSheet = (sheetId) => ({
    [RSAA]: {
        endpoint: '/api/sheets/' + sheetId + '/',
        method: 'GET',
        //headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            FETCH_SHEET, FETCH_SHEET_SUCCESS, FETCH_SHEET_FAILURE
        ]
    }
});

export const loadRecentSheets = (page) => ({
    [RSAA]: {
        endpoint: '/api/sheets/newest/',
        method: 'GET',
        //headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            FETCH_RECENT_SHEETS, FETCH_RECENT_SHEETS_SUCCESS, FETCH_RECENT_SHEETS_FAILURE
        ]
    }
});

export const loadMySheets = (page) => ({
    [RSAA]: {
        endpoint: '/api/sheets/mine/',
        method: 'GET',
        headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            FETCH_MY_SHEETS, FETCH_MY_SHEETS_SUCCESS, FETCH_MY_SHEETS_FAILURE
        ]
    }
});
