from rest_framework import routers
from .viewsets import FeatViewSet

router = routers.SimpleRouter()

router.register(r'^api/feats', FeatViewSet, 'feats')
