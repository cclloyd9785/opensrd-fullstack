import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, formValueSelector, getFormValues } from 'redux-form'
import { TextField, Paper, Typography, Divider, Card, CardHeader, CardContent, Button, IconButton, Checkbox } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import { Grid, Row, Col } from 'react-flexbox-grid'
import { dark } from '../App'
import {connect} from "react-redux";
import compose from "recompose/compose";
import {userId, authErrors, accessToken, currentGame} from "../../reducers";
import {updateGame} from "../../actions/game";
import SaveIcon from '@material-ui/icons/Save';
import CachedIcon from '@material-ui/icons/Cached';
import { renderCheckbox, renderSelectField, renderTextField } from "./reduxFormMaterialUi";
import {drawerWidth as drawerWidthRight} from "../nav/NavRight";

const ReactMarkdown = require('react-markdown');

const styles = theme => ({
    textInput: {
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    longPaper: {
        padding: theme.spacing.unit * 3,
        backgroundColor: dark[5],
    },
    gameTitle: {
        fontSize: 48,
        fontFamily: [
            'Balthazar',
        ],
    },
    gameContents: {
        fontSize: 22,
    },
    row: {
        'padding-top': theme.spacing.unit * 3,
        'padding-bottom': theme.spacing.unit * 3,
    },
    divider: {
        //'padding-top': theme.spacing.unit * 2
    },
    sectionTop: {
        'margin-top': theme.spacing.unit * 4,
    },
    sectionBottom: {
        'margin-bottom': theme.spacing.unit * 1,
    },
    sectionText: {
        'background-color': dark[5],
        'padding-left': theme.spacing.unit * 1,
    },
    markdownBody: {

    },
    pageContent: {},
    contentPage: {},
    sectionHeader: {
        'font-size': 24,
    },
    sectionPaper: {
        'margin-top': theme.spacing.unit * 3,
        'margin-bottom': theme.spacing.unit * 3,
    },
    gameCardContent: {
        'padding-left': 0,
        'padding-right': 0,
    },
    saveButton: {
        position: 'fixed',
        bottom: theme.spacing.unit * 2,
        right: theme.spacing.unit * 2 + drawerWidthRight,
    },
});

class FormGameEdit extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {classes} = this.props;

        const { submit, handleSubmit, pristine, reset, submitting } = this.props;


        return (
            <form
                onSubmit={handleSubmit(this.props.sendSubmit)}
            >
                <Button
                    variant="fab"
                    color="primary"
                    aria-label="Save"
                    disabled={pristine || submitting}
                    onClick={handleSubmit(this.props.sendSubmit)}
                    className={classes.saveButton}
                >
                    <SaveIcon/>
                </Button>

                <Paper className={classes.sectionPaper}>
                    <CardContent>
                        <Typography className={classes.sectionHeader}>Game Info</Typography>
                        <Divider />
                    </CardContent>
                    <CardContent>
                        <Grid container spacing={8}>
                            <Grid item xs={12} lg={4}>
                                <CardContent className={classes.gameCardContent}>
                                    <Typography>Game Name</Typography>
                                    <Divider />
                                    <Field
                                        fullWidth
                                        name="Name"
                                        component={renderTextField}
                                        label="Game Name"
                                    />
                                </CardContent>
                            </Grid>
                        </Grid>
                    </CardContent>
                </Paper>
            </form>
        );
    }
}

FormGameEdit.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
    return {
        errors: authErrors(state),
        user: state.user,
        page: {
            game: state.game,
        },
        initialValues: state.game.model,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        sendSubmit: (values) => dispatch(updateGame(values)),
    }
};


export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
    reduxForm({
        form: 'FormGameEdit',
        enableReinitialize: true
    }),
    withStyles(styles, {
            withTheme: true
        },
    ))(FormGameEdit);


