import React from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router'

import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';

import { dark } from '../../App'
import {authErrors, userId} from "../../../reducers";
import { connect } from 'react-redux'
import compose from 'recompose/compose';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import { ExpandLess, ExpandMore } from '@material-ui/icons';

import {loadSection} from "../../../actions/page";
import {loadView} from "../../../actions/view";


const styles = theme => ({
    hidden: {
        display: 'none',
    },

    navButton: {
        'text-decoration': 'none',
    },
    indentText: {
        paddingLeft: theme.spacing.unit * 5,
        backgroundColor: dark[2],
    },
    active: {
        backgroundColor: dark[1],
    },
});



class NavLeftSpells extends React.Component {
    state = {
        open: false,
    };

    constructor(props) {
        super(props);

        this.state = {
            open: false,
        };

    }

    componentDidMount() {
        let regex = /\/(spells)\//;
        let location = this.props.location.pathname;
        let matches = location.match(regex);
        let spellsSection = false;
        if (matches)
            spellsSection = true;

        this.setState({
            open: spellsSection,
        });
        this.props.fetchCurrentSection();
        this.props.fetchCurrentView();
    }

    handleToggle = () => {
        this.setState({
            open: !this.state.open,
        });
    };


    render() {
        let { classes } = this.props;

        let buttonView = undefined;
        if (this.props.nav.section === 'spells') {
            if (this.props.nav.view === 'view' || 'edit') {
                if (this.props.page.spell.model.id) {
                    buttonView =
                        <Link className={classes.navButton} to={"/spells/" + this.props.page.spell.model.id}>
                            <ListItem
                                button
                                className={classNames(classes.indentText, {
                                    [classes.active]: this.props.nav.section === 'spells' && this.props.nav.view === 'view',
                                })}
                            >
                                <ListItemText
                                    className={classes.navButton}>{this.props.page.spell.model.Name}</ListItemText>
                            </ListItem>
                        </Link>
                }
            }
        }

        let buttonEdit = undefined;
        if (this.props.nav.section === 'spells') {
            if (this.props.nav.view === 'view' || 'edit') {
                if (this.props.page.spell.model.owner) {
                    if (this.props.page.spell.model.owner === this.props.user_id) {
                        buttonEdit =
                            <Link className={classes.navButton} to={"/spells/" + this.props.page.spell.model.id + "/edit"}>
                                <ListItem
                                    button
                                    className={classNames(classes.indentText, {
                                        [classes.active]: this.props.nav.section === 'spells' && this.props.nav.view === 'edit',
                                    })}
                                >
                                    <ListItemText className={classes.navButton}>Edit {this.props.page.spell.model.Name}</ListItemText>
                                </ListItem>
                            </Link>
                    }
                }
            }
        }

        let buttonMine = undefined;
        if (this.props.user_id) {
            buttonMine =
                <Link className={classes.navButton} to={"/spells/mine"}>
                    <ListItem
                        button
                        className={classNames(classes.indentText, {
                            [classes.active]: this.props.nav.section === 'spells' && this.props.nav.view === 'mine',
                        })}
                    >
                        <ListItemText className={classes.navButton}>My Spells</ListItemText>
                    </ListItem>
                </Link>
        }

        let buttonNew = undefined;
        buttonNew =
            <Link className={classes.navButton} to={"/spells/new"}>
                <ListItem
                    button
                    className={classNames(classes.indentText, {
                        [classes.active]: this.props.nav.section === 'spells' && this.props.nav.view === 'new',
                    })}
                >
                    <ListItemText className={classes.navButton}>Create new spell</ListItemText>
                </ListItem>
            </Link>;

        return (
            <div>
                <ListItem
                    button
                    onClick={this.handleToggle}
                    className={classNames({
                        [classes.active]: this.state.open,
                    })}
                >
                    <ListItemText primary="Spells" />
                    {this.state.open ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={this.state.open} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                        <Link className={classes.navButton} to="/spells/recent">
                            <ListItem
                                button
                                className={classNames(classes.indentText, {
                                    [classes.active]: this.props.nav.section === 'spells' && this.props.nav.view === 'all',
                                })}
                            >
                                <ListItemText>All Spells</ListItemText>
                            </ListItem>
                        </Link>
                        {buttonNew}
                        {buttonMine}
                        {buttonView}
                        {buttonEdit}
                    </List>
                </Collapse>
            </div>
        );
    }
}


const mapStateToProps = (state) => ({
    errors: authErrors(state),
    user: state.auth.access,
    user_id: userId(state),
    page: {
        spell: state.spell,
    },
    nav: {
        navLeft: state.navLeft,
        navRight: state.navRight,
        section: state.currentSection,
        view: state.currentView,
    }
});

const mapDispatchToProps = (dispatch) => {
    return {
        fetchCurrentSection: () => dispatch(loadSection()),
        fetchCurrentView: () => dispatch(loadView()),
    }
};

export default compose(
    withRouter,
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
    withStyles(styles, {
            withTheme: true
        }
    ))(NavLeftSpells);

