import * as spell_action from '../actions/spell'

const initialState = {
    model: {},
    list: [],
};

export default (state=initialState, action) => {
    switch(action.type) {
        case spell_action.FETCH_SPELL_SUCCESS:
            return {
                model: action.payload,
                list: [],
            };
        case spell_action.UPDATE_SPELL_SUCCESS:
            return {
                model: action.payload,
                list: [],
            };
        case spell_action.CREATE_SPELL_SUCCESS:
            return {
                model: action.payload,
                list: [],
            };
        case spell_action.FETCH_RECENT_SPELLS_SUCCESS:
            return {
                model: {},
                list: action.payload,
            };
        case spell_action.FETCH_MY_SPELLS_SUCCESS:
            return {
                model: {},
                list: action.payload,
            };
        default:
            return state;
    }
}

export function loadSpell(state) {
    if (state.spell) {
        return state.spell
    }
}
