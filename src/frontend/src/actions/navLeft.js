
export const TOGGLE_NAV_LEFT = '@@nav/TOGGLE_NAV_LEFT';

const navLeftSuccess = (newState) => {
    return {
        type: TOGGLE_NAV_LEFT,
        payload: newState,
    }
};

export const switchNavLeft = (newState) => {
    return function (dispatch) {
        dispatch(navLeftSuccess(newState));
        return newState;
    }
};
