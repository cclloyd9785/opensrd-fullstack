import * as chat_action from '../actions/chat'
import {FETCH_RECENT_CHAT_MESSAGES_SUCCESS} from "../actions/chat";
import {PUSHER_ADD_MESSAGE_LIST_SUCCESS} from "../actions/chat";
import {PUSHER_SET_CHAT_MESSAGE_LIST_SUCCESS} from "../actions/chat";
import {ADD_CURRENT_INPUT_TO_INPUT_LOG_SUCCESS} from "../actions/chat";

const initialState = {
    messages: [],
    inputLog: [
        {
            Message: ' ',
        }
    ],
    session: {},
    error: {
        error: false,
        message: "",
    },
    status: {
        connected: false,
        connecting: false,
        error: false,
    },
};

export default (state=initialState, action) => {
    switch(action.type) {
        case chat_action.CREATE_CHAT_MESSAGE_SUCCESS:
            return state;
        case chat_action.CLEAR_CHAT_MESSAGES_SUCCESS:
            return {
                ...state,
                messages: [],
            };
        case chat_action.REPLACE_CURRENT_INPUT_IN_INPUT_LOG_SUCCESS:
            /*

            TODO:  Get chat log to append to array if sent message
            TODO:  Add {input: true} to current Input of inputLog
            - Get index of input:true
            - Get everything after that index
            - Prepend new input with index:false to everything else
            - return new array


             */
            let inputLog = state.inputLog;
            if (action.payload.Message === "")
                return state;
            if (inputLog.length === 1)
                return {
                    ...state,
                    inputLog: [action.payload],
                };
            inputLog.shift();
            return {
                ...state,
                inputLog: [
                    action.payload,
                    ...inputLog,
                ].slice(-50),
            };
        case chat_action.ADD_CURRENT_INPUT_TO_INPUT_LOG_SUCCESS:
            if (state.inputLog[0].Message === "")
                return [action.payload];
            if (state.inputLog.length === 1) {
                return {
                    ...state,
                    inputLog: [
                        action.payload,
                    ],
                };
            }
            return {
                ...state,
                inputLog: [
                    action.payload,
                    ...state.inputLog,
                ].slice(-50),
            };
        case chat_action.ADD_MESSAGE_TO_INPUT_LOG_SUCCESS:
            return {
                ...state,
                inputLog: [
                    ...state.inputLog,
                    action.payload,
                ].slice(-50),
            };
        case chat_action.CLEAR_INPUT_LOG_SUCCESS:
            return {
                ...state,
                inputLog: [],
            };
        case chat_action.CLEAR_CHAT_SESSION_SUCCESS:
            return {
                ...state,
                session: {},
            };
        case chat_action.PUSHER_SET_CURRENT_ROOM_SUCCESS:
            return {
                ...state,
                chatkit: {
                    ...state.chatkit,
                    currentRoom: action.payload,
                }
            };
        case chat_action.PUSHER_SET_CURRENT_USER_SUCCESS:
            return {
                ...state,
                chatkit: {
                    ...state.chatkit,
                    currentUser: action.payload,
                }
            };
        case chat_action.PUSHER_ADD_MESSAGE_LIST_SUCCESS:
            return {
                ...state,
                messages: [
                    ...state.messages,
                    action.payload,
                ].slice(-30),
            };
        case chat_action.PUSHER_SET_CHAT_MESSAGE_LIST_SUCCESS:
            return {
                ...state,
                messages: action.payload,
            };
        case chat_action.FETCH_RECENT_CHAT_MESSAGES_SUCCESS:
            return {
                ...state,
                messages: action.payload,
            };
        case chat_action.FETCH_ALL_CHAT_MESSAGES_SUCCESS:
            return {
                ...state,
                messages: action.payload,
            };
        case chat_action.CREATE_CHAT_SESSION_SUCCESS:
            return {
                ...state,
                session: action.payload,
            };
        case chat_action.FETCH_CHAT_SESSION_SUCCESS:
            return {
                ...state,
                session: action.payload,
            };
        case chat_action.FETCH_CHAT_SESSION_FAILURE:
            return {
                ...state,
                error: {
                    error: true,
                    message: '',
                }
            };
        case chat_action.JOIN_CHAT_SESSION_SUCCESS:
            let chat_session = state.session;

            return {
                ...state,
                session: action.payload,
                status: {
                    ...state.status,
                    connected: true,
                }
            };
        default:
            return state;
    }
}

export function loadChat(state) {
    if (state.chat) {
        return state.chat
    }
}
