from django.contrib import admin
from django.views import generic
from django.urls import path, include, re_path
from rest_framework.schemas import get_schema_view

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
from rest_framework import routers
from . import views as apiviews
import api.viewsets as api_viewsets

from spells.urls import router as spells
from feats.urls import router as feats
from artifacts.urls import router as artifacts
from sheets.urls import router as sheets
from games.urls import router as games
from gamesessions.urls import router as gamesessions
from chatmessages.urls import router as chatmessages
from chatsessions.urls import router as chatsessions
from characters.urls import router as characters

router = routers.SimpleRouter()

router.register(r'^api/user', api_viewsets.UserViewSet, 'user')

urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^$', generic.RedirectView.as_view(url='/api/', permanent=False)),
    re_path(r'^api/register/', apiviews.register, name='register'),
    re_path(r'^api/$', get_schema_view()),
    re_path(r'^api/auth/', include('rest_framework.urls', namespace='rest_framework')),
    re_path(r'^api/auth/token/obtain/$', TokenObtainPairView.as_view()),
    re_path(r'^api/auth/token/refresh/$', TokenRefreshView.as_view()),

]

urlpatterns += spells.urls
urlpatterns += feats.urls
urlpatterns += artifacts.urls
urlpatterns += sheets.urls
urlpatterns += games.urls
urlpatterns += gamesessions.urls
urlpatterns += chatmessages.urls
urlpatterns += chatsessions.urls
urlpatterns += characters.urls

urlpatterns += router.urls



