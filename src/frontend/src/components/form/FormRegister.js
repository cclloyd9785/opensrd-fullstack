import React, { Component } from 'react';
import classNames from 'classnames';
import { TextField, Typography, Divider } from '@material-ui/core'
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import { Grid, Row, Col } from 'react-flexbox-grid';
import { connect } from 'react-redux'
import { register, login, registerError } from  '../../actions/auth'
import {authErrors, userId} from '../../reducers'
import compose from 'recompose/compose';
import { renderCheckbox, renderSelectField, renderTextField } from "./reduxFormMaterialUi";
import {loadUser} from "../../actions/user";


const styles = theme => ({
    hidden: {
        display: 'none',
    },
    errorText: {
        color: 'red',
    }
});



class FormRegister extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            email: '',
            password: '',
            remember: false,
            sent: false,
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    onSubmit = (e) => {
        e.preventDefault();
        this.setState({
            sent: true,
        });
        this.props.onSubmit(this.state.username, this.state.email, this.state.password)
    };

    render() {
        const {classes, user_id} = this.props;

        if (!user_id)
            return (

                <form
                    className={classes.formLogin}
                    onSubmit={this.onSubmit}>
                    <Grid fluid>

                        <Row>
                            <Col sm={12}>
                                <Typography variant="display2">Register</Typography>
                            </Col>
                        </Row>

                        <Row>
                            <Col sm={12}>
                                <Divider/>
                            </Col>
                        </Row>

                        <Row className={classNames({
                            [classes.hidden]: !this.props.errors && !this.state.sent,
                        })}>
                            <Col sm={12}>
                                <Typography className={classes.errorText} variant="body2">There was an error registering with the current form values.</Typography>
                            </Col>
                            <Col sm={12}>
                                <Divider/>
                            </Col>
                        </Row>

                        <Row>
                            <Col md={3} xs={12} className={classes.textInput}>
                                <TextField
                                    id='username'
                                    name='username'
                                    label='Username'
                                    required fullWidth
                                    value={this.state.username}
                                    onChange={this.handleChange('username')}
                                />
                            </Col>
                            <Col md={3} xs={12} className={classes.textInput}>
                                <TextField
                                    id='email'
                                    name='email'
                                    label='Email'
                                    required fullWidth
                                    value={this.state.email}
                                    onChange={this.handleChange('email')}
                                />
                            </Col>
                            <Col md={2} xs={12} className={classes.textInput}>
                                <TextField
                                    id='password'
                                    type='password'
                                    name='username'
                                    label='Password'
                                    required fullWidth
                                    value={this.state.password}
                                    onChange={this.handleChange('password')}
                                />
                            </Col>
                            <Col md={2} mdOffset={0} xs={6} xsOffset={3} className={classes.textInput}>
                                <Button
                                    variant='raised'
                                    color='primary'
                                    fullWidth
                                    onClick={this.onSubmit}>Submit
                                </Button>
                            </Col>
                        </Row>
                    </Grid>
                </form>
            );
        else {
            return(
                <div>
                    <Typography variant="display1">Welcome.</Typography>
                </div>
            );
        }
    }
}

const mapStateToProps = (state) => ({
    errors: authErrors(state),
    user: state.auth.access,
    user_id: userId(state),
});

const mapDispatchToProps = (dispatch) => ({
    onSubmit: (username, email, password) => {
        dispatch(register(username, email, password))
            .then(response => {
                if (!response.error) {
                    dispatch(login(username, password))
                        .then(response => {
                            dispatch(loadUser())
                                .then(response => {
                                    window.location.reload();
                                });
                        });
                }
                else {
                    dispatch(registerError(response))
                }
            });
    }
});

export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),
    withStyles(styles, {
            withTheme: true
        }
    ))(FormRegister);

//export default withStyles(styles, { withTheme: true })(FormHome);