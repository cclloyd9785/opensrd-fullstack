import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router'

import FormHome from '../form/FormHome'
import {login} from '../../actions/auth'
import {authErrors, isAuthenticated} from '../../reducers/index'
import {withStyles} from "@material-ui/core/styles/index";
import compose from "recompose/compose";
import {setSection} from "../../actions/page";
import {setView} from "../../actions/view";
import appBarHeight from "../MaterialUiApp";
import { Typography } from '@material-ui/core';
import LayoutHome from "./LayoutHome";


const styles = theme => ({

});

class LayoutLogin extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.setSection('home');
        this.props.setView('login');
    }

    render() {
        let component = <FormHome {...this.props}/>;

        if (this.props.isAuthenticated) {
            if (this.props.location.state)
                if (this.props.location.state.from)
                    component = <Redirect to={this.props.location.state.from.pathname}/>
        }


        return (
            <div>
                {component}
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    errors: authErrors(state),
    isAuthenticated: isAuthenticated(state),
});

const mapDispatchToProps = (dispatch) => ({
    setSection: (section) => dispatch(setSection(section)),
    setView: (view) => dispatch(setView(view)),
});

export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),
    withStyles(styles, {
            withTheme: true
        }
    ))(LayoutLogin);