from rest_framework import views, serializers, status
from rest_framework.exceptions import PermissionDenied

from .models import Spell


class SpellSerializer(serializers.ModelSerializer):
    class Meta:
        model = Spell
        fields = '__all__'

    def create(self, validated_data):
        # Create and return a new `Snippet` instance, given the validated data.

        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user

        validated_data['owner'] = user.id
        return Spell.objects.create(**validated_data)

    def update(self, instance, validated_data):

        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user

            if user.id != instance.owner and not request.user.is_staff:
                raise PermissionDenied({"message": "You don't have permission to access this feat"})

        # Update and return an existing `Snippet` instance, given the validated data.
        [setattr(instance, k, v) for k, v in validated_data.items()]
        instance.save()
        return instance
