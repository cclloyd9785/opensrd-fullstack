import * as nav_action from '../actions/navLeft'

const initialState = false;

export default (state=initialState, action) => {
    switch(action.type) {
        case nav_action.TOGGLE_NAV_LEFT:
            return action.payload;
        default:
            return state;
    }
}

