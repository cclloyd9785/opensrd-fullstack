from django.db import models
from django.utils import timezone
import datetime
# Create your models here.


class Artifact(models.Model):
    owner = models.IntegerField('owner', default=0)

    Name =                  models.CharField('Name', max_length=255, default="", blank=True)
    Flavor =                models.CharField('Flavor', max_length=255, default="", blank=True)
    Aura =                  models.CharField('Aura', max_length=255, default="", blank=True)
    CL =                    models.CharField('CL', max_length=16, default="", blank=True)
    Slot =                  models.CharField('Slot', max_length=255, default="", blank=True)
    Weight =                models.CharField('Weight', max_length=255, default="", blank=True)
    SpecialPower =          models.CharField('SpecialPower', max_length=255, default="", blank=True)

    # Like +2 axiomatic sawtooth saber
    Type =                  models.CharField('Type', max_length=128, default="", blank=True)
    # Usually major (unique), minor, common, rare, ultra rare, unique, or custom.
    Rarity =                models.CharField('Rarity', max_length=32, default="", blank=True)
    Mythic =                models.BooleanField('Mythic', default=False)
    Artifact =              models.BooleanField('Artifact', default=False)

    # For sentient items
    Alignment =             models.CharField('Alignment', max_length=8, default="", blank=True)
    Senses =                models.CharField('Senses', max_length=255, default="", blank=True)
    Communication =         models.CharField('Communication', max_length=255, default="", blank=True)
    Ego =                   models.CharField('Ego', max_length=8, default="", blank=True)
    IntScore =              models.CharField('IntScore', max_length=8, default="", blank=True)
    WisScore =              models.CharField('WisScore', max_length=8, default="", blank=True)
    ChaScore =              models.CharField('ChaScore', max_length=8, default="", blank=True)
    SpecialPurpose =        models.CharField('SpecialPurpose', max_length=255, default="", blank=True)
    SpellLikeAbilities =    models.CharField('SpellLikeAbilities', max_length=255, default="", blank=True)

    Description =           models.TextField('Description', default="", blank=True)
    Destruction =           models.TextField('Destruction', default="", blank=True)
    Ramifications =         models.TextField('Ramifications', default="", blank=True)
    DescriptionShort =      models.CharField('DescriptionShort', max_length=255, default="", blank=True)
    DestructionShort =      models.CharField('DestructionShort', max_length=255, default="", blank=True)
    RamificationsShort =    models.CharField('RamificationsShort', max_length=255, default="", blank=True)

    Construction =                  models.TextField('Construction', default="", blank=True)
    ConstructionRequirements =      models.CharField('ConstructionRequirements', max_length=255, default="", blank=True)
    Price =                         models.CharField('Price', max_length=128, default="", blank=True)
    Cost =                          models.CharField('Cost', max_length=128, default="", blank=True)

    Views = models.IntegerField('Views', default=0)

    created = models.DateTimeField('created_at', default=timezone.now)
    modified = models.DateTimeField('modified_at', default=timezone.now)
    deleted = models.DateTimeField('deleted_at', default=None, null=True, blank=True)

    def __str__(self):
        return '%s' % self.Name

