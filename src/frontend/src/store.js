import storage from 'redux-persist/es/storage'
import { applyMiddleware, createStore, compose } from 'redux'
import { createFilter   } from 'redux-persist-transform-filter';
import { persistReducer, persistStore } from 'redux-persist'
import { routerMiddleware } from 'react-router-redux'
import thunk from "redux-thunk";

import apiMiddleware from './middleware';
import rootReducer from './reducers'

export default (history) => {
    const persistedFilter = createFilter(
        'auth', ['access', 'refresh']
    );

    const reducer = persistReducer(
        {
            key: 'polls',
            storage: storage,
            whitelist: ['auth', 'user', 'game'],
            transforms: [ persistedFilter]
        },
        rootReducer
    );

    const store = createStore(
        reducer, {},
        compose(
            applyMiddleware(thunk, apiMiddleware, routerMiddleware(history)),
            window.devToolsExtension ? window.devToolsExtension() : f => f,
        ),
    );

    persistStore(store);

    return store
}